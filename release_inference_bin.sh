#!bash


if [ -z "$1" ]; then
	echo ""
	echo "No release folder name found for output !"
	echo ""
	echo "Usage:   sh ./release_inference_bin.sh <release_folder_name>"
	echo "Example: sh ./release_inference_.sh Inference_Umbrella_bin_v1.0"
	exit
fi

# Get the working folder with absolutely path
working_folder=${PWD##*/}
echo ""
echo "Release $working_folder to ../$1"

custom_id=`sed -n 's/^#define  *\([^ ]*\)  *\(.*\) *$/\2/p' custom_id.h`

OUT_DIR=./x64/$1

NugetPackages=./caffe.git/windows/NugetPackages
OUT_Release=${OUT_DIR}/x64/Release
OUT_Debug=${OUT_DIR}/x64/Debug
echo ""

fun_DLLs()
{
	echo "Copy prebuilt release mode dlls"
	if [ ! -f ${OUT_Release}/libgflags.dll ]; then
		cp -a ${NugetPackages}/gflags.2.1.2.1/build/native/x64/v120/dynamic/Lib/gflags.dll                ${OUT_Release}/libgflags.dll
	fi
	if [ ! -f ${OUT_Release}/gflags.dll ]; then
		cp -a ${NugetPackages}/gflags.2.1.2.1/build/native/x64/v120/dynamic/Lib/gflags.dll                ${OUT_Release}/gflags.dll
	fi
	if [ ! -f ${OUT_Release}/hdf5.dll ]; then
		cp -a ${NugetPackages}/hdf5-v120-complete.1.8.15.2/lib/native/bin/x64/*.dll                       ${OUT_Release}
	fi
	if [ ! -f ${OUT_Release}/libglog.dll ]; then
		cp -a ${NugetPackages}/glog.0.3.3.0/build/native/bin/x64/v120/Release/dynamic/libglog.dll         ${OUT_Release}
	fi
	if [ ! -f ${OUT_Release}/libgcc_s_seh-1.dll ]; then
		cp -a ${NugetPackages}/OpenBLAS.0.2.14.1/lib/native/bin/x64/*.dll                                 ${OUT_Release}
	fi
	if [ ! -f ${OUT_Release}/lmdb.dll ]; then
		cp -a ${NugetPackages}/lmdb-v120-clean.0.9.14.0/lib/native/bin/x64/lmdb.dll                       ${OUT_Release}
	fi
	if [ ! -f ${OUT_Release}/opencv_core2411.dll ]; then
		cp -a ${NugetPackages}/OpenCV.2.4.11/build/native/bin/x64/v120/Release/opencv_core2411.dll        ${OUT_Release}
		cp -a ${NugetPackages}/OpenCV.2.4.11/build/native/bin/x64/v120/Release/opencv_highgui2411.dll     ${OUT_Release}
		cp -a ${NugetPackages}/OpenCV.2.4.11/build/native/bin/x64/v120/Release/opencv_imgproc2411.dll     ${OUT_Release}
		cp -a ${NugetPackages}/OpenCV.2.4.11/build/native/bin/x64/v120/Release/opencv_objdetect2411.dll   ${OUT_Release}
	fi

	cp -a ./x64/Release/cublas*.dll 		${OUT_Release}
	cp -a ./x64/Release/cudart*.dll 		${OUT_Release}
	cp -a ./x64/Release/cudnn*.dll 			${OUT_Release}
	cp -a ./x64/Release/curand*.dll 		${OUT_Release}
	
	if [ ! -f ${OUT_Release}/msvcr120.dll ]; then
		cp -a /c/Windows/System32/msvcr120.dll    ${OUT_Release}
	fi	
	if [ ! -f ${OUT_Release}/msvcp120.dll ]; then
		cp -a /c/Windows/System32/msvcp120.dll    ${OUT_Release}
	fi	
	
	if [ ! -f ${OUT_Release}/default.ini ]; then
		cp -a ./default.ini    ${OUT_Release}
	fi	

}


fun_Inference_GUI()
{
	echo "Copy classification_tool file"
	cp -a ./x64/Release/classification_tool.exe              ${OUT_Release}
}


fun_Doc()
{
	echo "Generate documents."
	cp ./doc/README_classifiy.md 				${OUT_DIR}/README_classifiy.md

}


fun_USB_Key_Driver()
{
	echo "Copy USB key driver."
	cp -a ./doc/USB_key_driver 	${OUT_DIR}
}


fun_inference_DLLs()
{
	cp -a ./x64/Release/Classifier.dll 		    ${OUT_Release}
	cp -a ./x64/Release/inference_CLR.dll 		${OUT_Release}
}


fun_copy_inference_models()
{
	echo "Copy inference models"
	cp -a ./x64/Release/resource 				${OUT_Release}
}


mkdir -p ${OUT_DIR}
mkdir -p ${OUT_Release}
mkdir -p ${OUT_DIR}/doc

fun_DLLs
fun_USB_Key_Driver
fun_copy_inference_models
fun_inference_DLLs
fun_Inference_GUI
fun_Doc


echo ""
echo "Done."
