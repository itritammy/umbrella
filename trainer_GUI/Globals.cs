﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace class_trainer
{
    static class Globals
    {
        // global function
        public static string GetCustomID()
        {
            return "UMBRELLA"; /// Customer Proprietary
        }
        
        public static string GetAppVersion()
        {
            return "2.8.6";
        }
        
        public static string GetApplicationLog()
        {
            return "log/application.log";
        }

        public static string GetDNN_Log()
        {
            return "log/DNN.log";
        }
        
    }
}
