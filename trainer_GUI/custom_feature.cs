﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;

using trainer_CLR;
using System.Diagnostics;
using System.Threading;
using System.Deployment.Application;


namespace class_trainer
{
    public partial class Form1 : Form
    {
        private string CUSTOM_NAME = Globals.GetCustomID(); //"CITC", "TRI", "ARDENTEC", etc.;

        private string get_ID_code_by_categoryID(int categoryID)
        {
            string fmt = "00";
            //if (CUSTOM_NAME == "ARDENTEC")
            //{
            //    /// for Ardentec
            //    switch (categoryID)
            //    {
            //        case 0: return "10"; // Probe Mark Shift
            //        case 1: return "15"; // Overkill
            //        case 2: return "2D"; // Ugly Die
            //        case 3: return "07"; // Fab Process Defect
            //        case 4: return "16"; // Particle
            //        case 5: return "09"; // Foreign Material
            //        case 6: return "1B"; // Pad discoloration
            //    }
            //}
            return categoryID.ToString(fmt);
        }

        private int get_Max_NbOf_category()
        {
            if (CUSTOM_NAME == "UMBRELLA")
                return 20;
            else
                return 60;
            //return 20; // 20class_limition
            //return 60; //Generic 
        }

        private void custom_tab_ModelParam()    // STEP2
        {
            if (CUSTOM_NAME == "ARDENTEC" || CUSTOM_NAME == "GIGA")
            {
                panel_augmentation.Enabled = false;
                textCropSize_training.Text = "256";
                textCropSize_validation.Text = "288";
                textDeltaWidthHeight_training.Text = "64";
                textNewSize_training.Text = "320";
                textNewSize_validation.Text = "320";
                ckb_advanced.Enabled = false;
                ckb_advanced.Visible = false;
                cmb_pseudoBalanceFactor.SelectedIndex = 0;
                cmb_downsampling_factor.SelectedIndex = 1; // for 500x500
                label61.Visible = false;
                cmb_top_N_model.Visible = false;
                cmb_top_N_model.SelectedIndex = 0;

                // Image pre-processing
                pObj.CFG_AddStringValue("image_data_param", "input_preprocessing", "keep_center_in_pixels");
                pObj.CFG_AddIntegerValue("image_data_param", "pixels_expand_to_top", 250);
                pObj.CFG_AddIntegerValue("image_data_param", "pixels_expand_to_bottom", 250);
                pObj.CFG_AddIntegerValue("image_data_param", "pixels_expand_to_left", 250);
                pObj.CFG_AddIntegerValue("image_data_param", "pixels_expand_to_right", 250);
            }
        }

        private List<Tuple<string, bool>> get_tab_permissions()
        {
            List<Tuple<string, bool>> available_tabs = new List<Tuple<string, bool>>();

            // Tuple<int, System::String^>(dev_list.at(i).GPU_id, gpu_name)
            available_tabs.Add(new Tuple<string, bool>("tab_step1", true));  // STEP1
            available_tabs.Add(new Tuple<string, bool>("tab_step2", true));  // STEP2
            available_tabs.Add(new Tuple<string, bool>("tab_step3", true));  // STEP3
            available_tabs.Add(new Tuple<string, bool>("tab_confusion", true));  // STEP4

            if (CUSTOM_NAME == "CITC")
            {
                available_tabs.Add(new Tuple<string, bool>("tab_threshold", false));  // Threshold Adjustment
                available_tabs.Add(new Tuple<string, bool>("tab_inference", true));  // Inference
                available_tabs.Add(new Tuple<string, bool>("tab_presorting", true));  // Pre-sorting
            }
            else if (CUSTOM_NAME == "TRI")
            {
                available_tabs.Add(new Tuple<string, bool>("tab_threshold", false));  // Threshold Adjustment
                available_tabs.Add(new Tuple<string, bool>("tab_inference", false));  // Inference
                available_tabs.Add(new Tuple<string, bool>("tab_presorting", false));  // Pre-sorting
            } 
            else if (CUSTOM_NAME == "ARDENTEC" || CUSTOM_NAME == "GIGA")
            {
                available_tabs.Add(new Tuple<string, bool>("tab_threshold", true));  // Threshold Adjustment
                available_tabs.Add(new Tuple<string, bool>("tab_inference", false));  // Inference
                available_tabs.Add(new Tuple<string, bool>("tab_presorting", false));  // Pre-sorting
            }
            else if (CUSTOM_NAME == "UMBRELLA")
            {
                available_tabs.Add(new Tuple<string, bool>("tab_threshold", false));  // Threshold Adjustment
                available_tabs.Add(new Tuple<string, bool>("tab_inference", true));  // Inference
                available_tabs.Add(new Tuple<string, bool>("tab_presorting", true));  // Pre-sorting
            }

            return available_tabs;
        }

    }
}
