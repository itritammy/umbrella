﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;


namespace class_trainer
{
    public partial class frmNewProj : Form
    {
        public string prj_name = string.Empty;
        public string prj_location = string.Empty;
        public string prj_machine_type = string.Empty;  // pretrained_solution
        public string prj_model_path = string.Empty;
        public string prj_published_model_name = string.Empty;

        public int error = 0;

        //bool BUILT_FOR_Generic_Vesion = false;

        private Melt_API melt_api = new Melt_API();


        public frmNewProj()
        {
            InitializeComponent();


            /// Init the combobox of pretrained_solution according to the models location
            SearchPretrainedSolutions();
            error = 0;

            //  remove the minimize and maximize buttons
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            /// 2020_0819,Jim: Support custom GUI according to the Customer ID
            custom_GUI();
        }

        private void custom_GUI()
        {
            string name = Globals.GetCustomID();
            if (name == "TRI")
            {
                label1.Visible = true;
                pretrained_solution.Visible = true;
            }
        }

        public void SearchPretrainedSolutions()
        {
            string str = System.AppDomain.CurrentDomain.BaseDirectory;
            string models_location = str + "data\\models";
            try
            {
                DirectoryInfo di = new DirectoryInfo(models_location);

                int i = 0;
                foreach (var item in di.GetDirectories())
                {
                    /// Check default filename
                    string default_file = models_location + "\\" + item.Name + "\\default.aes";
                    if (File.Exists(default_file))
                    {
                        pretrained_solution.Items.Add(item.Name);
                        if (item.Name == "DFBN")
                             pretrained_solution.SelectedIndex = i;

                        i++;
                    }
                }
            }
            catch (Exception exce)
            {
                MessageBox.Show(exce.ToString(), "Models location searching failed" , MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public void CreateFromDefaultProject(string project_file_name, string solution_name)
        {
            try
            {
                if (pretrained_solution.SelectedIndex >= 0)
                {
                    string def_file_path = "data/models/" + solution_name + "/default.aes";
                    System.IO.File.Copy(def_file_path, project_file_name, true);
                }
                else
                {
                    System.IO.File.Copy(@"data/models/DFBN/default.aes", project_file_name, true);
                }
            }
            catch (Exception exce)
            {
                MessageBox.Show("default.aes not found", exce.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnOK_Click(object sender, EventArgs e)
        {
            prj_name = txtProject_Name.Text;
            prj_location = txtProject_Location.Text + "/" + txtProject_Name.Text;
            prj_location = prj_location.Replace(@"\", @"/");
            error = -1000;

            if (MessageBox.Show("This will create the folder " + prj_location + "   \nAre your sure?", "Project Location",
                  MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                  == DialogResult.Yes)
            {
                // Specify the directory you want to manipulate.
                string path = prj_location;
                prj_model_path = prj_location + "/model";

                try
                {
                    // Determine whether the directory exists.
                    if (Directory.Exists(path))
                    {
                        //Console.WriteLine("That path exists already.");
                        MessageBox.Show("That path exists already.");
                        return;
                    }

                    // Try to create the directory.
                    DirectoryInfo di = Directory.CreateDirectory(path);
                    //Console.WriteLine("The directory was created successfully at {0}.", Directory.GetCreationTime(path));

                    // Delete the directory.
                    //di.Delete();
                    //Console.WriteLine("The directory was deleted successfully.");
                    string project_file = path + "/" + txtProject_Name.Text + ".proj";
                    if (!File.Exists(project_file))
                    {
                        prj_published_model_name = txtModelName.Text;
                        //prj_machine_type = pretrained_solution.Text;
                        CreateFromDefaultProject(project_file, pretrained_solution.Text);

                        // Create working folders
                        Directory.CreateDirectory(prj_model_path);
                        Directory.CreateDirectory(prj_location + "/snapshot");

                        /// copy models
                        string source_target = "data/models/" + pretrained_solution.Text;
                        melt_api.DirectoryCopy(source_target, prj_model_path, true);

                        error = 0;
                    }
                }
                catch (Exception exce)
                {
                    //Console.WriteLine("The process failed: {0}", exce.ToString());
                    System.IO.Directory.Delete(path,true);
                    MessageBox.Show("Project create fail or model not found. " + exce.ToString(), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                finally { }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            string folderName;

            // Show the FolderBrowserDialog.
            folderBrowserDialog1.Description = "Set the project folder";
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath;
                txtProject_Location.Text = folderName;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            return;
        }
    }
}
