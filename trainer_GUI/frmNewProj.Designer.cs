﻿namespace class_trainer
{
    partial class frmNewProj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_ok = new System.Windows.Forms.Button();
            this.txtProject_Location = new System.Windows.Forms.TextBox();
            this.button12 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txtProject_Name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pretrained_solution = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.label2 = new System.Windows.Forms.Label();
            this.txtModelName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_ok
            // 
            this.btn_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btn_ok.Location = new System.Drawing.Point(500, 99);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(58, 23);
            this.btn_ok.TabIndex = 11;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // txtProject_Location
            // 
            this.txtProject_Location.Location = new System.Drawing.Point(119, 42);
            this.txtProject_Location.Name = "txtProject_Location";
            this.txtProject_Location.Size = new System.Drawing.Size(367, 22);
            this.txtProject_Location.TabIndex = 10;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(492, 41);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(66, 23);
            this.button12.TabIndex = 9;
            this.button12.Text = "Browse...";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 45);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 12);
            this.label7.TabIndex = 8;
            this.label7.Text = "Project Location : ";
            // 
            // txtProject_Name
            // 
            this.txtProject_Name.Location = new System.Drawing.Point(119, 14);
            this.txtProject_Name.Name = "txtProject_Name";
            this.txtProject_Name.Size = new System.Drawing.Size(180, 22);
            this.txtProject_Name.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 12);
            this.label6.TabIndex = 6;
            this.label6.Text = "Project Name : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 12);
            this.label1.TabIndex = 12;
            this.label1.Text = "Pre-trained solution : ";
            this.label1.Visible = false;
            // 
            // pretrained_solution
            // 
            this.pretrained_solution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.pretrained_solution.FormattingEnabled = true;
            this.pretrained_solution.Location = new System.Drawing.Point(119, 98);
            this.pretrained_solution.Name = "pretrained_solution";
            this.pretrained_solution.Size = new System.Drawing.Size(180, 20);
            this.pretrained_solution.TabIndex = 13;
            this.pretrained_solution.Visible = false;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button1.Location = new System.Drawing.Point(436, 99);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(58, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Cancel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 73);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 12);
            this.label2.TabIndex = 15;
            this.label2.Text = "Model Name : ";
            // 
            // txtModelName
            // 
            this.txtModelName.Location = new System.Drawing.Point(119, 70);
            this.txtModelName.Name = "txtModelName";
            this.txtModelName.Size = new System.Drawing.Size(180, 22);
            this.txtModelName.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(305, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(222, 32);
            this.label3.TabIndex = 17;
            this.label3.Text = "Give a name for your model. For example, zebra, panda, SMT, Wafer, etc.";
            // 
            // frmNewProj
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(572, 130);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btn_ok);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtModelName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.pretrained_solution);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtProject_Location);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtProject_Name);
            this.Controls.Add(this.label6);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmNewProj";
            this.Text = "Create New Project";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.TextBox txtProject_Location;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtProject_Name;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox pretrained_solution;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtModelName;
        private System.Windows.Forms.Label label3;
    }
}
