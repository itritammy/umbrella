﻿namespace class_trainer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab_step1 = new System.Windows.Forms.TabPage();
            this.dgview_labels = new System.Windows.Forms.DataGridView();
            this.label_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label_code = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label9 = new System.Windows.Forms.Label();
            this.txtTrainingPercentage = new System.Windows.Forms.TextBox();
            this.btnNext = new System.Windows.Forms.Button();
            this.lstFileListOutput = new System.Windows.Forms.ListBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnGenerateList = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTestPercentage = new System.Windows.Forms.TextBox();
            this.txtValidatoinPercentage = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnImagesBrowse = new System.Windows.Forms.Button();
            this.txtImagesFolder = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tab_step2 = new System.Windows.Forms.TabPage();
            this.cmb_top_N_model = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.label60 = new System.Windows.Forms.Label();
            this.btnStep2Next = new System.Windows.Forms.Button();
            this.chk_GPU7 = new System.Windows.Forms.CheckBox();
            this.chk_GPU6 = new System.Windows.Forms.CheckBox();
            this.chk_GPU5 = new System.Windows.Forms.CheckBox();
            this.chk_GPU4 = new System.Windows.Forms.CheckBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.panel_augmentation = new System.Windows.Forms.Panel();
            this.label57 = new System.Windows.Forms.Label();
            this.cmb_pseudoBalanceFactor = new System.Windows.Forms.ComboBox();
            this.cmb_downsampling_factor = new System.Windows.Forms.ComboBox();
            this.label58 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.textDeltaWidthHeight_training = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.textNewSize_validation = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textNewSize_training = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.textCropSize_validation = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.textCropSize_training = new System.Windows.Forms.TextBox();
            this.ckb_Rotation = new System.Windows.Forms.CheckBox();
            this.ckb_VerticalFlip = new System.Windows.Forms.CheckBox();
            this.ckb_HorizontalFilp = new System.Windows.Forms.CheckBox();
            this.btn_cancel_mean_calc = new System.Windows.Forms.Button();
            this.progressBar_calcMean = new System.Windows.Forms.ProgressBar();
            this.ckb_advanced = new System.Windows.Forms.CheckBox();
            this.panel_advanced = new System.Windows.Forms.Panel();
            this.btn_imagePreProcessing = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.txtGamma = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtBaseLearningRate = new System.Windows.Forms.TextBox();
            this.txtMomentum = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtWeightDecay = new System.Windows.Forms.TextBox();
            this.txt_max_iterations = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.txtTestingImageCount = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtValidationImageCount = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.btn_generate_mean = new System.Windows.Forms.Button();
            this.txtValidationBatchSize = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.txtTrainingBatchSize = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSnapshotInverval = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.txtTrainingEpochs = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.chk_GPU3 = new System.Windows.Forms.CheckBox();
            this.chk_GPU2 = new System.Windows.Forms.CheckBox();
            this.chk_GPU1 = new System.Windows.Forms.CheckBox();
            this.chk_GPU0 = new System.Windows.Forms.CheckBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtTrainingImageCount = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMeanValue_B = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtMeanValue_G = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtMeanValue_R = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tab_step3 = new System.Windows.Forms.TabPage();
            this.label29 = new System.Windows.Forms.Label();
            this.btn_open_optimal_folder = new System.Windows.Forms.Button();
            this.lab_timeInfo = new System.Windows.Forms.Label();
            this.btn_output = new System.Windows.Forms.Button();
            this.cmb_prebuilt_model = new System.Windows.Forms.ComboBox();
            this.txt_curr_iterations = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txt_curr_validation_loss = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txt_curr_training_loss = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.btn_browse_prebuilt_file = new System.Windows.Forms.Button();
            this.txt_prebuilt_file = new System.Windows.Forms.TextBox();
            this.lab_training_status = new System.Windows.Forms.Label();
            this.progressBar_data = new System.Windows.Forms.ProgressBar();
            this.btn_stopTraining = new System.Windows.Forms.Button();
            this.btn_startTraining = new System.Windows.Forms.Button();
            this.chart_loss = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tab_confusion = new System.Windows.Forms.TabPage();
            this.btn_misclassified_move_test = new System.Windows.Forms.Button();
            this.btn_misclassified_export_browse_test = new System.Windows.Forms.Button();
            this.txt_misclassified_export_dir_test = new System.Windows.Forms.TextBox();
            this.label64 = new System.Windows.Forms.Label();
            this.btn_misclassified_move_val = new System.Windows.Forms.Button();
            this.btn_misclassified_export_browse_val = new System.Windows.Forms.Button();
            this.txt_misclassified_export_dir_val = new System.Windows.Forms.TextBox();
            this.label63 = new System.Windows.Forms.Label();
            this.btn_misclassified_move_train = new System.Windows.Forms.Button();
            this.btn_misclassified_export_browse_train = new System.Windows.Forms.Button();
            this.txt_misclassified_export_dir_train = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.lab_accuracy_testing = new System.Windows.Forms.Label();
            this.lab_accuracy_validation = new System.Windows.Forms.Label();
            this.chk_confusion_autofit_testing = new System.Windows.Forms.CheckBox();
            this.chk_confusion_autofit_validation = new System.Windows.Forms.CheckBox();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.button1btn_run_confusion_mat_testing = new System.Windows.Forms.Button();
            this.btn_run_confusion_mat_validation = new System.Windows.Forms.Button();
            this.dgv_confusion_matrix_testing = new System.Windows.Forms.DataGridView();
            this.dgv_confusion_matrix_validation = new System.Windows.Forms.DataGridView();
            this.btn_run_confusion_mat = new System.Windows.Forms.Button();
            this.chk_confusion_autofit_column = new System.Windows.Forms.CheckBox();
            this.lab_accuracy_training = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgv_confusion_matrix_training = new System.Windows.Forms.DataGridView();
            this.tab_threshold = new System.Windows.Forms.TabPage();
            this.chk_threshold_adjust = new System.Windows.Forms.CheckBox();
            this.btn_generate_param_file = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.dgv_threshold = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tab_inference = new System.Windows.Forms.TabPage();
            this.mtxt_batch_size_of_inference = new System.Windows.Forms.MaskedTextBox();
            this.mtxt_threshold_for_inference = new System.Windows.Forms.MaskedTextBox();
            this.btn_run_inference = new System.Windows.Forms.Button();
            this.gbox_pred_accuracy = new System.Windows.Forms.GroupBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label_target_image = new System.Windows.Forms.Label();
            this.pic_inference_image = new System.Windows.Forms.PictureBox();
            this.dgv_inference_result = new System.Windows.Forms.DataGridView();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.cbox_gpu_list = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_browse_inference_folder = new System.Windows.Forms.Button();
            this.txt_inference_target_folder = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.cbox_ground_truth = new System.Windows.Forms.ComboBox();
            this.label31 = new System.Windows.Forms.Label();
            this.tab_presorting = new System.Windows.Forms.TabPage();
            this.label48 = new System.Windows.Forms.Label();
            this.btn_check_dest_folder = new System.Windows.Forms.Button();
            this.mtxt_batch_size_of_presorting = new System.Windows.Forms.MaskedTextBox();
            this.mtxt_threshold_for_presorting = new System.Windows.Forms.MaskedTextBox();
            this.btn_run_presorting = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.cbox_presorting_gpu_list = new System.Windows.Forms.ComboBox();
            this.label47 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btn_browse_src = new System.Windows.Forms.Button();
            this.txt_presorting_src_folder = new System.Windows.Forms.TextBox();
            this.btn_browse_dest = new System.Windows.Forms.Button();
            this.txt_presorting_dest_folder = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.createNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.applicationLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dNNLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activeLearningRelabelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1.SuspendLayout();
            this.tab_step1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgview_labels)).BeginInit();
            this.tab_step2.SuspendLayout();
            this.panel_augmentation.SuspendLayout();
            this.panel_advanced.SuspendLayout();
            this.tab_step3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_loss)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tab_confusion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_confusion_matrix_testing)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_confusion_matrix_validation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_confusion_matrix_training)).BeginInit();
            this.tab_threshold.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_threshold)).BeginInit();
            this.tab_inference.SuspendLayout();
            this.gbox_pred_accuracy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_inference_image)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_inference_result)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tab_presorting.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab_step1);
            this.tabControl1.Controls.Add(this.tab_step2);
            this.tabControl1.Controls.Add(this.tab_step3);
            this.tabControl1.Controls.Add(this.tab_confusion);
            this.tabControl1.Controls.Add(this.tab_threshold);
            this.tabControl1.Controls.Add(this.tab_inference);
            this.tabControl1.Controls.Add(this.tab_presorting);
            this.tabControl1.Location = new System.Drawing.Point(0, 27);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(728, 475);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.Visible = false;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            this.tabControl1.Deselecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Deselecting);
            // 
            // tab_step1
            // 
            this.tab_step1.Controls.Add(this.dgview_labels);
            this.tab_step1.Controls.Add(this.label9);
            this.tab_step1.Controls.Add(this.txtTrainingPercentage);
            this.tab_step1.Controls.Add(this.btnNext);
            this.tab_step1.Controls.Add(this.lstFileListOutput);
            this.tab_step1.Controls.Add(this.label5);
            this.tab_step1.Controls.Add(this.btnGenerateList);
            this.tab_step1.Controls.Add(this.label4);
            this.tab_step1.Controls.Add(this.txtTestPercentage);
            this.tab_step1.Controls.Add(this.txtValidatoinPercentage);
            this.tab_step1.Controls.Add(this.label3);
            this.tab_step1.Controls.Add(this.label2);
            this.tab_step1.Controls.Add(this.btnImagesBrowse);
            this.tab_step1.Controls.Add(this.txtImagesFolder);
            this.tab_step1.Controls.Add(this.label1);
            this.tab_step1.Location = new System.Drawing.Point(4, 22);
            this.tab_step1.Name = "tab_step1";
            this.tab_step1.Padding = new System.Windows.Forms.Padding(3);
            this.tab_step1.Size = new System.Drawing.Size(720, 449);
            this.tab_step1.TabIndex = 0;
            this.tab_step1.Text = "STEP1:  Create dataset";
            this.tab_step1.UseVisualStyleBackColor = true;
            // 
            // dgview_labels
            // 
            this.dgview_labels.AllowDrop = true;
            this.dgview_labels.AllowUserToAddRows = false;
            this.dgview_labels.AllowUserToDeleteRows = false;
            this.dgview_labels.AllowUserToResizeRows = false;
            this.dgview_labels.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgview_labels.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.label_name,
            this.label_code});
            this.dgview_labels.Location = new System.Drawing.Point(14, 92);
            this.dgview_labels.MultiSelect = false;
            this.dgview_labels.Name = "dgview_labels";
            this.dgview_labels.RowHeadersWidth = 48;
            this.dgview_labels.RowTemplate.Height = 24;
            this.dgview_labels.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgview_labels.Size = new System.Drawing.Size(601, 183);
            this.dgview_labels.TabIndex = 49;
            this.dgview_labels.DragDrop += new System.Windows.Forms.DragEventHandler(this.dgview_labels_DragDrop);
            this.dgview_labels.DragOver += new System.Windows.Forms.DragEventHandler(this.dgview_labels_DragOver);
            this.dgview_labels.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dgview_labels_MouseDown);
            this.dgview_labels.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dgview_labels_MouseMove);
            // 
            // label_name
            // 
            this.label_name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.label_name.HeaderText = "Class Name";
            this.label_name.MinimumWidth = 6;
            this.label_name.Name = "label_name";
            this.label_name.ReadOnly = true;
            this.label_name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.label_name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // label_code
            // 
            this.label_code.HeaderText = "Code";
            this.label_code.MinimumWidth = 6;
            this.label_code.Name = "label_code";
            this.label_code.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.label_code.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.label_code.Width = 125;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(282, 292);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(79, 13);
            this.label9.TabIndex = 48;
            this.label9.Text = "% for training :";
            // 
            // txtTrainingPercentage
            // 
            this.txtTrainingPercentage.Enabled = false;
            this.txtTrainingPercentage.Location = new System.Drawing.Point(284, 307);
            this.txtTrainingPercentage.Name = "txtTrainingPercentage";
            this.txtTrainingPercentage.Size = new System.Drawing.Size(101, 22);
            this.txtTrainingPercentage.TabIndex = 47;
            this.txtTrainingPercentage.Text = "10";
            // 
            // btnNext
            // 
            this.btnNext.Location = new System.Drawing.Point(628, 422);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(75, 23);
            this.btnNext.TabIndex = 0;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // lstFileListOutput
            // 
            this.lstFileListOutput.FormattingEnabled = true;
            this.lstFileListOutput.ItemHeight = 12;
            this.lstFileListOutput.Location = new System.Drawing.Point(13, 360);
            this.lstFileListOutput.Name = "lstFileListOutput";
            this.lstFileListOutput.Size = new System.Drawing.Size(602, 52);
            this.lstFileListOutput.TabIndex = 38;
            this.lstFileListOutput.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.lstFileListOutput_MouseDoubleClick);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 345);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Image File List :";
            // 
            // btnGenerateList
            // 
            this.btnGenerateList.Location = new System.Drawing.Point(628, 388);
            this.btnGenerateList.Name = "btnGenerateList";
            this.btnGenerateList.Size = new System.Drawing.Size(75, 23);
            this.btnGenerateList.TabIndex = 28;
            this.btnGenerateList.Text = "Generate";
            this.btnGenerateList.UseVisualStyleBackColor = true;
            this.btnGenerateList.Click += new System.EventHandler(this.btnGenerateList_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 77);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Class Found  :";
            // 
            // txtTestPercentage
            // 
            this.txtTestPercentage.Location = new System.Drawing.Point(14, 307);
            this.txtTestPercentage.Name = "txtTestPercentage";
            this.txtTestPercentage.Size = new System.Drawing.Size(101, 22);
            this.txtTestPercentage.TabIndex = 32;
            this.txtTestPercentage.Text = "15";
            this.txtTestPercentage.Leave += new System.EventHandler(this.txtTestPercentage_Leave);
            // 
            // txtValidatoinPercentage
            // 
            this.txtValidatoinPercentage.Location = new System.Drawing.Point(149, 307);
            this.txtValidatoinPercentage.Name = "txtValidatoinPercentage";
            this.txtValidatoinPercentage.Size = new System.Drawing.Size(101, 22);
            this.txtValidatoinPercentage.TabIndex = 31;
            this.txtValidatoinPercentage.Text = "10";
            this.txtValidatoinPercentage.Leave += new System.EventHandler(this.txtValidatoinPercentage_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 292);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(74, 13);
            this.label3.TabIndex = 30;
            this.label3.Text = "% for testing :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(147, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 13);
            this.label2.TabIndex = 29;
            this.label2.Text = "% for validation :";
            // 
            // btnImagesBrowse
            // 
            this.btnImagesBrowse.Location = new System.Drawing.Point(628, 41);
            this.btnImagesBrowse.Name = "btnImagesBrowse";
            this.btnImagesBrowse.Size = new System.Drawing.Size(75, 23);
            this.btnImagesBrowse.TabIndex = 27;
            this.btnImagesBrowse.Text = "Browse ...";
            this.btnImagesBrowse.UseVisualStyleBackColor = true;
            this.btnImagesBrowse.Click += new System.EventHandler(this.btnImagesBrowse_Click);
            // 
            // txtImagesFolder
            // 
            this.txtImagesFolder.Location = new System.Drawing.Point(13, 44);
            this.txtImagesFolder.Name = "txtImagesFolder";
            this.txtImagesFolder.Size = new System.Drawing.Size(602, 22);
            this.txtImagesFolder.TabIndex = 26;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(115, 13);
            this.label1.TabIndex = 25;
            this.label1.Text = "Source Images folder :";
            // 
            // tab_step2
            // 
            this.tab_step2.AutoScroll = true;
            this.tab_step2.Controls.Add(this.cmb_top_N_model);
            this.tab_step2.Controls.Add(this.label61);
            this.tab_step2.Controls.Add(this.label60);
            this.tab_step2.Controls.Add(this.btnStep2Next);
            this.tab_step2.Controls.Add(this.chk_GPU7);
            this.tab_step2.Controls.Add(this.chk_GPU6);
            this.tab_step2.Controls.Add(this.chk_GPU5);
            this.tab_step2.Controls.Add(this.chk_GPU4);
            this.tab_step2.Controls.Add(this.label55);
            this.tab_step2.Controls.Add(this.label42);
            this.tab_step2.Controls.Add(this.panel_augmentation);
            this.tab_step2.Controls.Add(this.btn_cancel_mean_calc);
            this.tab_step2.Controls.Add(this.progressBar_calcMean);
            this.tab_step2.Controls.Add(this.ckb_advanced);
            this.tab_step2.Controls.Add(this.panel_advanced);
            this.tab_step2.Controls.Add(this.txt_max_iterations);
            this.tab_step2.Controls.Add(this.label28);
            this.tab_step2.Controls.Add(this.txtTestingImageCount);
            this.tab_step2.Controls.Add(this.label27);
            this.tab_step2.Controls.Add(this.txtValidationImageCount);
            this.tab_step2.Controls.Add(this.label15);
            this.tab_step2.Controls.Add(this.btn_generate_mean);
            this.tab_step2.Controls.Add(this.txtValidationBatchSize);
            this.tab_step2.Controls.Add(this.label20);
            this.tab_step2.Controls.Add(this.txtTrainingBatchSize);
            this.tab_step2.Controls.Add(this.label19);
            this.tab_step2.Controls.Add(this.txtSnapshotInverval);
            this.tab_step2.Controls.Add(this.label18);
            this.tab_step2.Controls.Add(this.txtTrainingEpochs);
            this.tab_step2.Controls.Add(this.label17);
            this.tab_step2.Controls.Add(this.chk_GPU3);
            this.tab_step2.Controls.Add(this.chk_GPU2);
            this.tab_step2.Controls.Add(this.chk_GPU1);
            this.tab_step2.Controls.Add(this.chk_GPU0);
            this.tab_step2.Controls.Add(this.label16);
            this.tab_step2.Controls.Add(this.txtTrainingImageCount);
            this.tab_step2.Controls.Add(this.label14);
            this.tab_step2.Controls.Add(this.label13);
            this.tab_step2.Controls.Add(this.txtMeanValue_B);
            this.tab_step2.Controls.Add(this.label12);
            this.tab_step2.Controls.Add(this.txtMeanValue_G);
            this.tab_step2.Controls.Add(this.label11);
            this.tab_step2.Controls.Add(this.txtMeanValue_R);
            this.tab_step2.Controls.Add(this.label10);
            this.tab_step2.Location = new System.Drawing.Point(4, 22);
            this.tab_step2.Name = "tab_step2";
            this.tab_step2.Padding = new System.Windows.Forms.Padding(3);
            this.tab_step2.Size = new System.Drawing.Size(720, 449);
            this.tab_step2.TabIndex = 1;
            this.tab_step2.Text = "STEP2:  Set Training Parameters";
            this.tab_step2.UseVisualStyleBackColor = true;
            // 
            // cmb_top_N_model
            // 
            this.cmb_top_N_model.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_top_N_model.FormattingEnabled = true;
            this.cmb_top_N_model.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8"});
            this.cmb_top_N_model.Location = new System.Drawing.Point(10, 208);
            this.cmb_top_N_model.Name = "cmb_top_N_model";
            this.cmb_top_N_model.Size = new System.Drawing.Size(190, 20);
            this.cmb_top_N_model.TabIndex = 50;
            this.cmb_top_N_model.SelectedIndexChanged += new System.EventHandler(this.cmb_top_N_model_SelectedIndexChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(11, 193);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(118, 13);
            this.label61.TabIndex = 53;
            this.label61.Text = "Saving Top N models :";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(9, 18);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(107, 13);
            this.label60.TabIndex = 52;
            this.label60.Text = "Number of Images : ";
            // 
            // btnStep2Next
            // 
            this.btnStep2Next.Location = new System.Drawing.Point(626, 422);
            this.btnStep2Next.Name = "btnStep2Next";
            this.btnStep2Next.Size = new System.Drawing.Size(75, 23);
            this.btnStep2Next.TabIndex = 7;
            this.btnStep2Next.Text = "Next";
            this.btnStep2Next.UseVisualStyleBackColor = true;
            this.btnStep2Next.Click += new System.EventHandler(this.btnStep2Next_Click);
            // 
            // chk_GPU7
            // 
            this.chk_GPU7.AutoSize = true;
            this.chk_GPU7.Location = new System.Drawing.Point(11, 422);
            this.chk_GPU7.Name = "chk_GPU7";
            this.chk_GPU7.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU7.TabIndex = 51;
            this.chk_GPU7.Text = "GPU7";
            this.chk_GPU7.UseVisualStyleBackColor = true;
            this.chk_GPU7.Visible = false;
            // 
            // chk_GPU6
            // 
            this.chk_GPU6.AutoSize = true;
            this.chk_GPU6.Location = new System.Drawing.Point(11, 399);
            this.chk_GPU6.Name = "chk_GPU6";
            this.chk_GPU6.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU6.TabIndex = 50;
            this.chk_GPU6.Text = "GPU6";
            this.chk_GPU6.UseVisualStyleBackColor = true;
            this.chk_GPU6.Visible = false;
            // 
            // chk_GPU5
            // 
            this.chk_GPU5.AutoSize = true;
            this.chk_GPU5.Location = new System.Drawing.Point(11, 376);
            this.chk_GPU5.Name = "chk_GPU5";
            this.chk_GPU5.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU5.TabIndex = 49;
            this.chk_GPU5.Text = "GPU5";
            this.chk_GPU5.UseVisualStyleBackColor = true;
            this.chk_GPU5.Visible = false;
            // 
            // chk_GPU4
            // 
            this.chk_GPU4.AutoSize = true;
            this.chk_GPU4.Location = new System.Drawing.Point(11, 353);
            this.chk_GPU4.Name = "chk_GPU4";
            this.chk_GPU4.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU4.TabIndex = 48;
            this.chk_GPU4.Text = "GPU4";
            this.chk_GPU4.UseVisualStyleBackColor = true;
            this.chk_GPU4.Visible = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(222, 219);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(95, 13);
            this.label55.TabIndex = 47;
            this.label55.Text = "Basic Parameters :";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(222, 18);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(106, 13);
            this.label42.TabIndex = 46;
            this.label42.Text = "Data Augmentation :";
            // 
            // panel_augmentation
            // 
            this.panel_augmentation.BackColor = System.Drawing.Color.Transparent;
            this.panel_augmentation.Controls.Add(this.label57);
            this.panel_augmentation.Controls.Add(this.cmb_pseudoBalanceFactor);
            this.panel_augmentation.Controls.Add(this.cmb_downsampling_factor);
            this.panel_augmentation.Controls.Add(this.label58);
            this.panel_augmentation.Controls.Add(this.label56);
            this.panel_augmentation.Controls.Add(this.textDeltaWidthHeight_training);
            this.panel_augmentation.Controls.Add(this.label53);
            this.panel_augmentation.Controls.Add(this.textNewSize_validation);
            this.panel_augmentation.Controls.Add(this.label54);
            this.panel_augmentation.Controls.Add(this.textNewSize_training);
            this.panel_augmentation.Controls.Add(this.label52);
            this.panel_augmentation.Controls.Add(this.textCropSize_validation);
            this.panel_augmentation.Controls.Add(this.label51);
            this.panel_augmentation.Controls.Add(this.textCropSize_training);
            this.panel_augmentation.Controls.Add(this.ckb_Rotation);
            this.panel_augmentation.Controls.Add(this.ckb_VerticalFlip);
            this.panel_augmentation.Controls.Add(this.ckb_HorizontalFilp);
            this.panel_augmentation.Location = new System.Drawing.Point(208, 32);
            this.panel_augmentation.Margin = new System.Windows.Forms.Padding(2);
            this.panel_augmentation.Name = "panel_augmentation";
            this.panel_augmentation.Size = new System.Drawing.Size(510, 179);
            this.panel_augmentation.TabIndex = 45;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(192, 84);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(108, 13);
            this.label57.TabIndex = 49;
            this.label57.Text = "Data Balance Factor :";
            // 
            // cmb_pseudoBalanceFactor
            // 
            this.cmb_pseudoBalanceFactor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_pseudoBalanceFactor.FormattingEnabled = true;
            this.cmb_pseudoBalanceFactor.Items.AddRange(new object[] {
            "0",
            "0.01",
            "0.05",
            "0.1",
            "0.15",
            "0.2",
            "0.25",
            "0.3",
            "0.35"});
            this.cmb_pseudoBalanceFactor.Location = new System.Drawing.Point(193, 100);
            this.cmb_pseudoBalanceFactor.Name = "cmb_pseudoBalanceFactor";
            this.cmb_pseudoBalanceFactor.Size = new System.Drawing.Size(130, 20);
            this.cmb_pseudoBalanceFactor.TabIndex = 47;
            this.cmb_pseudoBalanceFactor.SelectedIndexChanged += new System.EventHandler(this.cmb_pseudoBalanceFactor_SelectedIndexChanged);
            // 
            // cmb_downsampling_factor
            // 
            this.cmb_downsampling_factor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_downsampling_factor.FormattingEnabled = true;
            this.cmb_downsampling_factor.Items.AddRange(new object[] {
            "16",
            "32"});
            this.cmb_downsampling_factor.Location = new System.Drawing.Point(374, 49);
            this.cmb_downsampling_factor.Name = "cmb_downsampling_factor";
            this.cmb_downsampling_factor.Size = new System.Drawing.Size(130, 20);
            this.cmb_downsampling_factor.TabIndex = 45;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(372, 34);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(120, 13);
            this.label58.TabIndex = 41;
            this.label58.Text = "Downsampling Factor :";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(12, 84);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(129, 13);
            this.label56.TabIndex = 37;
            this.label56.Text = "Image Distortion (W/H) :";
            this.toolTip1.SetToolTip(this.label56, "Distortion Range (W/H) for Training Images  in pixels");
            // 
            // textDeltaWidthHeight_training
            // 
            this.textDeltaWidthHeight_training.Location = new System.Drawing.Point(13, 100);
            this.textDeltaWidthHeight_training.Name = "textDeltaWidthHeight_training";
            this.textDeltaWidthHeight_training.Size = new System.Drawing.Size(130, 22);
            this.textDeltaWidthHeight_training.TabIndex = 38;
            this.textDeltaWidthHeight_training.Text = "5";
            this.toolTip1.SetToolTip(this.textDeltaWidthHeight_training, "Distortion Range (W/H) for Training Images  in pixels");
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(192, 135);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(181, 13);
            this.label53.TabIndex = 35;
            this.label53.Text = "Image Resize (W/H) for Validation :";
            this.toolTip1.SetToolTip(this.label53, "in pixels");
            // 
            // textNewSize_validation
            // 
            this.textNewSize_validation.Location = new System.Drawing.Point(194, 151);
            this.textNewSize_validation.Name = "textNewSize_validation";
            this.textNewSize_validation.Size = new System.Drawing.Size(130, 22);
            this.textNewSize_validation.TabIndex = 36;
            this.textNewSize_validation.Text = "122";
            this.toolTip1.SetToolTip(this.textNewSize_validation, "in pixels");
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(11, 135);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(172, 13);
            this.label54.TabIndex = 33;
            this.label54.Text = "Image Resize (W/H) for Training :";
            this.toolTip1.SetToolTip(this.label54, "in pixels");
            // 
            // textNewSize_training
            // 
            this.textNewSize_training.Location = new System.Drawing.Point(13, 151);
            this.textNewSize_training.Name = "textNewSize_training";
            this.textNewSize_training.Size = new System.Drawing.Size(130, 22);
            this.textNewSize_training.TabIndex = 34;
            this.textNewSize_training.Text = "144";
            this.toolTip1.SetToolTip(this.textNewSize_training, "in pixels");
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(191, 33);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(164, 13);
            this.label52.TabIndex = 31;
            this.label52.Text = "Image (W/H) for Validation Net:";
            this.toolTip1.SetToolTip(this.label52, "in pixels");
            // 
            // textCropSize_validation
            // 
            this.textCropSize_validation.Location = new System.Drawing.Point(193, 49);
            this.textCropSize_validation.Name = "textCropSize_validation";
            this.textCropSize_validation.Size = new System.Drawing.Size(130, 22);
            this.textCropSize_validation.TabIndex = 32;
            this.textCropSize_validation.Text = "128";
            this.toolTip1.SetToolTip(this.textCropSize_validation, "in pixels");
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(14, 34);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(158, 13);
            this.label51.TabIndex = 29;
            this.label51.Text = "Image (W/H) for Training Net :";
            this.toolTip1.SetToolTip(this.label51, "in pixels");
            // 
            // textCropSize_training
            // 
            this.textCropSize_training.Location = new System.Drawing.Point(14, 49);
            this.textCropSize_training.Name = "textCropSize_training";
            this.textCropSize_training.Size = new System.Drawing.Size(130, 22);
            this.textCropSize_training.TabIndex = 30;
            this.textCropSize_training.Text = "112";
            this.toolTip1.SetToolTip(this.textCropSize_training, "in pixels");
            // 
            // ckb_Rotation
            // 
            this.ckb_Rotation.AutoSize = true;
            this.ckb_Rotation.Location = new System.Drawing.Point(374, 8);
            this.ckb_Rotation.Margin = new System.Windows.Forms.Padding(2);
            this.ckb_Rotation.Name = "ckb_Rotation";
            this.ckb_Rotation.Size = new System.Drawing.Size(69, 17);
            this.ckb_Rotation.TabIndex = 2;
            this.ckb_Rotation.Text = "Rotation";
            this.ckb_Rotation.UseVisualStyleBackColor = true;
            // 
            // ckb_VerticalFlip
            // 
            this.ckb_VerticalFlip.AutoSize = true;
            this.ckb_VerticalFlip.Location = new System.Drawing.Point(194, 8);
            this.ckb_VerticalFlip.Margin = new System.Windows.Forms.Padding(2);
            this.ckb_VerticalFlip.Name = "ckb_VerticalFlip";
            this.ckb_VerticalFlip.Size = new System.Drawing.Size(88, 17);
            this.ckb_VerticalFlip.TabIndex = 1;
            this.ckb_VerticalFlip.Text = "Vertical Flip";
            this.ckb_VerticalFlip.UseVisualStyleBackColor = true;
            // 
            // ckb_HorizontalFilp
            // 
            this.ckb_HorizontalFilp.AutoSize = true;
            this.ckb_HorizontalFilp.Location = new System.Drawing.Point(14, 8);
            this.ckb_HorizontalFilp.Margin = new System.Windows.Forms.Padding(2);
            this.ckb_HorizontalFilp.Name = "ckb_HorizontalFilp";
            this.ckb_HorizontalFilp.Size = new System.Drawing.Size(101, 17);
            this.ckb_HorizontalFilp.TabIndex = 0;
            this.ckb_HorizontalFilp.Text = "Horizontal Flip";
            this.ckb_HorizontalFilp.UseVisualStyleBackColor = true;
            // 
            // btn_cancel_mean_calc
            // 
            this.btn_cancel_mean_calc.Enabled = false;
            this.btn_cancel_mean_calc.Location = new System.Drawing.Point(151, 117);
            this.btn_cancel_mean_calc.Name = "btn_cancel_mean_calc";
            this.btn_cancel_mean_calc.Size = new System.Drawing.Size(51, 23);
            this.btn_cancel_mean_calc.TabIndex = 41;
            this.btn_cancel_mean_calc.Text = "Cancel";
            this.btn_cancel_mean_calc.UseVisualStyleBackColor = true;
            this.btn_cancel_mean_calc.Click += new System.EventHandler(this.btn_cancel_mean_calc_Click);
            // 
            // progressBar_calcMean
            // 
            this.progressBar_calcMean.Location = new System.Drawing.Point(12, 174);
            this.progressBar_calcMean.Name = "progressBar_calcMean";
            this.progressBar_calcMean.Size = new System.Drawing.Size(190, 10);
            this.progressBar_calcMean.TabIndex = 40;
            // 
            // ckb_advanced
            // 
            this.ckb_advanced.AutoSize = true;
            this.ckb_advanced.Location = new System.Drawing.Point(222, 337);
            this.ckb_advanced.Margin = new System.Windows.Forms.Padding(2);
            this.ckb_advanced.Name = "ckb_advanced";
            this.ckb_advanced.Size = new System.Drawing.Size(139, 17);
            this.ckb_advanced.TabIndex = 39;
            this.ckb_advanced.Text = "Advanced Parameters :";
            this.ckb_advanced.UseVisualStyleBackColor = true;
            this.ckb_advanced.CheckedChanged += new System.EventHandler(this.ckb_advanced_CheckedChanged);
            // 
            // panel_advanced
            // 
            this.panel_advanced.BackColor = System.Drawing.Color.Transparent;
            this.panel_advanced.Controls.Add(this.btn_imagePreProcessing);
            this.panel_advanced.Controls.Add(this.label59);
            this.panel_advanced.Controls.Add(this.txtGamma);
            this.panel_advanced.Controls.Add(this.label23);
            this.panel_advanced.Controls.Add(this.label22);
            this.panel_advanced.Controls.Add(this.txtBaseLearningRate);
            this.panel_advanced.Controls.Add(this.txtMomentum);
            this.panel_advanced.Controls.Add(this.label21);
            this.panel_advanced.Controls.Add(this.txtWeightDecay);
            this.panel_advanced.Location = new System.Drawing.Point(208, 357);
            this.panel_advanced.Margin = new System.Windows.Forms.Padding(2);
            this.panel_advanced.Name = "panel_advanced";
            this.panel_advanced.Size = new System.Drawing.Size(510, 87);
            this.panel_advanced.TabIndex = 38;
            this.panel_advanced.Visible = false;
            // 
            // btn_imagePreProcessing
            // 
            this.btn_imagePreProcessing.Location = new System.Drawing.Point(13, 58);
            this.btn_imagePreProcessing.Name = "btn_imagePreProcessing";
            this.btn_imagePreProcessing.Size = new System.Drawing.Size(129, 23);
            this.btn_imagePreProcessing.TabIndex = 57;
            this.btn_imagePreProcessing.Text = "Image Pre-Processing ...";
            this.btn_imagePreProcessing.UseVisualStyleBackColor = true;
            this.btn_imagePreProcessing.Visible = false;
            this.btn_imagePreProcessing.Click += new System.EventHandler(this.btn_imagePreProcessing_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(414, 6);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(52, 13);
            this.label59.TabIndex = 31;
            this.label59.Text = "Gamma :";
            // 
            // txtGamma
            // 
            this.txtGamma.Location = new System.Drawing.Point(416, 20);
            this.txtGamma.Name = "txtGamma";
            this.txtGamma.Size = new System.Drawing.Size(85, 22);
            this.txtGamma.TabIndex = 32;
            this.txtGamma.Text = "127";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(146, 5);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(70, 13);
            this.label23.TabIndex = 29;
            this.label23.Text = "Momentum :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(14, 6);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(105, 13);
            this.label22.TabIndex = 27;
            this.label22.Text = "Base Learning Rate :";
            // 
            // txtBaseLearningRate
            // 
            this.txtBaseLearningRate.Location = new System.Drawing.Point(16, 21);
            this.txtBaseLearningRate.Name = "txtBaseLearningRate";
            this.txtBaseLearningRate.Size = new System.Drawing.Size(85, 22);
            this.txtBaseLearningRate.TabIndex = 28;
            this.txtBaseLearningRate.Text = "127";
            // 
            // txtMomentum
            // 
            this.txtMomentum.Location = new System.Drawing.Point(148, 21);
            this.txtMomentum.Name = "txtMomentum";
            this.txtMomentum.Size = new System.Drawing.Size(87, 22);
            this.txtMomentum.TabIndex = 30;
            this.txtMomentum.Text = "127";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(280, 5);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(81, 13);
            this.label21.TabIndex = 25;
            this.label21.Text = "Weight Decay :";
            // 
            // txtWeightDecay
            // 
            this.txtWeightDecay.Location = new System.Drawing.Point(282, 21);
            this.txtWeightDecay.Name = "txtWeightDecay";
            this.txtWeightDecay.Size = new System.Drawing.Size(87, 22);
            this.txtWeightDecay.TabIndex = 26;
            this.txtWeightDecay.Text = "127";
            // 
            // txt_max_iterations
            // 
            this.txt_max_iterations.Enabled = false;
            this.txt_max_iterations.Location = new System.Drawing.Point(223, 253);
            this.txt_max_iterations.Name = "txt_max_iterations";
            this.txt_max_iterations.Size = new System.Drawing.Size(130, 22);
            this.txt_max_iterations.TabIndex = 37;
            this.txt_max_iterations.Text = "0";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(222, 237);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(111, 13);
            this.label28.TabIndex = 36;
            this.label28.Text = "Maximum Iterations :";
            // 
            // txtTestingImageCount
            // 
            this.txtTestingImageCount.Enabled = false;
            this.txtTestingImageCount.Location = new System.Drawing.Point(86, 89);
            this.txtTestingImageCount.Name = "txtTestingImageCount";
            this.txtTestingImageCount.Size = new System.Drawing.Size(115, 22);
            this.txtTestingImageCount.TabIndex = 35;
            this.txtTestingImageCount.Text = "0";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(9, 92);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(48, 13);
            this.label27.TabIndex = 34;
            this.label27.Text = "Testing :";
            // 
            // txtValidationImageCount
            // 
            this.txtValidationImageCount.Enabled = false;
            this.txtValidationImageCount.Location = new System.Drawing.Point(86, 61);
            this.txtValidationImageCount.Name = "txtValidationImageCount";
            this.txtValidationImageCount.Size = new System.Drawing.Size(117, 22);
            this.txtValidationImageCount.TabIndex = 33;
            this.txtValidationImageCount.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(8, 66);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(62, 13);
            this.label15.TabIndex = 32;
            this.label15.Text = "Validation :";
            // 
            // btn_generate_mean
            // 
            this.btn_generate_mean.Location = new System.Drawing.Point(86, 117);
            this.btn_generate_mean.Name = "btn_generate_mean";
            this.btn_generate_mean.Size = new System.Drawing.Size(59, 23);
            this.btn_generate_mean.TabIndex = 31;
            this.btn_generate_mean.Text = "Genereate";
            this.btn_generate_mean.UseVisualStyleBackColor = true;
            this.btn_generate_mean.Click += new System.EventHandler(this.btn_generate_mean_Click);
            // 
            // txtValidationBatchSize
            // 
            this.txtValidationBatchSize.Location = new System.Drawing.Point(582, 253);
            this.txtValidationBatchSize.Name = "txtValidationBatchSize";
            this.txtValidationBatchSize.Size = new System.Drawing.Size(130, 22);
            this.txtValidationBatchSize.TabIndex = 5;
            this.txtValidationBatchSize.Text = "127";
            this.toolTip1.SetToolTip(this.txtValidationBatchSize, "in number of images");
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(580, 237);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 13);
            this.label20.TabIndex = 23;
            this.label20.Text = "Validation Batch Size :";
            this.toolTip1.SetToolTip(this.label20, "in number of images");
            // 
            // txtTrainingBatchSize
            // 
            this.txtTrainingBatchSize.Location = new System.Drawing.Point(402, 250);
            this.txtTrainingBatchSize.Name = "txtTrainingBatchSize";
            this.txtTrainingBatchSize.Size = new System.Drawing.Size(130, 22);
            this.txtTrainingBatchSize.TabIndex = 4;
            this.txtTrainingBatchSize.Text = "127";
            this.toolTip1.SetToolTip(this.txtTrainingBatchSize, "in number of images");
            this.txtTrainingBatchSize.TextChanged += new System.EventHandler(this.txtTrainingBatchSize_TextChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(400, 237);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(106, 13);
            this.label19.TabIndex = 21;
            this.label19.Text = "Training Batch Size :";
            this.toolTip1.SetToolTip(this.label19, "in number of images");
            // 
            // txtSnapshotInverval
            // 
            this.txtSnapshotInverval.Location = new System.Drawing.Point(402, 303);
            this.txtSnapshotInverval.Name = "txtSnapshotInverval";
            this.txtSnapshotInverval.Size = new System.Drawing.Size(130, 22);
            this.txtSnapshotInverval.TabIndex = 6;
            this.txtSnapshotInverval.Text = "127";
            this.toolTip1.SetToolTip(this.txtSnapshotInverval, " in iterations ");
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(400, 287);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(96, 13);
            this.label18.TabIndex = 19;
            this.label18.Text = "Snapshot Interval :";
            this.toolTip1.SetToolTip(this.label18, " in iterations ");
            // 
            // txtTrainingEpochs
            // 
            this.txtTrainingEpochs.Location = new System.Drawing.Point(223, 303);
            this.txtTrainingEpochs.Name = "txtTrainingEpochs";
            this.txtTrainingEpochs.Size = new System.Drawing.Size(130, 22);
            this.txtTrainingEpochs.TabIndex = 3;
            this.txtTrainingEpochs.Text = "127";
            this.txtTrainingEpochs.TextChanged += new System.EventHandler(this.txtTrainingEpochs_TextChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(221, 287);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(91, 13);
            this.label17.TabIndex = 17;
            this.label17.Text = "Training Epochs :";
            // 
            // chk_GPU3
            // 
            this.chk_GPU3.AutoSize = true;
            this.chk_GPU3.Location = new System.Drawing.Point(11, 330);
            this.chk_GPU3.Name = "chk_GPU3";
            this.chk_GPU3.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU3.TabIndex = 16;
            this.chk_GPU3.Text = "GPU3";
            this.chk_GPU3.UseVisualStyleBackColor = true;
            this.chk_GPU3.Visible = false;
            this.chk_GPU3.CheckedChanged += new System.EventHandler(this.chk_GPU3_CheckedChanged);
            // 
            // chk_GPU2
            // 
            this.chk_GPU2.AutoSize = true;
            this.chk_GPU2.Location = new System.Drawing.Point(11, 307);
            this.chk_GPU2.Name = "chk_GPU2";
            this.chk_GPU2.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU2.TabIndex = 15;
            this.chk_GPU2.Text = "GPU2";
            this.chk_GPU2.UseVisualStyleBackColor = true;
            this.chk_GPU2.Visible = false;
            this.chk_GPU2.CheckedChanged += new System.EventHandler(this.chk_GPU2_CheckedChanged);
            // 
            // chk_GPU1
            // 
            this.chk_GPU1.AutoSize = true;
            this.chk_GPU1.Location = new System.Drawing.Point(11, 284);
            this.chk_GPU1.Name = "chk_GPU1";
            this.chk_GPU1.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU1.TabIndex = 14;
            this.chk_GPU1.Text = "GPU1";
            this.chk_GPU1.UseVisualStyleBackColor = true;
            this.chk_GPU1.Visible = false;
            this.chk_GPU1.CheckedChanged += new System.EventHandler(this.chk_GPU1_CheckedChanged);
            // 
            // chk_GPU0
            // 
            this.chk_GPU0.AutoSize = true;
            this.chk_GPU0.Location = new System.Drawing.Point(11, 261);
            this.chk_GPU0.Name = "chk_GPU0";
            this.chk_GPU0.Size = new System.Drawing.Size(60, 17);
            this.chk_GPU0.TabIndex = 13;
            this.chk_GPU0.Text = "GPU0";
            this.chk_GPU0.UseVisualStyleBackColor = true;
            this.chk_GPU0.Visible = false;
            this.chk_GPU0.CheckedChanged += new System.EventHandler(this.chk_GPU0_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 240);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(69, 13);
            this.label16.TabIndex = 12;
            this.label16.Text = "GPU to use :";
            // 
            // txtTrainingImageCount
            // 
            this.txtTrainingImageCount.Enabled = false;
            this.txtTrainingImageCount.Location = new System.Drawing.Point(86, 33);
            this.txtTrainingImageCount.Name = "txtTrainingImageCount";
            this.txtTrainingImageCount.Size = new System.Drawing.Size(117, 22);
            this.txtTrainingImageCount.TabIndex = 9;
            this.txtTrainingImageCount.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(10, 36);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(53, 13);
            this.label14.TabIndex = 8;
            this.label14.Text = "Training :";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(142, 149);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(15, 13);
            this.label13.TabIndex = 7;
            this.label13.Text = "B";
            // 
            // txtMeanValue_B
            // 
            this.txtMeanValue_B.Location = new System.Drawing.Point(159, 143);
            this.txtMeanValue_B.Name = "txtMeanValue_B";
            this.txtMeanValue_B.Size = new System.Drawing.Size(41, 22);
            this.txtMeanValue_B.TabIndex = 2;
            this.txtMeanValue_B.Text = "0";
            this.txtMeanValue_B.Validating += new System.ComponentModel.CancelEventHandler(this.txtMeanValue_B_Validating);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(75, 148);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(16, 13);
            this.label12.TabIndex = 5;
            this.label12.Text = "G";
            // 
            // txtMeanValue_G
            // 
            this.txtMeanValue_G.Location = new System.Drawing.Point(94, 144);
            this.txtMeanValue_G.Name = "txtMeanValue_G";
            this.txtMeanValue_G.Size = new System.Drawing.Size(41, 22);
            this.txtMeanValue_G.TabIndex = 1;
            this.txtMeanValue_G.Text = "0";
            this.txtMeanValue_G.Validating += new System.ComponentModel.CancelEventHandler(this.txtMeanValue_G_Validating);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 148);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(15, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "R";
            // 
            // txtMeanValue_R
            // 
            this.txtMeanValue_R.Location = new System.Drawing.Point(28, 144);
            this.txtMeanValue_R.Name = "txtMeanValue_R";
            this.txtMeanValue_R.Size = new System.Drawing.Size(42, 22);
            this.txtMeanValue_R.TabIndex = 0;
            this.txtMeanValue_R.Text = "0";
            this.txtMeanValue_R.Validating += new System.ComponentModel.CancelEventHandler(this.txtMeanValue_R_Validating);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 1;
            this.label10.Text = "Mean Values :";
            // 
            // tab_step3
            // 
            this.tab_step3.Controls.Add(this.label29);
            this.tab_step3.Controls.Add(this.btn_open_optimal_folder);
            this.tab_step3.Controls.Add(this.lab_timeInfo);
            this.tab_step3.Controls.Add(this.btn_output);
            this.tab_step3.Controls.Add(this.cmb_prebuilt_model);
            this.tab_step3.Controls.Add(this.txt_curr_iterations);
            this.tab_step3.Controls.Add(this.label26);
            this.tab_step3.Controls.Add(this.txt_curr_validation_loss);
            this.tab_step3.Controls.Add(this.label25);
            this.tab_step3.Controls.Add(this.txt_curr_training_loss);
            this.tab_step3.Controls.Add(this.label24);
            this.tab_step3.Controls.Add(this.btn_browse_prebuilt_file);
            this.tab_step3.Controls.Add(this.txt_prebuilt_file);
            this.tab_step3.Controls.Add(this.lab_training_status);
            this.tab_step3.Controls.Add(this.progressBar_data);
            this.tab_step3.Controls.Add(this.btn_stopTraining);
            this.tab_step3.Controls.Add(this.btn_startTraining);
            this.tab_step3.Controls.Add(this.chart_loss);
            this.tab_step3.Controls.Add(this.chart1);
            this.tab_step3.Location = new System.Drawing.Point(4, 22);
            this.tab_step3.Name = "tab_step3";
            this.tab_step3.Padding = new System.Windows.Forms.Padding(3);
            this.tab_step3.Size = new System.Drawing.Size(720, 449);
            this.tab_step3.TabIndex = 2;
            this.tab_step3.Text = "STEP3:  Training Process";
            this.tab_step3.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(15, 200);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(86, 13);
            this.label29.TabIndex = 26;
            this.label29.Text = "Optimal Model :";
            // 
            // btn_open_optimal_folder
            // 
            this.btn_open_optimal_folder.Location = new System.Drawing.Point(17, 244);
            this.btn_open_optimal_folder.Name = "btn_open_optimal_folder";
            this.btn_open_optimal_folder.Size = new System.Drawing.Size(153, 23);
            this.btn_open_optimal_folder.TabIndex = 25;
            this.btn_open_optimal_folder.Text = "Browse optimal model";
            this.btn_open_optimal_folder.UseVisualStyleBackColor = true;
            this.btn_open_optimal_folder.Click += new System.EventHandler(this.btn_open_optimal_folder_Click);
            // 
            // lab_timeInfo
            // 
            this.lab_timeInfo.AutoSize = true;
            this.lab_timeInfo.Location = new System.Drawing.Point(15, 116);
            this.lab_timeInfo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lab_timeInfo.Name = "lab_timeInfo";
            this.lab_timeInfo.Size = new System.Drawing.Size(75, 13);
            this.lab_timeInfo.TabIndex = 24;
            this.lab_timeInfo.Text = "Time Elapsed:";
            // 
            // btn_output
            // 
            this.btn_output.Location = new System.Drawing.Point(17, 215);
            this.btn_output.Name = "btn_output";
            this.btn_output.Size = new System.Drawing.Size(153, 23);
            this.btn_output.TabIndex = 22;
            this.btn_output.Text = "Output optimal model again";
            this.btn_output.UseVisualStyleBackColor = true;
            this.btn_output.Click += new System.EventHandler(this.btn_output_Click);
            // 
            // cmb_prebuilt_model
            // 
            this.cmb_prebuilt_model.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmb_prebuilt_model.FormattingEnabled = true;
            this.cmb_prebuilt_model.Items.AddRange(new object[] {
            "Start a new training",
            "Fine tune from previous model",
            "Resume from previous state"});
            this.cmb_prebuilt_model.Location = new System.Drawing.Point(17, 16);
            this.cmb_prebuilt_model.Margin = new System.Windows.Forms.Padding(2);
            this.cmb_prebuilt_model.Name = "cmb_prebuilt_model";
            this.cmb_prebuilt_model.Size = new System.Drawing.Size(153, 20);
            this.cmb_prebuilt_model.TabIndex = 21;
            this.cmb_prebuilt_model.SelectedIndexChanged += new System.EventHandler(this.cmb_prebuilt_model_SelectedIndexChanged);
            // 
            // txt_curr_iterations
            // 
            this.txt_curr_iterations.Enabled = false;
            this.txt_curr_iterations.Location = new System.Drawing.Point(629, 105);
            this.txt_curr_iterations.Name = "txt_curr_iterations";
            this.txt_curr_iterations.Size = new System.Drawing.Size(75, 22);
            this.txt_curr_iterations.TabIndex = 20;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(545, 110);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(56, 13);
            this.label26.TabIndex = 19;
            this.label26.Text = "iterations :";
            // 
            // txt_curr_validation_loss
            // 
            this.txt_curr_validation_loss.Enabled = false;
            this.txt_curr_validation_loss.Location = new System.Drawing.Point(629, 128);
            this.txt_curr_validation_loss.Name = "txt_curr_validation_loss";
            this.txt_curr_validation_loss.Size = new System.Drawing.Size(75, 22);
            this.txt_curr_validation_loss.TabIndex = 18;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(545, 133);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(85, 13);
            this.label25.TabIndex = 17;
            this.label25.Text = "validation error :";
            // 
            // txt_curr_training_loss
            // 
            this.txt_curr_training_loss.Enabled = false;
            this.txt_curr_training_loss.Location = new System.Drawing.Point(629, 81);
            this.txt_curr_training_loss.Name = "txt_curr_training_loss";
            this.txt_curr_training_loss.Size = new System.Drawing.Size(75, 22);
            this.txt_curr_training_loss.TabIndex = 16;
            this.txt_curr_training_loss.TextChanged += new System.EventHandler(this.txt_curr_training_loss_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(545, 83);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(71, 13);
            this.label24.TabIndex = 15;
            this.label24.Text = "training loss :";
            // 
            // btn_browse_prebuilt_file
            // 
            this.btn_browse_prebuilt_file.Location = new System.Drawing.Point(628, 16);
            this.btn_browse_prebuilt_file.Name = "btn_browse_prebuilt_file";
            this.btn_browse_prebuilt_file.Size = new System.Drawing.Size(75, 23);
            this.btn_browse_prebuilt_file.TabIndex = 12;
            this.btn_browse_prebuilt_file.Text = "Browse ..";
            this.btn_browse_prebuilt_file.UseVisualStyleBackColor = true;
            this.btn_browse_prebuilt_file.Visible = false;
            this.btn_browse_prebuilt_file.Click += new System.EventHandler(this.btn_browse_prebuilt_file_Click);
            // 
            // txt_prebuilt_file
            // 
            this.txt_prebuilt_file.Location = new System.Drawing.Point(186, 16);
            this.txt_prebuilt_file.Name = "txt_prebuilt_file";
            this.txt_prebuilt_file.Size = new System.Drawing.Size(420, 22);
            this.txt_prebuilt_file.TabIndex = 10;
            this.txt_prebuilt_file.Visible = false;
            // 
            // lab_training_status
            // 
            this.lab_training_status.AutoSize = true;
            this.lab_training_status.Location = new System.Drawing.Point(15, 431);
            this.lab_training_status.Name = "lab_training_status";
            this.lab_training_status.Size = new System.Drawing.Size(69, 13);
            this.lab_training_status.TabIndex = 6;
            this.lab_training_status.Text = "Progress Bar";
            // 
            // progressBar_data
            // 
            this.progressBar_data.Location = new System.Drawing.Point(182, 420);
            this.progressBar_data.Name = "progressBar_data";
            this.progressBar_data.Size = new System.Drawing.Size(520, 23);
            this.progressBar_data.TabIndex = 5;
            // 
            // btn_stopTraining
            // 
            this.btn_stopTraining.Enabled = false;
            this.btn_stopTraining.Location = new System.Drawing.Point(17, 90);
            this.btn_stopTraining.Name = "btn_stopTraining";
            this.btn_stopTraining.Size = new System.Drawing.Size(153, 23);
            this.btn_stopTraining.TabIndex = 1;
            this.btn_stopTraining.Text = "Stop";
            this.btn_stopTraining.UseVisualStyleBackColor = true;
            this.btn_stopTraining.Click += new System.EventHandler(this.btn_stopTraining_Click);
            // 
            // btn_startTraining
            // 
            this.btn_startTraining.Location = new System.Drawing.Point(17, 61);
            this.btn_startTraining.Name = "btn_startTraining";
            this.btn_startTraining.Size = new System.Drawing.Size(153, 23);
            this.btn_startTraining.TabIndex = 0;
            this.btn_startTraining.Text = "Start";
            this.btn_startTraining.UseVisualStyleBackColor = true;
            this.btn_startTraining.Click += new System.EventHandler(this.btn_startTraining_Click);
            // 
            // chart_loss
            // 
            this.chart_loss.AllowDrop = true;
            chartArea1.AxisX.Title = "Iteration";
            chartArea1.AxisY.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY.LabelStyle.Interval = 0D;
            chartArea1.AxisY.LabelStyle.IntervalOffset = 0D;
            chartArea1.AxisY.LabelStyle.IntervalOffsetType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea1.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Auto;
            chartArea1.AxisY.LabelStyle.TruncatedLabels = true;
            chartArea1.AxisY.Title = "Loss";
            chartArea1.BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            chartArea1.Name = "ChartArea1";
            this.chart_loss.ChartAreas.Add(chartArea1);
            this.chart_loss.Location = new System.Drawing.Point(182, 44);
            this.chart_loss.Name = "chart_loss";
            this.chart_loss.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.SemiTransparent;
            series1.BackSecondaryColor = System.Drawing.Color.White;
            series1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Area;
            series1.Color = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            series1.LabelBackColor = System.Drawing.Color.Blue;
            series1.Name = "Series1";
            series1.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            this.chart_loss.Series.Add(series1);
            this.chart_loss.Size = new System.Drawing.Size(363, 142);
            this.chart_loss.TabIndex = 23;
            this.chart_loss.Text = "chart2";
            // 
            // chart1
            // 
            chartArea2.AxisX.Title = "Iteration";
            chartArea2.AxisY.Title = "Loss";
            chartArea2.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea2);
            legend1.Docking = System.Windows.Forms.DataVisualization.Charting.Docking.Bottom;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(164, 171);
            this.chart1.Name = "chart1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Color = System.Drawing.Color.RoyalBlue;
            series2.Legend = "Legend1";
            series2.LegendText = "loss (train)";
            series2.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series2.Name = "Series1";
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Color = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            series3.Legend = "Legend1";
            series3.LegendText = "loss (val)";
            series3.MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;
            series3.Name = "Series2";
            this.chart1.Series.Add(series2);
            this.chart1.Series.Add(series3);
            this.chart1.Size = new System.Drawing.Size(550, 252);
            this.chart1.TabIndex = 4;
            this.chart1.Text = "Training";
            title1.Name = "Training Output";
            this.chart1.Titles.Add(title1);
            // 
            // tab_confusion
            // 
            this.tab_confusion.AutoScroll = true;
            this.tab_confusion.Controls.Add(this.btn_misclassified_move_test);
            this.tab_confusion.Controls.Add(this.btn_misclassified_export_browse_test);
            this.tab_confusion.Controls.Add(this.txt_misclassified_export_dir_test);
            this.tab_confusion.Controls.Add(this.label64);
            this.tab_confusion.Controls.Add(this.btn_misclassified_move_val);
            this.tab_confusion.Controls.Add(this.btn_misclassified_export_browse_val);
            this.tab_confusion.Controls.Add(this.txt_misclassified_export_dir_val);
            this.tab_confusion.Controls.Add(this.label63);
            this.tab_confusion.Controls.Add(this.btn_misclassified_move_train);
            this.tab_confusion.Controls.Add(this.btn_misclassified_export_browse_train);
            this.tab_confusion.Controls.Add(this.txt_misclassified_export_dir_train);
            this.tab_confusion.Controls.Add(this.label62);
            this.tab_confusion.Controls.Add(this.lab_accuracy_testing);
            this.tab_confusion.Controls.Add(this.lab_accuracy_validation);
            this.tab_confusion.Controls.Add(this.chk_confusion_autofit_testing);
            this.tab_confusion.Controls.Add(this.chk_confusion_autofit_validation);
            this.tab_confusion.Controls.Add(this.label39);
            this.tab_confusion.Controls.Add(this.label40);
            this.tab_confusion.Controls.Add(this.label41);
            this.tab_confusion.Controls.Add(this.label38);
            this.tab_confusion.Controls.Add(this.label37);
            this.tab_confusion.Controls.Add(this.label36);
            this.tab_confusion.Controls.Add(this.button1btn_run_confusion_mat_testing);
            this.tab_confusion.Controls.Add(this.btn_run_confusion_mat_validation);
            this.tab_confusion.Controls.Add(this.dgv_confusion_matrix_testing);
            this.tab_confusion.Controls.Add(this.dgv_confusion_matrix_validation);
            this.tab_confusion.Controls.Add(this.btn_run_confusion_mat);
            this.tab_confusion.Controls.Add(this.chk_confusion_autofit_column);
            this.tab_confusion.Controls.Add(this.lab_accuracy_training);
            this.tab_confusion.Controls.Add(this.label8);
            this.tab_confusion.Controls.Add(this.label7);
            this.tab_confusion.Controls.Add(this.label6);
            this.tab_confusion.Controls.Add(this.dgv_confusion_matrix_training);
            this.tab_confusion.Location = new System.Drawing.Point(4, 22);
            this.tab_confusion.Name = "tab_confusion";
            this.tab_confusion.Padding = new System.Windows.Forms.Padding(3);
            this.tab_confusion.Size = new System.Drawing.Size(720, 449);
            this.tab_confusion.TabIndex = 3;
            this.tab_confusion.Text = "Confusion Matrix";
            this.tab_confusion.UseVisualStyleBackColor = true;
            // 
            // btn_misclassified_move_test
            // 
            this.btn_misclassified_move_test.Location = new System.Drawing.Point(603, 914);
            this.btn_misclassified_move_test.Name = "btn_misclassified_move_test";
            this.btn_misclassified_move_test.Size = new System.Drawing.Size(75, 23);
            this.btn_misclassified_move_test.TabIndex = 35;
            this.btn_misclassified_move_test.Text = "Move";
            this.btn_misclassified_move_test.UseVisualStyleBackColor = true;
            this.btn_misclassified_move_test.Click += new System.EventHandler(this.btn_misclassified_move_test_Click);
            // 
            // btn_misclassified_export_browse_test
            // 
            this.btn_misclassified_export_browse_test.Location = new System.Drawing.Point(524, 914);
            this.btn_misclassified_export_browse_test.Name = "btn_misclassified_export_browse_test";
            this.btn_misclassified_export_browse_test.Size = new System.Drawing.Size(75, 23);
            this.btn_misclassified_export_browse_test.TabIndex = 34;
            this.btn_misclassified_export_browse_test.Text = "Browse ..";
            this.btn_misclassified_export_browse_test.UseVisualStyleBackColor = true;
            this.btn_misclassified_export_browse_test.Click += new System.EventHandler(this.btn_misclassified_export_browse_test_Click);
            // 
            // txt_misclassified_export_dir_test
            // 
            this.txt_misclassified_export_dir_test.Location = new System.Drawing.Point(204, 915);
            this.txt_misclassified_export_dir_test.Name = "txt_misclassified_export_dir_test";
            this.txt_misclassified_export_dir_test.Size = new System.Drawing.Size(314, 22);
            this.txt_misclassified_export_dir_test.TabIndex = 33;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(26, 920);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(139, 13);
            this.label64.TabIndex = 32;
            this.label64.Text = "Move uncertain images to : ";
            // 
            // btn_misclassified_move_val
            // 
            this.btn_misclassified_move_val.Location = new System.Drawing.Point(609, 480);
            this.btn_misclassified_move_val.Name = "btn_misclassified_move_val";
            this.btn_misclassified_move_val.Size = new System.Drawing.Size(75, 23);
            this.btn_misclassified_move_val.TabIndex = 31;
            this.btn_misclassified_move_val.Text = "Move";
            this.btn_misclassified_move_val.UseVisualStyleBackColor = true;
            this.btn_misclassified_move_val.Click += new System.EventHandler(this.btn_misclassified_move_val_Click);
            // 
            // btn_misclassified_export_browse_val
            // 
            this.btn_misclassified_export_browse_val.Location = new System.Drawing.Point(530, 480);
            this.btn_misclassified_export_browse_val.Name = "btn_misclassified_export_browse_val";
            this.btn_misclassified_export_browse_val.Size = new System.Drawing.Size(75, 23);
            this.btn_misclassified_export_browse_val.TabIndex = 30;
            this.btn_misclassified_export_browse_val.Text = "Browse ..";
            this.btn_misclassified_export_browse_val.UseVisualStyleBackColor = true;
            this.btn_misclassified_export_browse_val.Click += new System.EventHandler(this.btn_misclassified_export_browse_val_Click);
            // 
            // txt_misclassified_export_dir_val
            // 
            this.txt_misclassified_export_dir_val.Location = new System.Drawing.Point(222, 481);
            this.txt_misclassified_export_dir_val.Name = "txt_misclassified_export_dir_val";
            this.txt_misclassified_export_dir_val.Size = new System.Drawing.Size(302, 22);
            this.txt_misclassified_export_dir_val.TabIndex = 29;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(32, 486);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(139, 13);
            this.label63.TabIndex = 28;
            this.label63.Text = "Move uncertain images to : ";
            // 
            // btn_misclassified_move_train
            // 
            this.btn_misclassified_move_train.Location = new System.Drawing.Point(609, 36);
            this.btn_misclassified_move_train.Name = "btn_misclassified_move_train";
            this.btn_misclassified_move_train.Size = new System.Drawing.Size(75, 23);
            this.btn_misclassified_move_train.TabIndex = 27;
            this.btn_misclassified_move_train.Text = "Move";
            this.btn_misclassified_move_train.UseVisualStyleBackColor = true;
            this.btn_misclassified_move_train.Click += new System.EventHandler(this.btn_misclassified_move_train_Click);
            // 
            // btn_misclassified_export_browse_train
            // 
            this.btn_misclassified_export_browse_train.Location = new System.Drawing.Point(530, 36);
            this.btn_misclassified_export_browse_train.Name = "btn_misclassified_export_browse_train";
            this.btn_misclassified_export_browse_train.Size = new System.Drawing.Size(75, 23);
            this.btn_misclassified_export_browse_train.TabIndex = 26;
            this.btn_misclassified_export_browse_train.Text = "Browse ..";
            this.btn_misclassified_export_browse_train.UseVisualStyleBackColor = true;
            this.btn_misclassified_export_browse_train.Click += new System.EventHandler(this.btn_misclassified_export_browse_train_Click);
            // 
            // txt_misclassified_export_dir_train
            // 
            this.txt_misclassified_export_dir_train.Location = new System.Drawing.Point(207, 37);
            this.txt_misclassified_export_dir_train.Name = "txt_misclassified_export_dir_train";
            this.txt_misclassified_export_dir_train.Size = new System.Drawing.Size(317, 22);
            this.txt_misclassified_export_dir_train.TabIndex = 25;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(28, 40);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(139, 13);
            this.label62.TabIndex = 24;
            this.label62.Text = "Move uncertain images to : ";
            // 
            // lab_accuracy_testing
            // 
            this.lab_accuracy_testing.AutoSize = true;
            this.lab_accuracy_testing.Location = new System.Drawing.Point(22, 944);
            this.lab_accuracy_testing.Name = "lab_accuracy_testing";
            this.lab_accuracy_testing.Size = new System.Drawing.Size(58, 13);
            this.lab_accuracy_testing.TabIndex = 23;
            this.lab_accuracy_testing.Text = "Accuracy :";
            // 
            // lab_accuracy_validation
            // 
            this.lab_accuracy_validation.AutoSize = true;
            this.lab_accuracy_validation.Location = new System.Drawing.Point(28, 511);
            this.lab_accuracy_validation.Name = "lab_accuracy_validation";
            this.lab_accuracy_validation.Size = new System.Drawing.Size(58, 13);
            this.lab_accuracy_validation.TabIndex = 22;
            this.lab_accuracy_validation.Text = "Accuracy :";
            // 
            // chk_confusion_autofit_testing
            // 
            this.chk_confusion_autofit_testing.AutoSize = true;
            this.chk_confusion_autofit_testing.Checked = true;
            this.chk_confusion_autofit_testing.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_confusion_autofit_testing.Location = new System.Drawing.Point(546, 943);
            this.chk_confusion_autofit_testing.Name = "chk_confusion_autofit_testing";
            this.chk_confusion_autofit_testing.Size = new System.Drawing.Size(141, 17);
            this.chk_confusion_autofit_testing.TabIndex = 21;
            this.chk_confusion_autofit_testing.Text = "AutoFit Column Width";
            this.chk_confusion_autofit_testing.UseVisualStyleBackColor = true;
            this.chk_confusion_autofit_testing.CheckedChanged += new System.EventHandler(this.chk_confusion_autofit_testing_CheckedChanged);
            // 
            // chk_confusion_autofit_validation
            // 
            this.chk_confusion_autofit_validation.AutoSize = true;
            this.chk_confusion_autofit_validation.Checked = true;
            this.chk_confusion_autofit_validation.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_confusion_autofit_validation.Location = new System.Drawing.Point(552, 510);
            this.chk_confusion_autofit_validation.Name = "chk_confusion_autofit_validation";
            this.chk_confusion_autofit_validation.Size = new System.Drawing.Size(141, 17);
            this.chk_confusion_autofit_validation.TabIndex = 20;
            this.chk_confusion_autofit_validation.Text = "AutoFit Column Width";
            this.chk_confusion_autofit_validation.UseVisualStyleBackColor = true;
            this.chk_confusion_autofit_validation.CheckedChanged += new System.EventHandler(this.chk_confusion_autofit_validation_CheckedChanged);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(5, 1092);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(16, 78);
            this.label39.TabIndex = 19;
            this.label39.Text = "A\r\nc\r\nt\r\nu\r\na\r\nl ";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(333, 944);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(51, 13);
            this.label40.TabIndex = 18;
            this.label40.Text = "Predicted";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(28, 895);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(175, 13);
            this.label41.TabIndex = 17;
            this.label41.Text = "Run confusion matrix for Testing : ";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(11, 647);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(16, 78);
            this.label38.TabIndex = 16;
            this.label38.Text = "A\r\nc\r\nt\r\nu\r\na\r\nl ";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(341, 511);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(51, 13);
            this.label37.TabIndex = 15;
            this.label37.Text = "Predicted";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(32, 461);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(189, 13);
            this.label36.TabIndex = 14;
            this.label36.Text = "Run confusion matrix for Validation : ";
            // 
            // button1btn_run_confusion_mat_testing
            // 
            this.button1btn_run_confusion_mat_testing.Location = new System.Drawing.Point(204, 890);
            this.button1btn_run_confusion_mat_testing.Name = "button1btn_run_confusion_mat_testing";
            this.button1btn_run_confusion_mat_testing.Size = new System.Drawing.Size(75, 23);
            this.button1btn_run_confusion_mat_testing.TabIndex = 13;
            this.button1btn_run_confusion_mat_testing.Text = "Start";
            this.button1btn_run_confusion_mat_testing.UseVisualStyleBackColor = true;
            this.button1btn_run_confusion_mat_testing.Click += new System.EventHandler(this.button1btn_run_confusion_mat_testing_Click);
            // 
            // btn_run_confusion_mat_validation
            // 
            this.btn_run_confusion_mat_validation.Location = new System.Drawing.Point(222, 456);
            this.btn_run_confusion_mat_validation.Name = "btn_run_confusion_mat_validation";
            this.btn_run_confusion_mat_validation.Size = new System.Drawing.Size(75, 23);
            this.btn_run_confusion_mat_validation.TabIndex = 12;
            this.btn_run_confusion_mat_validation.Text = "Start";
            this.btn_run_confusion_mat_validation.UseVisualStyleBackColor = true;
            this.btn_run_confusion_mat_validation.Click += new System.EventHandler(this.btn_run_confusion_mat_validation_Click);
            // 
            // dgv_confusion_matrix_testing
            // 
            this.dgv_confusion_matrix_testing.AllowUserToAddRows = false;
            this.dgv_confusion_matrix_testing.AllowUserToDeleteRows = false;
            this.dgv_confusion_matrix_testing.AllowUserToResizeRows = false;
            this.dgv_confusion_matrix_testing.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_confusion_matrix_testing.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_confusion_matrix_testing.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_confusion_matrix_testing.ColumnHeadersVisible = false;
            this.dgv_confusion_matrix_testing.Location = new System.Drawing.Point(24, 959);
            this.dgv_confusion_matrix_testing.Name = "dgv_confusion_matrix_testing";
            this.dgv_confusion_matrix_testing.ReadOnly = true;
            this.dgv_confusion_matrix_testing.RowHeadersVisible = false;
            this.dgv_confusion_matrix_testing.RowHeadersWidth = 51;
            this.dgv_confusion_matrix_testing.RowTemplate.Height = 24;
            this.dgv_confusion_matrix_testing.Size = new System.Drawing.Size(654, 342);
            this.dgv_confusion_matrix_testing.TabIndex = 11;
            this.dgv_confusion_matrix_testing.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_confusion_matrix_testing_CellContentClick);
            // 
            // dgv_confusion_matrix_validation
            // 
            this.dgv_confusion_matrix_validation.AllowUserToAddRows = false;
            this.dgv_confusion_matrix_validation.AllowUserToDeleteRows = false;
            this.dgv_confusion_matrix_validation.AllowUserToResizeRows = false;
            this.dgv_confusion_matrix_validation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_confusion_matrix_validation.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_confusion_matrix_validation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_confusion_matrix_validation.ColumnHeadersVisible = false;
            this.dgv_confusion_matrix_validation.Location = new System.Drawing.Point(30, 526);
            this.dgv_confusion_matrix_validation.Name = "dgv_confusion_matrix_validation";
            this.dgv_confusion_matrix_validation.ReadOnly = true;
            this.dgv_confusion_matrix_validation.RowHeadersVisible = false;
            this.dgv_confusion_matrix_validation.RowHeadersWidth = 51;
            this.dgv_confusion_matrix_validation.RowTemplate.Height = 24;
            this.dgv_confusion_matrix_validation.Size = new System.Drawing.Size(654, 342);
            this.dgv_confusion_matrix_validation.TabIndex = 10;
            this.dgv_confusion_matrix_validation.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_confusion_matrix_validation_CellContentClick);
            // 
            // btn_run_confusion_mat
            // 
            this.btn_run_confusion_mat.Location = new System.Drawing.Point(207, 12);
            this.btn_run_confusion_mat.Name = "btn_run_confusion_mat";
            this.btn_run_confusion_mat.Size = new System.Drawing.Size(75, 23);
            this.btn_run_confusion_mat.TabIndex = 9;
            this.btn_run_confusion_mat.Text = "Start";
            this.btn_run_confusion_mat.UseVisualStyleBackColor = true;
            this.btn_run_confusion_mat.Click += new System.EventHandler(this.btn_run_confusion_mat_Click);
            // 
            // chk_confusion_autofit_column
            // 
            this.chk_confusion_autofit_column.AutoSize = true;
            this.chk_confusion_autofit_column.Checked = true;
            this.chk_confusion_autofit_column.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk_confusion_autofit_column.Location = new System.Drawing.Point(552, 69);
            this.chk_confusion_autofit_column.Name = "chk_confusion_autofit_column";
            this.chk_confusion_autofit_column.Size = new System.Drawing.Size(141, 17);
            this.chk_confusion_autofit_column.TabIndex = 8;
            this.chk_confusion_autofit_column.Text = "AutoFit Column Width";
            this.chk_confusion_autofit_column.UseVisualStyleBackColor = true;
            this.chk_confusion_autofit_column.CheckedChanged += new System.EventHandler(this.chk_confusion_autofit_column_CheckedChanged);
            // 
            // lab_accuracy_training
            // 
            this.lab_accuracy_training.AutoSize = true;
            this.lab_accuracy_training.Location = new System.Drawing.Point(28, 70);
            this.lab_accuracy_training.Name = "lab_accuracy_training";
            this.lab_accuracy_training.Size = new System.Drawing.Size(58, 13);
            this.lab_accuracy_training.TabIndex = 5;
            this.lab_accuracy_training.Text = "Accuracy :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 135);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 78);
            this.label8.TabIndex = 4;
            this.label8.Text = "A\r\nc\r\nt\r\nu\r\na\r\nl ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(341, 70);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "Predicted";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(177, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "Run confusion matrix for Training :";
            // 
            // dgv_confusion_matrix_training
            // 
            this.dgv_confusion_matrix_training.AllowUserToAddRows = false;
            this.dgv_confusion_matrix_training.AllowUserToDeleteRows = false;
            this.dgv_confusion_matrix_training.AllowUserToResizeRows = false;
            this.dgv_confusion_matrix_training.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_confusion_matrix_training.BackgroundColor = System.Drawing.SystemColors.Window;
            this.dgv_confusion_matrix_training.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_confusion_matrix_training.ColumnHeadersVisible = false;
            this.dgv_confusion_matrix_training.Location = new System.Drawing.Point(30, 91);
            this.dgv_confusion_matrix_training.Name = "dgv_confusion_matrix_training";
            this.dgv_confusion_matrix_training.ReadOnly = true;
            this.dgv_confusion_matrix_training.RowHeadersVisible = false;
            this.dgv_confusion_matrix_training.RowHeadersWidth = 51;
            this.dgv_confusion_matrix_training.RowTemplate.Height = 24;
            this.dgv_confusion_matrix_training.Size = new System.Drawing.Size(654, 342);
            this.dgv_confusion_matrix_training.TabIndex = 1;
            this.dgv_confusion_matrix_training.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_confusion_matrix_training_CellContentClick);
            // 
            // tab_threshold
            // 
            this.tab_threshold.Controls.Add(this.chk_threshold_adjust);
            this.tab_threshold.Controls.Add(this.btn_generate_param_file);
            this.tab_threshold.Controls.Add(this.label30);
            this.tab_threshold.Controls.Add(this.dgv_threshold);
            this.tab_threshold.Location = new System.Drawing.Point(4, 22);
            this.tab_threshold.Name = "tab_threshold";
            this.tab_threshold.Padding = new System.Windows.Forms.Padding(3);
            this.tab_threshold.Size = new System.Drawing.Size(720, 449);
            this.tab_threshold.TabIndex = 4;
            this.tab_threshold.Text = "Threshold Adjustment";
            this.tab_threshold.UseVisualStyleBackColor = true;
            // 
            // chk_threshold_adjust
            // 
            this.chk_threshold_adjust.AutoSize = true;
            this.chk_threshold_adjust.Location = new System.Drawing.Point(562, 29);
            this.chk_threshold_adjust.Name = "chk_threshold_adjust";
            this.chk_threshold_adjust.Size = new System.Drawing.Size(141, 17);
            this.chk_threshold_adjust.TabIndex = 24;
            this.chk_threshold_adjust.Text = "AutoFit Column Width";
            this.chk_threshold_adjust.UseVisualStyleBackColor = true;
            this.chk_threshold_adjust.CheckedChanged += new System.EventHandler(this.chk_threshold_adjust_CheckedChanged);
            // 
            // btn_generate_param_file
            // 
            this.btn_generate_param_file.Location = new System.Drawing.Point(21, 393);
            this.btn_generate_param_file.Name = "btn_generate_param_file";
            this.btn_generate_param_file.Size = new System.Drawing.Size(126, 23);
            this.btn_generate_param_file.TabIndex = 23;
            this.btn_generate_param_file.Text = "Generate parameters.cfg";
            this.btn_generate_param_file.UseVisualStyleBackColor = true;
            this.btn_generate_param_file.Click += new System.EventHandler(this.btn_generate_param_file_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(19, 22);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(120, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Threshold Adjustment :";
            // 
            // dgv_threshold
            // 
            this.dgv_threshold.AllowUserToAddRows = false;
            this.dgv_threshold.AllowUserToDeleteRows = false;
            this.dgv_threshold.AllowUserToResizeRows = false;
            this.dgv_threshold.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_threshold.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_threshold.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column5,
            this.Column4,
            this.Column2,
            this.Column6,
            this.Column7,
            this.Column3});
            this.dgv_threshold.Location = new System.Drawing.Point(21, 51);
            this.dgv_threshold.Name = "dgv_threshold";
            this.dgv_threshold.RowHeadersVisible = false;
            this.dgv_threshold.RowHeadersWidth = 51;
            this.dgv_threshold.RowTemplate.Height = 24;
            this.dgv_threshold.Size = new System.Drawing.Size(673, 326);
            this.dgv_threshold.TabIndex = 0;
            this.dgv_threshold.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_threshold_CellContentClick);
            this.dgv_threshold.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_threshold_CellEndEdit);
            // 
            // Column1
            // 
            this.Column1.FillWeight = 40F;
            this.Column1.HeaderText = "ID";
            this.Column1.MinimumWidth = 6;
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            // 
            // Column5
            // 
            this.Column5.FillWeight = 200F;
            this.Column5.HeaderText = "Class Name";
            this.Column5.MinimumWidth = 6;
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            // 
            // Column4
            // 
            this.Column4.FillWeight = 60F;
            this.Column4.HeaderText = "Class Code";
            this.Column4.MinimumWidth = 6;
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "Threshold";
            this.Column2.MinimumWidth = 6;
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            // 
            // Column6
            // 
            this.Column6.FillWeight = 80F;
            this.Column6.HeaderText = "# of passed images";
            this.Column6.MinimumWidth = 6;
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.FillWeight = 80F;
            this.Column7.HeaderText = "# of total images";
            this.Column7.MinimumWidth = 6;
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            // 
            // Column3
            // 
            this.Column3.FillWeight = 60F;
            this.Column3.HeaderText = "Accuracy (%)";
            this.Column3.MinimumWidth = 6;
            this.Column3.Name = "Column3";
            // 
            // tab_inference
            // 
            this.tab_inference.Controls.Add(this.mtxt_batch_size_of_inference);
            this.tab_inference.Controls.Add(this.mtxt_threshold_for_inference);
            this.tab_inference.Controls.Add(this.btn_run_inference);
            this.tab_inference.Controls.Add(this.gbox_pred_accuracy);
            this.tab_inference.Controls.Add(this.label35);
            this.tab_inference.Controls.Add(this.label34);
            this.tab_inference.Controls.Add(this.cbox_gpu_list);
            this.tab_inference.Controls.Add(this.label33);
            this.tab_inference.Controls.Add(this.groupBox1);
            this.tab_inference.Location = new System.Drawing.Point(4, 22);
            this.tab_inference.Name = "tab_inference";
            this.tab_inference.Padding = new System.Windows.Forms.Padding(3);
            this.tab_inference.Size = new System.Drawing.Size(720, 449);
            this.tab_inference.TabIndex = 5;
            this.tab_inference.Text = "Inference";
            this.tab_inference.UseVisualStyleBackColor = true;
            // 
            // mtxt_batch_size_of_inference
            // 
            this.mtxt_batch_size_of_inference.Location = new System.Drawing.Point(227, 130);
            this.mtxt_batch_size_of_inference.Mask = "000";
            this.mtxt_batch_size_of_inference.Name = "mtxt_batch_size_of_inference";
            this.mtxt_batch_size_of_inference.PromptChar = ' ';
            this.mtxt_batch_size_of_inference.Size = new System.Drawing.Size(168, 22);
            this.mtxt_batch_size_of_inference.TabIndex = 38;
            this.mtxt_batch_size_of_inference.Text = "10";
            this.mtxt_batch_size_of_inference.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // mtxt_threshold_for_inference
            // 
            this.mtxt_threshold_for_inference.Location = new System.Drawing.Point(421, 130);
            this.mtxt_threshold_for_inference.Mask = "0.000000";
            this.mtxt_threshold_for_inference.Name = "mtxt_threshold_for_inference";
            this.mtxt_threshold_for_inference.Size = new System.Drawing.Size(174, 22);
            this.mtxt_threshold_for_inference.TabIndex = 37;
            this.mtxt_threshold_for_inference.Text = "05";
            this.mtxt_threshold_for_inference.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mtxt_threshold_for_inference.Visible = false;
            // 
            // btn_run_inference
            // 
            this.btn_run_inference.Location = new System.Drawing.Point(617, 128);
            this.btn_run_inference.Name = "btn_run_inference";
            this.btn_run_inference.Size = new System.Drawing.Size(75, 23);
            this.btn_run_inference.TabIndex = 29;
            this.btn_run_inference.Text = "Run";
            this.btn_run_inference.UseVisualStyleBackColor = true;
            this.btn_run_inference.Click += new System.EventHandler(this.btn_run_inference_Click);
            // 
            // gbox_pred_accuracy
            // 
            this.gbox_pred_accuracy.Controls.Add(this.label50);
            this.gbox_pred_accuracy.Controls.Add(this.label49);
            this.gbox_pred_accuracy.Controls.Add(this.panel2);
            this.gbox_pred_accuracy.Controls.Add(this.panel1);
            this.gbox_pred_accuracy.Controls.Add(this.label_target_image);
            this.gbox_pred_accuracy.Controls.Add(this.pic_inference_image);
            this.gbox_pred_accuracy.Controls.Add(this.dgv_inference_result);
            this.gbox_pred_accuracy.Location = new System.Drawing.Point(23, 162);
            this.gbox_pred_accuracy.Name = "gbox_pred_accuracy";
            this.gbox_pred_accuracy.Size = new System.Drawing.Size(675, 285);
            this.gbox_pred_accuracy.TabIndex = 36;
            this.gbox_pred_accuracy.TabStop = false;
            this.gbox_pred_accuracy.Text = "Prediction Accuracy :";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(147, 21);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(55, 13);
            this.label50.TabIndex = 43;
            this.label50.Text = "Unkonwn";
            this.label50.Visible = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(30, 21);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(84, 13);
            this.label49.TabIndex = 39;
            this.label49.Text = "Prediction failed";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.NavajoWhite;
            this.panel2.Location = new System.Drawing.Point(131, 21);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(10, 11);
            this.panel2.TabIndex = 42;
            this.panel2.Visible = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightPink;
            this.panel1.Location = new System.Drawing.Point(14, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(10, 11);
            this.panel1.TabIndex = 41;
            // 
            // label_target_image
            // 
            this.label_target_image.AutoSize = true;
            this.label_target_image.Location = new System.Drawing.Point(6, 267);
            this.label_target_image.Name = "label_target_image";
            this.label_target_image.Size = new System.Drawing.Size(72, 13);
            this.label_target_image.TabIndex = 40;
            this.label_target_image.Text = "Image Name:";
            // 
            // pic_inference_image
            // 
            this.pic_inference_image.Location = new System.Drawing.Point(367, 42);
            this.pic_inference_image.Name = "pic_inference_image";
            this.pic_inference_image.Size = new System.Drawing.Size(302, 218);
            this.pic_inference_image.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pic_inference_image.TabIndex = 39;
            this.pic_inference_image.TabStop = false;
            // 
            // dgv_inference_result
            // 
            this.dgv_inference_result.AllowUserToAddRows = false;
            this.dgv_inference_result.AllowUserToDeleteRows = false;
            this.dgv_inference_result.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_inference_result.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_inference_result.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column8,
            this.Column9});
            this.dgv_inference_result.Location = new System.Drawing.Point(6, 42);
            this.dgv_inference_result.MultiSelect = false;
            this.dgv_inference_result.Name = "dgv_inference_result";
            this.dgv_inference_result.ReadOnly = true;
            this.dgv_inference_result.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("微軟正黑體", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_inference_result.RowHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_inference_result.RowHeadersWidth = 50;
            this.dgv_inference_result.RowTemplate.Height = 24;
            this.dgv_inference_result.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv_inference_result.Size = new System.Drawing.Size(355, 218);
            this.dgv_inference_result.TabIndex = 38;
            this.dgv_inference_result.SelectionChanged += new System.EventHandler(this.dgv_inference_result_SelectionChanged);
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column8.FillWeight = 106.599F;
            this.Column8.HeaderText = "File Name";
            this.Column8.MinimumWidth = 6;
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column9.DefaultCellStyle = dataGridViewCellStyle1;
            this.Column9.FillWeight = 93.40102F;
            this.Column9.HeaderText = "Prediction";
            this.Column9.MinimumWidth = 6;
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Width = 58;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(419, 113);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(61, 13);
            this.label35.TabIndex = 32;
            this.label35.Text = "Threshold :";
            this.label35.Visible = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(225, 113);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(168, 13);
            this.label34.TabIndex = 30;
            this.label34.Text = "Batch Size (Number of Pictures) :";
            // 
            // cbox_gpu_list
            // 
            this.cbox_gpu_list.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_gpu_list.FormattingEnabled = true;
            this.cbox_gpu_list.Location = new System.Drawing.Point(23, 132);
            this.cbox_gpu_list.Name = "cbox_gpu_list";
            this.cbox_gpu_list.Size = new System.Drawing.Size(182, 20);
            this.cbox_gpu_list.TabIndex = 29;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(21, 113);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(69, 13);
            this.label33.TabIndex = 13;
            this.label33.Text = "GPU to use :";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btn_browse_inference_folder);
            this.groupBox1.Controls.Add(this.txt_inference_target_folder);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.cbox_ground_truth);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Location = new System.Drawing.Point(23, 15);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(675, 90);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Choose Images with given ground truth to evaluate the new model";
            // 
            // btn_browse_inference_folder
            // 
            this.btn_browse_inference_folder.Location = new System.Drawing.Point(594, 51);
            this.btn_browse_inference_folder.Name = "btn_browse_inference_folder";
            this.btn_browse_inference_folder.Size = new System.Drawing.Size(75, 23);
            this.btn_browse_inference_folder.TabIndex = 28;
            this.btn_browse_inference_folder.Text = "Browse ...";
            this.btn_browse_inference_folder.UseVisualStyleBackColor = true;
            this.btn_browse_inference_folder.Click += new System.EventHandler(this.btn_browse_inference_folder_Click);
            // 
            // txt_inference_target_folder
            // 
            this.txt_inference_target_folder.Location = new System.Drawing.Point(94, 53);
            this.txt_inference_target_folder.Name = "txt_inference_target_folder";
            this.txt_inference_target_folder.Size = new System.Drawing.Size(480, 22);
            this.txt_inference_target_folder.TabIndex = 7;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(12, 56);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(77, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Image Folder :";
            // 
            // cbox_ground_truth
            // 
            this.cbox_ground_truth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_ground_truth.FormattingEnabled = true;
            this.cbox_ground_truth.Location = new System.Drawing.Point(94, 26);
            this.cbox_ground_truth.Name = "cbox_ground_truth";
            this.cbox_ground_truth.Size = new System.Drawing.Size(480, 20);
            this.cbox_ground_truth.TabIndex = 5;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(12, 29);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(79, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "Ground Truth :";
            // 
            // tab_presorting
            // 
            this.tab_presorting.Controls.Add(this.label48);
            this.tab_presorting.Controls.Add(this.btn_check_dest_folder);
            this.tab_presorting.Controls.Add(this.mtxt_batch_size_of_presorting);
            this.tab_presorting.Controls.Add(this.mtxt_threshold_for_presorting);
            this.tab_presorting.Controls.Add(this.btn_run_presorting);
            this.tab_presorting.Controls.Add(this.label45);
            this.tab_presorting.Controls.Add(this.label46);
            this.tab_presorting.Controls.Add(this.cbox_presorting_gpu_list);
            this.tab_presorting.Controls.Add(this.label47);
            this.tab_presorting.Controls.Add(this.groupBox2);
            this.tab_presorting.Location = new System.Drawing.Point(4, 22);
            this.tab_presorting.Name = "tab_presorting";
            this.tab_presorting.Padding = new System.Windows.Forms.Padding(3);
            this.tab_presorting.Size = new System.Drawing.Size(720, 449);
            this.tab_presorting.TabIndex = 6;
            this.tab_presorting.Text = "Pre-sorting";
            this.tab_presorting.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(21, 188);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(134, 13);
            this.label48.TabIndex = 47;
            this.label48.Text = "Check Destination Folder :";
            // 
            // btn_check_dest_folder
            // 
            this.btn_check_dest_folder.Location = new System.Drawing.Point(23, 203);
            this.btn_check_dest_folder.Name = "btn_check_dest_folder";
            this.btn_check_dest_folder.Size = new System.Drawing.Size(75, 23);
            this.btn_check_dest_folder.TabIndex = 46;
            this.btn_check_dest_folder.Text = "Open";
            this.btn_check_dest_folder.UseVisualStyleBackColor = true;
            this.btn_check_dest_folder.Click += new System.EventHandler(this.btn_check_dest_folder_Click);
            // 
            // mtxt_batch_size_of_presorting
            // 
            this.mtxt_batch_size_of_presorting.Location = new System.Drawing.Point(227, 140);
            this.mtxt_batch_size_of_presorting.Mask = "000";
            this.mtxt_batch_size_of_presorting.Name = "mtxt_batch_size_of_presorting";
            this.mtxt_batch_size_of_presorting.PromptChar = ' ';
            this.mtxt_batch_size_of_presorting.Size = new System.Drawing.Size(168, 22);
            this.mtxt_batch_size_of_presorting.TabIndex = 45;
            this.mtxt_batch_size_of_presorting.Text = "10";
            this.mtxt_batch_size_of_presorting.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // mtxt_threshold_for_presorting
            // 
            this.mtxt_threshold_for_presorting.Location = new System.Drawing.Point(421, 140);
            this.mtxt_threshold_for_presorting.Mask = "0.000000";
            this.mtxt_threshold_for_presorting.Name = "mtxt_threshold_for_presorting";
            this.mtxt_threshold_for_presorting.Size = new System.Drawing.Size(174, 22);
            this.mtxt_threshold_for_presorting.TabIndex = 44;
            this.mtxt_threshold_for_presorting.Text = "05";
            this.mtxt_threshold_for_presorting.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.mtxt_threshold_for_presorting.Visible = false;
            // 
            // btn_run_presorting
            // 
            this.btn_run_presorting.Location = new System.Drawing.Point(617, 138);
            this.btn_run_presorting.Name = "btn_run_presorting";
            this.btn_run_presorting.Size = new System.Drawing.Size(75, 23);
            this.btn_run_presorting.TabIndex = 40;
            this.btn_run_presorting.Text = "Start to move";
            this.btn_run_presorting.UseVisualStyleBackColor = true;
            this.btn_run_presorting.Click += new System.EventHandler(this.btn_run_presorting_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(419, 123);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(61, 13);
            this.label45.TabIndex = 43;
            this.label45.Text = "Threshold :";
            this.label45.Visible = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(225, 123);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(168, 13);
            this.label46.TabIndex = 42;
            this.label46.Text = "Batch Size (Number of Pictures) :";
            // 
            // cbox_presorting_gpu_list
            // 
            this.cbox_presorting_gpu_list.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_presorting_gpu_list.FormattingEnabled = true;
            this.cbox_presorting_gpu_list.Location = new System.Drawing.Point(23, 142);
            this.cbox_presorting_gpu_list.Name = "cbox_presorting_gpu_list";
            this.cbox_presorting_gpu_list.Size = new System.Drawing.Size(182, 20);
            this.cbox_presorting_gpu_list.TabIndex = 41;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(21, 123);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(69, 13);
            this.label47.TabIndex = 39;
            this.label47.Text = "GPU to use :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btn_browse_src);
            this.groupBox2.Controls.Add(this.txt_presorting_src_folder);
            this.groupBox2.Controls.Add(this.btn_browse_dest);
            this.groupBox2.Controls.Add(this.txt_presorting_dest_folder);
            this.groupBox2.Controls.Add(this.label43);
            this.groupBox2.Controls.Add(this.label44);
            this.groupBox2.Location = new System.Drawing.Point(23, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(675, 90);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pre-sorting  the Images from source folder and move results to the destination fo" +
    "lder";
            // 
            // btn_browse_src
            // 
            this.btn_browse_src.Location = new System.Drawing.Point(594, 19);
            this.btn_browse_src.Name = "btn_browse_src";
            this.btn_browse_src.Size = new System.Drawing.Size(75, 23);
            this.btn_browse_src.TabIndex = 30;
            this.btn_browse_src.Text = "Browse ...";
            this.btn_browse_src.UseVisualStyleBackColor = true;
            this.btn_browse_src.Click += new System.EventHandler(this.btn_browse_src_Click);
            // 
            // txt_presorting_src_folder
            // 
            this.txt_presorting_src_folder.Location = new System.Drawing.Point(114, 21);
            this.txt_presorting_src_folder.Name = "txt_presorting_src_folder";
            this.txt_presorting_src_folder.Size = new System.Drawing.Size(460, 22);
            this.txt_presorting_src_folder.TabIndex = 29;
            // 
            // btn_browse_dest
            // 
            this.btn_browse_dest.Location = new System.Drawing.Point(594, 51);
            this.btn_browse_dest.Name = "btn_browse_dest";
            this.btn_browse_dest.Size = new System.Drawing.Size(75, 23);
            this.btn_browse_dest.TabIndex = 28;
            this.btn_browse_dest.Text = "Browse ...";
            this.btn_browse_dest.UseVisualStyleBackColor = true;
            this.btn_browse_dest.Click += new System.EventHandler(this.btn_browse_dest_Click);
            // 
            // txt_presorting_dest_folder
            // 
            this.txt_presorting_dest_folder.Location = new System.Drawing.Point(114, 53);
            this.txt_presorting_dest_folder.Name = "txt_presorting_dest_folder";
            this.txt_presorting_dest_folder.Size = new System.Drawing.Size(460, 22);
            this.txt_presorting_dest_folder.TabIndex = 7;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(12, 56);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(101, 13);
            this.label43.TabIndex = 6;
            this.label43.Text = "Destination Folder :";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(12, 29);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(80, 13);
            this.label44.TabIndex = 4;
            this.label44.Text = "Source Folder :";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.logToolStripMenuItem,
            this.toolToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(4, 2, 0, 2);
            this.menuStrip1.Size = new System.Drawing.Size(728, 27);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createNewToolStripMenuItem,
            this.loadToolStripMenuItem,
            this.closeToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(47, 23);
            this.toolStripMenuItem1.Text = "File";
            // 
            // createNewToolStripMenuItem
            // 
            this.createNewToolStripMenuItem.Name = "createNewToolStripMenuItem";
            this.createNewToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.createNewToolStripMenuItem.Text = "New  Project";
            this.createNewToolStripMenuItem.Click += new System.EventHandler(this.createNewToolStripMenuItem_Click);
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.loadToolStripMenuItem.Text = "Open Project";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.closeToolStripMenuItem.Text = "Exit";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // logToolStripMenuItem
            // 
            this.logToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.applicationLogToolStripMenuItem,
            this.dNNLogToolStripMenuItem});
            this.logToolStripMenuItem.Name = "logToolStripMenuItem";
            this.logToolStripMenuItem.Size = new System.Drawing.Size(50, 23);
            this.logToolStripMenuItem.Text = "Log";
            // 
            // applicationLogToolStripMenuItem
            // 
            this.applicationLogToolStripMenuItem.Name = "applicationLogToolStripMenuItem";
            this.applicationLogToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.applicationLogToolStripMenuItem.Text = "Application Log";
            this.applicationLogToolStripMenuItem.Click += new System.EventHandler(this.applicationLogToolStripMenuItem_Click);
            // 
            // dNNLogToolStripMenuItem
            // 
            this.dNNLogToolStripMenuItem.Name = "dNNLogToolStripMenuItem";
            this.dNNLogToolStripMenuItem.Size = new System.Drawing.Size(201, 26);
            this.dNNLogToolStripMenuItem.Text = "DNN Log";
            this.dNNLogToolStripMenuItem.Click += new System.EventHandler(this.dNNLogToolStripMenuItem_Click);
            // 
            // toolToolStripMenuItem
            // 
            this.toolToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.activeLearningRelabelToolStripMenuItem});
            this.toolToolStripMenuItem.Name = "toolToolStripMenuItem";
            this.toolToolStripMenuItem.Size = new System.Drawing.Size(53, 23);
            this.toolToolStripMenuItem.Text = "Tool";
            this.toolToolStripMenuItem.Visible = false;
            // 
            // activeLearningRelabelToolStripMenuItem
            // 
            this.activeLearningRelabelToolStripMenuItem.Name = "activeLearningRelabelToolStripMenuItem";
            this.activeLearningRelabelToolStripMenuItem.Size = new System.Drawing.Size(255, 26);
            this.activeLearningRelabelToolStripMenuItem.Text = "Active Learning Relabel";
            this.activeLearningRelabelToolStripMenuItem.Visible = false;
            this.activeLearningRelabelToolStripMenuItem.Click += new System.EventHandler(this.activeLearningRelabelToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(65, 23);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(728, 504);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Training Tool";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tab_step1.ResumeLayout(false);
            this.tab_step1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgview_labels)).EndInit();
            this.tab_step2.ResumeLayout(false);
            this.tab_step2.PerformLayout();
            this.panel_augmentation.ResumeLayout(false);
            this.panel_augmentation.PerformLayout();
            this.panel_advanced.ResumeLayout(false);
            this.panel_advanced.PerformLayout();
            this.tab_step3.ResumeLayout(false);
            this.tab_step3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart_loss)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tab_confusion.ResumeLayout(false);
            this.tab_confusion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_confusion_matrix_testing)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_confusion_matrix_validation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_confusion_matrix_training)).EndInit();
            this.tab_threshold.ResumeLayout(false);
            this.tab_threshold.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_threshold)).EndInit();
            this.tab_inference.ResumeLayout(false);
            this.tab_inference.PerformLayout();
            this.gbox_pred_accuracy.ResumeLayout(false);
            this.gbox_pred_accuracy.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_inference_image)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_inference_result)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tab_presorting.ResumeLayout(false);
            this.tab_presorting.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab_step1;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.TabPage tab_step2;
        private System.Windows.Forms.Button btnStep2Next;
        private System.Windows.Forms.TabPage tab_step3;
        private System.Windows.Forms.TextBox txtTrainingImageCount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtMeanValue_B;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtMeanValue_G;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtMeanValue_R;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtMomentum;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtBaseLearningRate;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox txtWeightDecay;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtValidationBatchSize;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtTrainingBatchSize;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtSnapshotInverval;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtTrainingEpochs;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckBox chk_GPU3;
        private System.Windows.Forms.CheckBox chk_GPU2;
        private System.Windows.Forms.CheckBox chk_GPU1;
        private System.Windows.Forms.CheckBox chk_GPU0;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Button btn_stopTraining;
        private System.Windows.Forms.Button btn_startTraining;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem createNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button btn_generate_mean;
        private System.Windows.Forms.ListBox lstFileListOutput;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnGenerateList;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTestPercentage;
        private System.Windows.Forms.TextBox txtValidatoinPercentage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnImagesBrowse;
        private System.Windows.Forms.TextBox txtImagesFolder;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtTrainingPercentage;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ProgressBar progressBar_data;
        private System.Windows.Forms.Label lab_training_status;
        private System.Windows.Forms.Button btn_browse_prebuilt_file;
        private System.Windows.Forms.TextBox txt_prebuilt_file;
        private System.Windows.Forms.TextBox txt_curr_validation_loss;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txt_curr_training_loss;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txt_curr_iterations;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtTestingImageCount;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtValidationImageCount;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txt_max_iterations;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.CheckBox ckb_advanced;
        private System.Windows.Forms.Panel panel_advanced;
        private System.Windows.Forms.ComboBox cmb_prebuilt_model;
        private System.Windows.Forms.Button btn_output;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart_loss;
        private System.Windows.Forms.Label lab_timeInfo;
        private System.Windows.Forms.ProgressBar progressBar_calcMean;
        private System.Windows.Forms.Button btn_cancel_mean_calc;
        private System.Windows.Forms.DataGridView dgview_labels;
        private System.Windows.Forms.DataGridViewTextBoxColumn label_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn label_code;
        private System.Windows.Forms.TabPage tab_confusion;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DataGridView dgv_confusion_matrix_training;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lab_accuracy_training;
        private System.Windows.Forms.TabPage tab_threshold;
        private System.Windows.Forms.Button btn_generate_param_file;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.DataGridView dgv_threshold;
        private System.Windows.Forms.CheckBox chk_confusion_autofit_column;
        private System.Windows.Forms.CheckBox chk_threshold_adjust;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.TabPage tab_inference;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox cbox_gpu_list;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_browse_inference_folder;
        private System.Windows.Forms.TextBox txt_inference_target_folder;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cbox_ground_truth;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button btn_run_inference;
        private System.Windows.Forms.GroupBox gbox_pred_accuracy;
        private System.Windows.Forms.PictureBox pic_inference_image;
        private System.Windows.Forms.DataGridView dgv_inference_result;
        private System.Windows.Forms.MaskedTextBox mtxt_batch_size_of_inference;
        private System.Windows.Forms.MaskedTextBox mtxt_threshold_for_inference;
        private System.Windows.Forms.Label label_target_image;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.Button btn_run_confusion_mat;
        private System.Windows.Forms.DataGridView dgv_confusion_matrix_validation;
        private System.Windows.Forms.DataGridView dgv_confusion_matrix_testing;
        private System.Windows.Forms.Button button1btn_run_confusion_mat_testing;
        private System.Windows.Forms.Button btn_run_confusion_mat_validation;
        private System.Windows.Forms.CheckBox chk_confusion_autofit_testing;
        private System.Windows.Forms.CheckBox chk_confusion_autofit_validation;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button btn_open_optimal_folder;
        private System.Windows.Forms.Label lab_accuracy_testing;
        private System.Windows.Forms.Label lab_accuracy_validation;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TabPage tab_presorting;
        private System.Windows.Forms.MaskedTextBox mtxt_batch_size_of_presorting;
        private System.Windows.Forms.MaskedTextBox mtxt_threshold_for_presorting;
        private System.Windows.Forms.Button btn_run_presorting;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cbox_presorting_gpu_list;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_browse_src;
        private System.Windows.Forms.TextBox txt_presorting_src_folder;
        private System.Windows.Forms.Button btn_browse_dest;
        private System.Windows.Forms.TextBox txt_presorting_dest_folder;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button btn_check_dest_folder;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Panel panel_augmentation;
        private System.Windows.Forms.ComboBox cmb_downsampling_factor;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textDeltaWidthHeight_training;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox textNewSize_validation;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textNewSize_training;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox textCropSize_validation;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.TextBox textCropSize_training;
        private System.Windows.Forms.CheckBox ckb_Rotation;
        private System.Windows.Forms.CheckBox ckb_VerticalFlip;
        private System.Windows.Forms.CheckBox ckb_HorizontalFilp;
        private System.Windows.Forms.ComboBox cmb_pseudoBalanceFactor;
        private System.Windows.Forms.CheckBox chk_GPU7;
        private System.Windows.Forms.CheckBox chk_GPU6;
        private System.Windows.Forms.CheckBox chk_GPU5;
        private System.Windows.Forms.CheckBox chk_GPU4;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtGamma;
        private System.Windows.Forms.ComboBox cmb_top_N_model;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button btn_misclassified_move_train;
        private System.Windows.Forms.Button btn_misclassified_export_browse_train;
        private System.Windows.Forms.TextBox txt_misclassified_export_dir_train;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Button btn_misclassified_move_test;
        private System.Windows.Forms.Button btn_misclassified_export_browse_test;
        private System.Windows.Forms.TextBox txt_misclassified_export_dir_test;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button btn_misclassified_move_val;
        private System.Windows.Forms.Button btn_misclassified_export_browse_val;
        private System.Windows.Forms.TextBox txt_misclassified_export_dir_val;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ToolStripMenuItem logToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem applicationLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dNNLogToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activeLearningRelabelToolStripMenuItem;
        private System.Windows.Forms.Button btn_imagePreProcessing;
    }
}

