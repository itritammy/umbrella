﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace class_trainer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            /// check app name
            string app_name = System.IO.Path.GetFileName(
                System.Diagnostics.Process.GetCurrentProcess().MainModule.FileName);

            /// Check 連線特定 DNS 尾碼
            int NetDNS_Pass = 1; // 2020_0205,Jim: Now always alow to open multiple Apps at the same time.
            //string domainName = System.Net.NetworkInformation.IPGlobalProperties.GetIPGlobalProperties().DomainName;
            //System.Net.NetworkInformation.NetworkInterface[] adapters = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
            //foreach (System.Net.NetworkInformation.NetworkInterface adapter in adapters)
            //{
            //    System.Net.NetworkInformation.IPInterfaceProperties properties = adapter.GetIPProperties();
            //    if (properties.DnsSuffix == "itri.ds.")
            //    { 
            //        NetDNS_Pass = 1;
            //        break;
            //    }
            //}

            /// Check App
            bool result;
            var mutex = new System.Threading.Mutex(true, "UniqueAppId", out result);
            if (NetDNS_Pass == 0)
            {
                if (!(app_name == "training_tool.exe" || app_name == "training_tool.vshost.exe"))
                {
                    MessageBox.Show(
                        "Please don't change the program name.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
            
                if (!result)
                {
                    MessageBox.Show(
                        "Another instance is already running.",
                        "Warning",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);
                    return;
                }
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());

            GC.KeepAlive(mutex);
        }
    }
}
