﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Media;

using trainer_CLR;
using System.Diagnostics;
using System.Threading;
using System.Deployment.Application;


namespace class_trainer
{
    public partial class Form1 : Form
    {
        public class CUDA_GPU
        {
            public int id { get; set; }
            public string name { get; set; }
        }

        public enum ThreadID
        {
            TrainResult = 0,
            Timer = 1,
            CalcMean = 2,
            CheckMean = 3,
            WaitForm = 4,
            CalcConfusionMat = 5,
            Inference = 6,
            MAX = 7
        }

        public string prj_name = string.Empty;
        public string prj_location = string.Empty;
        //public string prj_machine_type = string.Empty;
        public string prj_model_path = string.Empty;
        public string prj_published_model_name = string.Empty;

        ManagedUtils pObj = null;
        trainer_CLR.TrainerWrapper pTrainerObj = null;
        private Process process;
        private ProcessStartInfo startInfo;
        BackgroundWorker ResultWorker = null;
        BackgroundWorker TimerWorker = null;
        BackgroundWorker CalcMeanWorker = null;
        BackgroundWorker CheckMeanWorker = null;
        //BackgroundWorker WaitingFormWorker = null;
        Thread[] bgWorkersThreads = new Thread[(int)ThreadID.MAX];  //List<Thread> bgWorkersThreads = new List<Thread>();
        
        List<float> bgr_list = new List<float>();

        Melt_API melt_api = new Melt_API();

        ulong m_total_num_for_training = 0;
        ulong m_total_num_for_validation = 0;
        ulong m_total_num_for_testing = 0;

        ulong m_num_of_training_for_pseudo_balance = 0;

        ulong g_iter_num_for_display = 0;
        string m_optimal_model_filename = "";
        string m_log_filename = Globals.GetApplicationLog(); //"log/trainer.log";

        frm_waiting m_form_wait = null;
        bool m_IsFormClosing = false;

        CheckBox[] m_ckb_GPUlist;
        int m_MAX_GPUs = 8;
        int m_prev_iter = -1;

        string m_threshold_old;
        // for gridview drap and drop
        int rowIndexFromMouseDown;
        //DataGridViewRow rw;
        private Rectangle dragBoxFromMouseDown;


        List<Tuple<int, string>> m_gpu_list = new List<Tuple<int, string>>();

        public Form1()
        {
            InitializeComponent();

            //  remove the minimize and maximize buttons
            this.MaximizeBox = false;
            this.MinimizeBox = false;

            if (pObj == null)
            {
                pObj = new ManagedUtils();
                pObj.LogInit(m_log_filename);
            }

            bgr_list.Clear();
            bgr_list.Add(127);
            bgr_list.Add(128);
            bgr_list.Add(129);

            for(int i = 0; i < (int)ThreadID.MAX; i++)
                bgWorkersThreads[i] = null;

            init_ResultWorker();
            init_timer();
            init_calc_image_mean();
            init_check_mena_progress();

            /// set initial UI values
            this.cmb_prebuilt_model.SelectedIndex = 0;
            //this.cmb_downsampling_factor.SelectedIndex = 1;
            //this.cmb_pseudoBalanceFactor.SelectedIndex = 0;
            m_ckb_GPUlist = new CheckBox[] { chk_GPU0, chk_GPU1, chk_GPU2, chk_GPU3, chk_GPU4, chk_GPU5, chk_GPU6, chk_GPU7 };

            /// Remove tabs. e.g., tabControl1.TabPages.RemoveByKey("tabPage6");
            List<Tuple<string, bool>> available_tabs = get_tab_permissions();
            for(int i = 0; i < available_tabs.Count; i++)
            {
                if ( available_tabs.ElementAt(i).Item2 == false )
                {
                    string tab_name = available_tabs.ElementAt(i).Item1;
                    tabControl1.TabPages.RemoveByKey(tab_name);
                }
            }
        }
        
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            //close_waiting_form();
        }


        private void release_resource ()
        {
            m_IsFormClosing = true;
            cancel_mean_calc();
            cancel_training();

            if (TimerWorker != null)
                TimerWorker.CancelAsync();

            if (ResultWorker != null)
                ResultWorker.CancelAsync();

            
            /// wait for background thread stop
            for (int i = 0; i < (int)ThreadID.MAX; i++)
            {
                if (bgWorkersThreads[i] != null)
                {
                    while (bgWorkersThreads[i].IsAlive)
                    {
                        System.Threading.Thread.Sleep(800);
                    }
                    bgWorkersThreads[i].Abort();
                }
            }

            if (pTrainerObj != null)
                pTrainerObj.DestroyInstant();

            if (m_form_wait != null)
                m_form_wait.Dispose();

        }


        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Exit or not?",
                    "Training Tool",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Information) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            //show_waiting_form();
            //release_resource();
            if (pObj != null && pObj.isConfigReady())
            {
                save_model_params();
                pObj.CFG_Save();
                pObj.LogRelease();
            }

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ( tabControl1.TabPages[tabControl1.SelectedIndex].Name == "tab_confusion" )
                loadConfusionMatrix();

            if (tabControl1.TabPages[tabControl1.SelectedIndex].Name == "tab_threshold")
                LoadThreshold_gridView();

            if (tabControl1.TabPages[tabControl1.SelectedIndex].Name == "tab_inference")
                init_inference_tab();

            if (tabControl1.TabPages[tabControl1.SelectedIndex].Name == "tab_presorting")
                init_presorting_tab();

            if (tabControl1.TabPages[tabControl1.SelectedIndex].Name == "tab_step2") // training parameters
                custom_tab_ModelParam();

            if (tabControl1.TabPages[tabControl1.SelectedIndex].Name == "tab_step3") // training process
                init_training_process_tab();

            ///// Show Confusion matrix
            //if (available_tabs.ElementAt(4).Item2 == true)
            //{
            //    if (tabControl1.SelectedTab == tabControl1.TabPages[4])
            //        loadConfusionMatrix();
            //}

            ///// Show Threshold/accuracy grid view
            //if (available_tabs.ElementAt(4).Item2 == true)
            //    if (tabControl1.SelectedTab == tabControl1.TabPages[4])
            //        LoadThreshold_gridView();

            ///// Show Inference
            //if (tabControl1.TabCount > 5)
            //    if (tabControl1.SelectedTab == tabControl1.TabPages[5])
            //        init_inference_tab();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Training tool\nVersion " + Globals.GetAppVersion(), "About", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void tabControl1_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            int idx = tabControl1.SelectedIndex;

            if (idx == 0)
            {
                pObj.CFG_Save();
            }
            else if (idx == 1)
            {
                if (check_GPU_selection() == 0)
                {
                    MessageBox.Show("Please select at leat onoe GPU.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                save_model_params();
            }
        }

        private void show_waiting_form()
        {
            /// show waiting form
            if (m_form_wait == null)
                m_form_wait = new frm_waiting();
            m_form_wait.Owner = this;
            m_form_wait.StartPosition = FormStartPosition.CenterParent;
            m_form_wait.Show(this);
        }


        private void close_waiting_form()
        {
            // close by flag since it's on another UI thread
            if (m_form_wait != null)
                m_form_wait.m_waiting = false;
        }

        private bool load_ProjectConfig (string cfg_filename)
        {
            if (pObj == null)
            {
                pObj = new ManagedUtils();
                pObj.LogInit(m_log_filename);
            }

            if (pObj == null)
                return false;

            if (pObj.LoadConfig(cfg_filename) != ManagedUtils.ERROR_t.ERR_NO_ERROR)
                return false;

            return true;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            this.tabControl1.SelectedTab = tab_step2;

            /// store
            /// pObj.CFG_Save();  /// called by tabControl1_Deselecting()
        }

        private void UpdateLabelsToProject()
        {
            /// update the number of labels
            pObj.CFG_AddIntegerValue("common", "num_of_labels", dgview_labels.RowCount);

            /// Remove old labels
            pObj.CFG_RemoveSection("labels");

            // pObj.CFG_AddStringValue("labels", "0", "normal", "ba1");
            for (int i = 0; i < dgview_labels.RowCount; i++)
            {
                pObj.CFG_AddStringValue("labels",
                    i.ToString(),
                    dgview_labels.Rows[i].Cells[0].Value.ToString(),
                    dgview_labels.Rows[i].Cells[1].Value.ToString());
            }
            pObj.CFG_Save();
        }

        private void btnGenerateList_Click(object sender, EventArgs e)
        {
            if (prj_location == "" || pObj == null)
            {
                MessageBox.Show("Training core or project is not ready.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            pObj.LogWrite("Start to generate image mapping lists.", 0);

            if (dgview_labels.RowCount == 0)
            {
                MessageBox.Show("Images and Labels not assigned.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            string prefix_name = prj_location + "/temp";
            try
            {
                melt_api.EmptyDirectory(prefix_name);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            /// update labels
            UpdateLabelsToProject();

            lstFileListOutput.Items.Clear();
            /// create out files

            /* These files are generated here.
            string validation_list = prj_location + "/" + pObj.CFG_Get("validation", "source_images", "validation_images.txt");
            string testing_list = prj_location + "/" + pObj.CFG_Get("testing", "source_images", "testing_images.txt");
            string training_list = prj_location + "/" + pObj.CFG_Get("training", "source_images", "training_images.txt");
            */
            
            string validation_list = prj_location + "/validation_images.txt";
            string testing_list = prj_location + "/testing_images.txt";
            string training_list = prj_location + "/training_images.txt";

            // Delete previous mapping files
            try
            {
                if (System.IO.File.Exists(validation_list) == true)
                    System.IO.File.Delete(validation_list);

                if (System.IO.File.Exists(testing_list) == true)
                    System.IO.File.Delete(testing_list);

                if (System.IO.File.Exists(training_list) == true)
                    System.IO.File.Delete(training_list);
            } catch (Exception ex)
            {
                MessageBox.Show("Delete " + validation_list + ", " + testing_list + ", " + training_list + "Failed.", ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            
            StreamWriter[] dest_stream = new StreamWriter[3];
            //FileStream fs = null;

            //fs = new FileStream(validation_list, FileMode.Create);
            //dest_stream[0] = new StreamWriter(fs, Encoding.Default);
            //fs = new FileStream(testing_list, FileMode.Create);
            //dest_stream[1] = new StreamWriter(fs, Encoding.Default);
            //fs = new FileStream(training_list, FileMode.Create);
            //dest_stream[2] = new StreamWriter(fs, Encoding.Default);


            /// dispatch images
            int validation_percentage = Int32.Parse(txtValidatoinPercentage.Text);
            int testing_percentage = Int32.Parse(txtTestPercentage.Text);
            int training_percentage = 100 - validation_percentage - testing_percentage;


            melt_api.CreateDirectory(prefix_name);
            m_total_num_for_training = 0;
            m_total_num_for_validation = 0;
            m_total_num_for_testing = 0;
            float file_counts_for_giving_label = 0;
            string img_src_path = "";
            for (int i = 0; i < dgview_labels.RowCount; i++)
            {
                int label_id = i;
                string filename = prefix_name + "/" + label_id + "_" + dgview_labels.Rows[i].Cells[0].Value + ".txt";
                img_src_path = txtImagesFolder.Text + "/" + dgview_labels.Rows[i].Cells[0].Value;

                //float file_counts_for_giving_label = pObj.Generate_ImageList(filename, img_src_path, label_id);
                file_counts_for_giving_label = pObj.Generate_ImageList(filename, img_src_path, i);
                if (file_counts_for_giving_label == 0)
                    break;

                ulong count_for_validation = (ulong)(file_counts_for_giving_label * validation_percentage / 100);
                ulong count_for_testing = (ulong)(file_counts_for_giving_label * testing_percentage / 100);
                ulong count_for_training = (ulong)(file_counts_for_giving_label - count_for_validation - count_for_testing);

                //melt_api.dispatchImageList(filename, count_for_validation, count_for_testing, count_for_training, dest_stream);
                pObj.Dispatch_ImgList (filename, count_for_training, count_for_validation, count_for_testing,
                    training_list, validation_list, testing_list);


                m_total_num_for_training += count_for_training;
                m_total_num_for_validation += count_for_validation;
                m_total_num_for_testing += count_for_testing;

                pObj.CFG_AddStringValue("labels",
                    i.ToString(),
                    dgview_labels.Rows[i].Cells[0].Value.ToString(),
                    dgview_labels.Rows[i].Cells[1].Value.ToString());


                /// keep the count of images for training
                pObj.CFG_AddUInteger64Value("labels",
                    i.ToString(),
                    "quantity_for_training",
                    count_for_training
                    );
                
            }

            /// close output files
            //dest_stream[0].Close();
            //dest_stream[1].Close();
            //dest_stream[2].Close();

            if (file_counts_for_giving_label == 0)
            {
                MessageBox.Show("Image source directory is empty, please check\n" + img_src_path, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            txtTrainingImageCount.Text = m_total_num_for_training.ToString();


            /// create out files
            pObj.CFG_AddStringValue("training", "source_images", training_list);
            pObj.CFG_AddStringValue("validation", "source_images", validation_list);
            pObj.CFG_AddStringValue("testing", "source_images", testing_list);

            /// update project config
            pObj.CFG_AddIntegerValue("validation", "percentage", Int32.Parse(txtValidatoinPercentage.Text));
            pObj.CFG_AddIntegerValue("testing", "percentage", Int32.Parse(txtTestPercentage.Text));
            pObj.CFG_AddIntegerValue("training", "percentage", Int32.Parse(txtTrainingPercentage.Text));

            pObj.CFG_AddUInteger64Value("training", "numbers", m_total_num_for_training);
            pObj.CFG_AddUInteger64Value("validation", "numbers", m_total_num_for_validation);
            pObj.CFG_AddUInteger64Value("testing", "numbers", m_total_num_for_testing);

            pObj.CFG_AddUInteger64Value("training", "numbers_p", m_total_num_for_training);
            m_num_of_training_for_pseudo_balance = m_total_num_for_training;

            txtTrainingImageCount.Text = m_total_num_for_training.ToString();
            txtValidationImageCount.Text = m_total_num_for_validation.ToString();
            txtTestingImageCount.Text = m_total_num_for_testing.ToString();

            ////update_max_iterations();
            //pObj.CFG_Save();

            ///// generate pseudo copy training list
            //if (pTrainerObj != null)
            //{
            //    pTrainerObj.GeneratePsuedoCopyList(prj_location, prj_location + "/" + prj_name + ".proj");
            //}
            //pObj.LoadConfig(prj_location + "/" + prj_name + ".proj");   /// Reload config for pseudo copy add path and number in config.

            //m_num_of_training_for_pseudo_balance = (ulong) pObj.CFG_GetDouble("training", "numbers_p", 0);
            //update_max_iterations();

            /// update list view
            lstFileListOutput.Items.Add(validation_list);
            lstFileListOutput.Items.Add(testing_list);
            lstFileListOutput.Items.Add(training_list);
            MessageBox.Show("Generate mapping files completed.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);

            pObj.LogWrite("Image mappings for training is in " + training_list, 0);
            pObj.LogWrite("Image mappings for validation is in " + validation_list, 0);
            pObj.LogWrite("Image mappings for testing is in " + testing_list, 0);

            update_max_iterations();
        }


        private void add_default_values(string published_model_name, string prj_name)
        {
            pObj.CFG_AddStringValue("common", "version", Globals.GetAppVersion());
            pObj.CFG_AddStringValue("common", "project_name", prj_name);
            pObj.CFG_AddStringValue("common", "published_model_name", published_model_name);
            pObj.CFG_AddStringValue("common", "project_location", prj_location);
            pObj.CFG_AddStringValue("model_params", "model_path", prj_model_path);

            

            // 2020_0214, Jim: Move the below code to init_training_process_tab()
            //string default_pretrained_model = pObj.CFG_Get("common", "default_pretrained_model", "pretrained_df.caffemodel");
            //pObj.CFG_AddStringValue("model_params", "model_file", prj_location + "/models/" + prj_machine_type + "/" + default_pretrained_model); 

            pObj.CFG_Save();
        }

        /// clear previous GUI
        private void clear_GUI()
        {
            /// Step1:
            dgview_labels.Rows.Clear();
            dgview_labels.Refresh();
            lstFileListOutput.Items.Clear();
            lstFileListOutput.Refresh();

            /// Step2: tab_training_parameters
            txtTrainingImageCount.Text = "0";
            txtValidationImageCount.Text = "0";
            txtTestingImageCount.Text = "0";
            txtMeanValue_R.Text = "127";
            txtMeanValue_G.Text = "128";
            txtMeanValue_B.Text = "129";
            for (int i = 0; i < m_MAX_GPUs; i++)
                m_ckb_GPUlist[i].Checked = false;

            // Step3: tab_training_process
            cmb_prebuilt_model.SelectedIndex = 0;
            txt_prebuilt_file.Text = "";
            chart_loss.Series[0].Points.Clear();
            chart1.Series[0].Points.Clear();
            chart1.Series[1].Points.Clear();
            txt_curr_training_loss.Text = "0";
            txt_curr_iterations.Text = "0";
            txt_curr_validation_loss.Text = "0";
            txt_prebuilt_file.Text = "";
            lab_timeInfo.Text = "Time Elapsed: \n";

            // Step4: Confusion Matrix
            dgv_confusion_matrix_training.Rows.Clear();
            dgv_confusion_matrix_validation.Rows.Clear();
            dgv_confusion_matrix_testing.Rows.Clear();

            // Threshold: tab_threshold
            if (tabControl1.TabPages.ContainsKey("tab_threshold") == true)
                dgv_threshold.Rows.Clear();

            /// switch to tab1
            tabControl1.SelectedIndex = 0;
        }

        private void createNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            /// Pop up new window to create new project
            frmNewProj frmPop = new frmNewProj();
            frmPop.StartPosition = FormStartPosition.CenterParent;
            DialogResult dialog_result = frmPop.ShowDialog(); 

            if (dialog_result == DialogResult.OK)
            {
                if (frmPop.error != 0)
                    return;

                prj_name = frmPop.prj_name;
                prj_location = frmPop.prj_location;
                prj_location = prj_location.Replace(@"\", @"/");
                //prj_machine_type = frmPop.prj_machine_type;
                prj_published_model_name = frmPop.prj_published_model_name;
                prj_model_path = frmPop.prj_model_path;
                prj_model_path = prj_model_path.Replace(@"\", @"/");

                this.Text = "Project : " + prj_location;

                if (load_ProjectConfig(frmPop.prj_location + "/" + frmPop.prj_name + ".proj" ) == false)
                {
                    MessageBox.Show("Load project configuration failed.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    tabControl1.Visible = true;
                    /// clear previous GUI
                    clear_GUI();

                    /// add default values for specified model
                    add_default_values(prj_published_model_name, prj_name);

                    /// update UI
                    update_UI_from_config();

                    pObj.LogWrite("New project " + prj_location +  "created.", 0);
                }

            }
            frmPop.Dispose();

        }


        private void CheckProjectLocation(string prj_location)
        {
            string pseudo_source_images = pObj.CFG_Get("training", "pseudo_source_images", "");
            string location = pseudo_source_images.Replace("/temp/pseudo_list.txt","");

            if (prj_location == location)
                return;

            /// Update path of parameters
            string prj_name = pObj.CFG_Get("common", "project_name", "undified");
            string prj_published_model_name = pObj.CFG_Get("common", "published_model_name", "undified");
            string default_pretrained_model = pObj.CFG_Get("common", "default_pretrained_model", "pretrained_df.caffemodel");


            pObj.CFG_AddStringValue("common", "project_location", prj_location);
            //pObj.CFG_AddStringValue("model_params", "model_path", prj_location + "/models");
            prj_model_path = prj_location + "/model";
            pObj.CFG_AddStringValue("model_params", "model_path", prj_model_path);
            

            /// create out files
            pObj.CFG_AddStringValue("training", "source_images", prj_location + "/training_images.txt");
            pObj.CFG_AddStringValue("validation", "source_images", prj_location + "/validation_images.txt");
            pObj.CFG_AddStringValue("testing", "source_images", prj_location + "/testing_images.txt");

            pObj.CFG_AddStringValue("training", "pseudo_source_images", prj_location + "/temp/pseudo_list.txt");
            //pObj.CFG_AddStringValue("model_params", "train_net", prj_location + "/models/" + prj_published_model_name + "/train_net.aes");
            //pObj.CFG_AddStringValue("model_params", "test_net", prj_location + "/models/" + prj_published_model_name + "/val_net.aes");
            pObj.CFG_AddStringValue("model_params", "train_net", prj_model_path + "/train_net.aes");
            pObj.CFG_AddStringValue("model_params", "test_net", prj_model_path + "/val_net.aes");
            pObj.CFG_AddStringValue("model_params", "snapshot_prefix", prj_location + "/snapshot/" + prj_name);
            pObj.CFG_AddStringValue("model_params", "model_path", prj_model_path);  /// 2020_0518,Jim: To keep the path for trainer.cpp
            pObj.CFG_AddStringValue("model_params", "model_file", prj_model_path + "/" + default_pretrained_model);
            pObj.CFG_Save();
        }


        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string FileName;

            openFileDialog1.Filter = "Project Files (*.proj)|*.proj";
            openFileDialog1.Multiselect = false;
            
            // Show the FolderBrowserDialog.
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                FileName = openFileDialog1.FileName;

                /// Get folder name
                System.IO.FileInfo fInfo = new System.IO.FileInfo(FileName);
                prj_location = fInfo.DirectoryName;
                prj_location = prj_location.Replace(@"\", @"/");
                //prj_model_path = prj_location + "/models";
                prj_model_path = prj_location + "/model";

                this.Text = "Project : " + prj_location;

                pObj.LogWrite("Loading project " + prj_location, 0);

                if (load_ProjectConfig(FileName) == false)
                {
                    MessageBox.Show("Load project configuration failed.");
                } 
                else
                {
                    tabControl1.Visible = true;
                    /// clear previous GUI
                    clear_GUI();

                    /// Is the project location moved ?
                    CheckProjectLocation(prj_location);

                    /// update UI
                    update_UI_from_config();
                    pObj.LogWrite("Project " + prj_location + " is loaded.", 0);
                }
            }
        }

        private void btnImagesBrowse_Click(object sender, EventArgs e)
        {
            string folderName;

            // Show the FolderBrowserDialog.
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                folderName = folderBrowserDialog1.SelectedPath;
                folderName = folderName.Replace(@"\", @"/");
                txtImagesFolder.Text = folderName;

                if (pObj != null)
                {
                    /// keep image folder
                    pObj.CFG_AddStringValue("common", "image_folder_path", folderName);
                    genLabels_byImageFolder(folderName);
                }
            }

            pObj.LogWrite("Set image folder to " + txtImagesFolder.Text, 0);
        }



        private void genLabels_byImageFolder(string folderName)
        {
            pObj.LogWrite("Start to generate labels from given image folder " + folderName, 0);

            List<string> pic_labels = new List<string>();
            pic_labels.Clear();

            ManagedUtils.ERROR_t ret = pObj.Get_ImageLabels(folderName, pic_labels);

            int max_NbOf_category = get_Max_NbOf_category();
            if (pic_labels.Count > max_NbOf_category)
            {
                MessageBox.Show("The maximum number of support classes is " + max_NbOf_category + ". Please check your image folders in "+ folderName , "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            if (ret == ManagedUtils.ERROR_t.ERR_NO_ERROR)
            {
                dgview_labels.Rows.Clear();
                dgview_labels.Refresh();
                for (int k = 0; k < pic_labels.Count && k < max_NbOf_category; k++)
                {
                    string id_code = get_ID_code_by_categoryID(k);
                    string[] new_row = new string[] { pic_labels[k], id_code };
                    dgview_labels.Rows.Add(new_row);
                    dgview_labels.Rows[k].Cells[dgview_labels.ColumnCount-1].Style.BackColor = Color.BlanchedAlmond;
                }
            }

            setRowNumber(dgview_labels);

            /// update labels
            UpdateLabelsToProject();

            pObj.LogWrite("labels is updated completely", 0);
        }


        private void show_labels(int num)
        {
            dgview_labels.Rows.Clear();
            dgview_labels.Refresh();
            for (int k = 0; k < num; k++)
            {
                string name = pObj.CFG_GetFirstChildKey("labels", k.ToString(), "");
                string code = pObj.CFG_GetFirstChildValue("labels", k.ToString(), "");

                string[] new_row = new string[] { name, code };
                dgview_labels.Rows.Add(new_row);
                dgview_labels.Rows[k].Cells[dgview_labels.ColumnCount - 1].Style.BackColor = Color.BlanchedAlmond;
            }
            setRowNumber(dgview_labels);            
        }


        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if ( pObj.CFG_Save() == ManagedUtils.ERROR_t.ERR_NO_ERROR)
                MessageBox.Show("Config save successful");
            else
                MessageBox.Show("Config save failed");
        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRefreshLables_Click(object sender, EventArgs e)
        {
            if (txtImagesFolder.Text != "")
                genLabels_byImageFolder(txtImagesFolder.Text);
        }

        private void lstFileListOutput_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            int index = lstFileListOutput.IndexFromPoint(e.Location); /// start from 0
            if (index != System.Windows.Forms.ListBox.NoMatches)
            {
                process = new System.Diagnostics.Process();
                startInfo = new System.Diagnostics.ProcessStartInfo();
                //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                startInfo.CreateNoWindow = false;
                startInfo.FileName = "notepad.exe"; // C:\\WINDOWS\\system32\\notepad.exe
                startInfo.Arguments = lstFileListOutput.Items[index].ToString();
                process.StartInfo = startInfo;
                process.Start();
            }
        }


        private void txtTestPercentage_Leave(object sender, EventArgs e)
        {
            computePercentage();
        }

        private void txtValidatoinPercentage_Leave(object sender, EventArgs e)
        {
            computePercentage();
        }

        private int computePercentage()
        {
            int validation_percentage = Int32.Parse(txtValidatoinPercentage.Text);
            int testing_percentage = Int32.Parse(txtTestPercentage.Text);
            int training_percentage = 100 - validation_percentage - testing_percentage;

            txtTrainingPercentage.Text = training_percentage.ToString();

            return 0;
        }

        private void update_GPU_list ()
        {
            // Get available GPU
            //pObj.CFG_AddBoolValue("model_params", "enable_GPU0", false);
            //pObj.CFG_AddBoolValue("model_params", "enable_GPU1", false);
            //pObj.CFG_AddBoolValue("model_params", "enable_GPU2", false);
            //pObj.CFG_AddBoolValue("model_params", "enable_GPU3", false);


            /// inital to false
            m_gpu_list.Clear();
            for (int i = 0; i < m_MAX_GPUs; i++)
            {
                m_ckb_GPUlist[i].Checked = false;
                string target_GPU = "enable_GPU" + i.ToString();
                pObj.CFG_AddBoolValue("model_params", target_GPU, false);
            }

            int cnt = pTrainerObj.GetCUDA_GPU_list(m_gpu_list);
            for (int i = 0; i < cnt; i++)
            {
                // update GUI
                m_ckb_GPUlist[i].Visible = true;
                m_ckb_GPUlist[i].Checked = true;
                m_ckb_GPUlist[i].Text = "GPU" + i + " : " + m_gpu_list[i].Item2;
                // update config
                int gpu_id = m_gpu_list[i].Item1;
                string target_GPU = "enable_GPU" + gpu_id.ToString();
                pObj.CFG_AddBoolValue("model_params", target_GPU, true);
            }
        }

        private bool update_UI_from_config()
        {
            if (pObj == null)
                return false;

            if (pTrainerObj == null)
            {
                pTrainerObj = new trainer_CLR.TrainerWrapper();
                if (pTrainerObj.CreatInstant() != 0)
                    MessageBox.Show("Load DLL failed.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

            /// project info
            prj_name = pObj.CFG_Get("common", "project_name", "undified");
            prj_published_model_name = pObj.CFG_Get("common", "published_model_name", "undified");
            txtImagesFolder.Text = pObj.CFG_Get("common", "image_folder_path", "");

            /// show data percentage
            int iV = pObj.CFG_GetInteger("validation", "percentage", 15);
            int iT = pObj.CFG_GetInteger("testing", "percentage", 15);
            int iN = pObj.CFG_GetInteger("training", "percentage", 15);
            txtValidatoinPercentage.Text = iV.ToString();
            txtTestPercentage.Text = iV.ToString();
            txtTrainingPercentage.Text = iN.ToString();

            /// show classification labels
            int num = pObj.CFG_GetInteger("common", "num_of_labels", 0);
            if (num != 0)
                show_labels(num);

            /// show  source image lists
            string filename = pObj.CFG_Get("training", "source_images", "");
            if (filename.Length > 0)
            {
                lstFileListOutput.Items.Clear();
                lstFileListOutput.Items.Add(filename);
                filename = pObj.CFG_Get("validation", "source_images", "");
                lstFileListOutput.Items.Add(filename);
                filename = pObj.CFG_Get("testing", "source_images", "");
                lstFileListOutput.Items.Add(filename);
            }

            /// apply model values by config
            txtMeanValue_B.Text = (pObj.CFG_GetDouble("training", "mean_b", bgr_list[0])).ToString();
            txtMeanValue_G.Text = (pObj.CFG_GetDouble("training", "mean_g", bgr_list[1])).ToString();
            txtMeanValue_R.Text = (pObj.CFG_GetDouble("training", "mean_r", bgr_list[2])).ToString();

            m_total_num_for_validation = (ulong) pObj.CFG_GetUInteger64("validation", "numbers", 0);
            m_total_num_for_testing = (ulong)pObj.CFG_GetUInteger64("testing", "numbers", 0);
            m_total_num_for_training = (ulong)pObj.CFG_GetUInteger64("training", "numbers", 0);
            m_num_of_training_for_pseudo_balance = (ulong)pObj.CFG_GetUInteger64("training", "numbers_p", 0);

            txtValidationImageCount.Text = m_total_num_for_validation.ToString();
            txtTestingImageCount.Text = m_total_num_for_testing.ToString();
            txtTrainingImageCount.Text = m_total_num_for_training.ToString();

            txtTrainingEpochs.Text = (pObj.CFG_GetInteger("model_params", "training_eopchs", 100)).ToString();
            txtSnapshotInverval.Text = (pObj.CFG_GetUInteger64("model_params", "snapshot", 1000)).ToString();
            txtTrainingBatchSize.Text = (pObj.CFG_GetInteger("training", "batch_size", 6)).ToString();
            txtValidationBatchSize.Text = (pObj.CFG_GetInteger("validation", "batch_size", 4)).ToString();
            txtWeightDecay.Text = (pObj.CFG_GetDouble("model_params", "weight_decay", 0.0005)).ToString();
            txtBaseLearningRate.Text = (pObj.CFG_GetDouble("model_params", "base_lr", 0.01)).ToString();
            txtMomentum.Text = (pObj.CFG_GetDouble("model_params", "momentum", 0.9)).ToString();
            txtGamma.Text = (pObj.CFG_GetDouble("model_params", "gamma", 0.2)).ToString();

            txtTrainingEpochs.Text = (pObj.CFG_GetInteger("model_params", "training_epochs", 100)).ToString();

            /// Data augumentation
            ckb_Rotation.Checked = pObj.CFG_GetBool("model_params", "rotate", true);
            if (pObj.CFG_GetBool("model_params", "mirror", true))
            {
                if (pObj.CFG_Get("model_params", "mirror_direction", "") == "both")
                {
                    ckb_HorizontalFilp.Checked = true;
                    ckb_VerticalFlip.Checked = true;
                }
                else if (pObj.CFG_Get("model_params", "mirror_direction", "") == "horizontal")
                    ckb_HorizontalFilp.Checked = true;
                else
                    ckb_VerticalFlip.Checked = true;
            }
            else
            {
                ckb_HorizontalFilp.Checked = false;
                ckb_VerticalFlip.Checked = false;
            }
            textNewSize_training.Text = (pObj.CFG_GetInteger("model_params", "training_new_width_height", 320)).ToString();
            textNewSize_validation.Text = (pObj.CFG_GetInteger("model_params", "valid_new_width_height", 320)).ToString();
            textDeltaWidthHeight_training.Text = (pObj.CFG_GetInteger("model_params", "training_delta_width_height", 64)).ToString();
            textCropSize_training.Text = (pObj.CFG_GetInteger("model_params", "training_crop_size", 256)).ToString();
            textCropSize_validation.Text = (pObj.CFG_GetInteger("model_params", "valid_crop_size", 256)).ToString();
            int num_factor = pObj.CFG_GetInteger("model_params", "pool1_downsampling_factor", 16) / 16;
            if (num_factor >= 2)
                cmb_downsampling_factor.SelectedIndex = 1;
            else
                cmb_downsampling_factor.SelectedIndex = 0;

            //
            float pseudo_balance_factor = (float)pObj.CFG_GetDouble("model_params", "pseudo_balance_factor", 0);
            for (int k = 0; k < cmb_pseudoBalanceFactor.Items.Count; k++ )
            {
                float factor = Convert.ToSingle(cmb_pseudoBalanceFactor.Items[k].ToString());
                if (factor == pseudo_balance_factor)
                {
                    cmb_pseudoBalanceFactor.SelectedIndex = k;
                    break;
                }
            }

            int top_N_model = (int)pObj.CFG_GetInteger("model_params", "top_N_model", 1);
            cmb_top_N_model.SelectedIndex = top_N_model - 1;


            update_GPU_list();

            m_optimal_model_filename = pObj.CFG_Get("training", "optimal_model", "");

            return true;
        }

        private void dgview_labels_MouseDown(object sender, MouseEventArgs e)
        {
            // Get the index of the item the mouse is below.
            rowIndexFromMouseDown = dgview_labels.HitTest(e.X, e.Y).RowIndex;
            if (rowIndexFromMouseDown != -1)
            {
                // Remember the point where the mouse down occurred. 
                // The DragSize indicates the size that the mouse can move 
                // before a drag event should be started.                
                Size dragSize = SystemInformation.DragSize;

                // Create a rectangle using the DragSize, with the mouse position being
                // at the center of the rectangle.
                dragBoxFromMouseDown = new Rectangle(new Point(e.X - (dragSize.Width / 2),
                                                               e.Y - (dragSize.Height / 2)),
                                                               dragSize);
            }
            else
                // Reset the rectangle if the mouse is not over an item in the ListBox.
                dragBoxFromMouseDown = Rectangle.Empty;
        }

        private void dgview_labels_MouseMove(object sender, MouseEventArgs e)
        {
            if ((e.Button & MouseButtons.Left) == MouseButtons.Left)
            {
                // If the mouse moves outside the rectangle, start the drag.
                if (dragBoxFromMouseDown != Rectangle.Empty &&
                    !dragBoxFromMouseDown.Contains(e.X, e.Y))
                {

                    // Proceed with the drag and drop, passing in the list item.                    
                    DragDropEffects dropEffect = dgview_labels.DoDragDrop(
                                                            dgview_labels.Rows[rowIndexFromMouseDown],
                                                            DragDropEffects.Move);
                }
            }
        }

        private void dgview_labels_DragDrop(object sender, DragEventArgs e)
        {
            // The mouse locations are relative to the screen, so they must be 
            // converted to client coordinates.
            Point clientPoint = dgview_labels.PointToClient(new Point(e.X, e.Y));

            // Get the row index of the item the mouse is below. 
            int rowIndexOfItemUnderMouseToDrop =
                dgview_labels.HitTest(clientPoint.X, clientPoint.Y).RowIndex;

            // If the drag operation was a move then remove and insert the row.
            if (e.Effect == DragDropEffects.Move)
            {
                DataGridViewRow rowToMove = e.Data.GetData(
                    typeof(DataGridViewRow)) as DataGridViewRow;

                /// 2019_0729,Jim: Row to move is out of range.
                if (rowToMove.Index == -1 || rowIndexOfItemUnderMouseToDrop == -1)
                    return;

                dgview_labels.Rows.RemoveAt(rowIndexFromMouseDown);
                dgview_labels.Rows.Insert(rowIndexOfItemUnderMouseToDrop, rowToMove);

                dgview_labels.CurrentCell = dgview_labels.Rows[rowIndexOfItemUnderMouseToDrop].Cells[0];
            }

            setRowNumber(dgview_labels);
        }

        private void dgview_labels_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void setRowNumber(DataGridView dgv)
        {
            foreach (DataGridViewRow row in dgv.Rows)
            {
                row.HeaderCell.Value = (row.Index).ToString();
            }
        }

        private void applicationLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string target_file = Globals.GetApplicationLog(); //"log\\trainer.log";
            if (!System.IO.File.Exists(target_file))
            {
                MessageBox.Show("Log is empty now.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            process = new System.Diagnostics.Process();
            startInfo = new System.Diagnostics.ProcessStartInfo();
            //startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.CreateNoWindow = false;
            startInfo.FileName = "notepad.exe"; // C:\\WINDOWS\\system32\\notepad.exe
            startInfo.Arguments = target_file;
            process.StartInfo = startInfo;
            process.Start();
        }

        private void dNNLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string target_file = Globals.GetDNN_Log(); //"log\\kernel.log";
            if (!System.IO.File.Exists(target_file))
            {
                MessageBox.Show("Log is empty now.", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            process = new System.Diagnostics.Process();
            startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.CreateNoWindow = false;
            startInfo.FileName = "notepad.exe";
            startInfo.Arguments = target_file;
            process.StartInfo = startInfo;
            process.Start();
        }


        private void activeLearningRelabelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string published_optimal_output;

            if (!System.IO.Directory.Exists(prj_location + @"/output/resource/" + prj_published_model_name + @"/"))
            {
                MessageBox.Show("Optimal model is not ready.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
                published_optimal_output = prj_location + @"/output/resource/" + prj_published_model_name;

            /// Fetch class labels
            List<string> class_labels = new List<string>(); ;
            for (int i = 0; i < dgview_labels.RowCount; i++)
            {
                string new_item = dgview_labels.Rows[i].Cells[1].Value.ToString() + " - " +
                                    dgview_labels.Rows[i].Cells[0].Value.ToString();
                class_labels.Add(new_item);
            }

            class_trainer.frmActiveLearningRelabel frmAL_form = new class_trainer.frmActiveLearningRelabel (
                pObj, prj_name, prj_location, m_log_filename, txtImagesFolder.Text.ToString(), class_labels, published_optimal_output, ref m_gpu_list);
            frmAL_form.Owner = this;
            frmAL_form.StartPosition = FormStartPosition.CenterParent;
            DialogResult dialog_result = frmAL_form.ShowDialog();

            if (dialog_result == DialogResult.OK)
            {
                return;
            }
            frmAL_form.Dispose();
        }

    }
}

