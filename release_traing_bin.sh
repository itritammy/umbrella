#!bash
#
#

#source ./custom_id.txt
custom_id=`sed -n 's/^#define  *\([^ ]*\)  *\(.*\) *$/\2/p' custom_id.h`

if [ -z "$1" ]; then
	echo ""
	echo "No release folder name found for output !"
	echo ""
	echo "Usage:   sh ./release_traing_bin.sh <release_folder_name>"
	echo "Example: sh ./release_traing_bin.sh training_tool_v1.8"
	exit
fi


fun_CUDA_DLL()
{
	SRC=$1
	DEST=$2
	cp $SRC/cublas*.dll  $DEST/
	cp $SRC/cudart*.dll  $DEST/
	cp $SRC/cudnn*.dll    $DEST/
	cp $SRC/curand*.dll  $DEST/
}


fun_dependency_DLL()
{
	SRC=$1
	DEST=$2
	cp $SRC/gflags.dll          $DEST/	
	cp $SRC/hdf5*               $DEST/	
	cp $SRC/libgcc_s_seh-1.dll  $DEST/	
	cp $SRC/libgflags.dll       $DEST/	
	cp $SRC/libgfortran-3.dll   $DEST/	
	cp $SRC/libglog.dll         $DEST/	
	cp $SRC/libopenblas.dll     $DEST/	
	cp $SRC/libquadmath-0.dll   $DEST/	
	cp $SRC/lmdb.dll            $DEST/	
	cp $SRC/szip.dll            $DEST/	
	cp $SRC/zlib.dll            $DEST/	
	if [ ! -f ${DEST}/msvcr120.dll ]; then
		cp -a /c/Windows/System32/msvcr120.dll    ${DEST}
	fi	
	if [ ! -f ${DEST}/msvcp120.dll ]; then
		cp -a /c/Windows/System32/msvcp120.dll    ${DEST}
	fi	
}


fun_opencv_DLL()
{
	SRC=$1
	DEST=$2
	cp $SRC/opencv_*            $DEST/	
}


working_folder=./x64/Release
target_folder=./x64/$1
echo ""
echo "Released from $working_folder to $target_folder"


mkdir -p $target_folder

echo "Copy DLLs and .exe."
fun_CUDA_DLL $working_folder $target_folder
fun_dependency_DLL $working_folder $target_folder
fun_opencv_DLL $working_folder $target_folder

cp  $working_folder/trainer_CLR.dll        $target_folder/
cp  $working_folder/Training.dll           $target_folder/
cp  $working_folder/training_tool.exe      $target_folder/
echo "Clone release folder done."

echo "Copy models" 
WIN_SolutionDir=.\\
WIN_OutDir=`echo $target_folder | sed 's/\//\\\\/g'`
./common_source.git/script_property/copy_training_models.cmd "$WIN_SolutionDir" "$WIN_OutDir\\" > /dev/null 

cp -r ./doc/USB_key_driver      $target_folder
cp -r ./doc/README_training.md  $target_folder/README.md

echo ""
echo "All Done. Please check the release in $target_folder"
