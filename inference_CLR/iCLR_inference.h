#pragma once

#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <string>

#include "Status.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace CiKIT;


namespace CiKIT  {
	/*!
	\brief This is a C++/CLI Wrapper Class for ITRI c/c++ library

	To use this class in your c#, please use  the CiKIT namespace and new a CLR_inference instance as below.
	\code

	using CiKIT;
	CiKIT.CLR_inference pObj;

	\endcode
	*/
	public ref class iCLR_inference abstract 
	{
	public:

		/*!
		*
		* \brief Create the instance of CLR_inference and load Classifier.dll.
		*
		* \retval 0: Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS 	CreatInstant() = 0;


		/*!
		*
		* \brief Create the instance from the given .DLL file.
		*
		* \retval 0: Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS 	CreatInstant(System::String^ dll_name) = 0;


		/*!
		*
		* \brief Init the classification. The classification will be processed according to the given machine type.
		*
		* \param GPU_id Specify the ID of GPUs to be used in the classification. The valid values are 0, 1, 2, and 3.
		* \param resource_path The path of the 'config.ini'. Please make sure all the other essential files included the config.ini are also put in the given resource directory.
		* \param enable_cuDNN_engine Set to enable or disable the cuDNN engine on the computing. Default is true.
		* \param net_size Set the net topology flow. The value 1: Small-Net, 2: Medium-Net, 3: Large-Net. Default is 3.
		* \retval 0: Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS  Init(unsigned int GPU_id, System::String^ resource_path, bool enable_cuDNN_engine, size_t net_size) = 0;


		/*!
		*
		* \brief Get analysis time.
		*
		* \return The value of analysis time per image in ms (millisecond).
		*
		*/
		virtual float 	GetTime() = 0;


		/*!
		*
		* \brief Get total forward testing time.
		*
		* \return The value of total forward testing time in ms (millisecond).
		*
		*/
		virtual float 	GetForwardTestingTime() = 0;


		/*!
		*
		* \brief Get the labels and number of labels. This could only be called when the init() function is executed successfully.
		*
		* \param labels A List to store the labels.
		* \retval The number of labels
		*
		*/
		virtual STATUS		GetLabels(List<System::String^>^ labels) = 0;


		/*!
		*
		* \brief Set the maximum size of the cache buffer to buffering the incomming Image and processing the batching classification.
		*
		* \param size The maximum size for caching and batching process.
		* \retval =0 Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS		BatchAnalysis_SetCacheMaxSize(int size) = 0;


		/*!
		*
		* \brief Get the current number of elements in the cache buffer.

		* \retval The current number of elements in cache.
		*
		*/
		virtual int		BatchAnalysis_GetSizeOfCache() = 0;


		/*!
		*
		* \brief Clear the cache buffer.
		*
		* \return none.
		*/
		virtual void	BatchAnalysis_ClearCache() = 0;


		/*!
		*
		* \brief Check the USB Key status. This could be called after the Classifier instance is created before the init() function.
		*
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		CheckUSB_Key() = 0;


		/*!
		*
		* \brief Add a new incomming Image data into the cache buffer to wait for classification.
		*
		* \param cstrImage	The filename of image.
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		BatchAnalysis_AddToCache_withFilename(System::String^ cstrImage) = 0;


		/*!
		*
		* \brief Add a new incomming Bitmap image into the cache buffer to wait for classification.
		*
		* \param bitmap	The bitmap image.
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		BatchAnalysis_AddToCache(System::Drawing::Bitmap^ bitmap) = 0;


		/*!
		*
		* \brief Use this function to get both the classification label number and probabilities for the given images stored in the cached buffer.
		*        If the predicted probability of predicted label number is lower than the given threshold, then the label number with second high probability
		*        will be cloosen as the predicted label number.
		* \param predict_array A List of label numbers to store the predicted results for each input image.
		* \param proba_array A float List to store the predicted probabilities.
		* \param threshold_array A float List to store the threshold to filter the predicted probabilities. 
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		Classification_WithThreshold(List<int>^ predict_array, List<float>^ proba_array, List<float>^ threshold_array) = 0;


		/*!
		*
		* \brief Use this function to get both the classification label number and probabilities for the given images stored in the cached buffer.
		*        If the predicted probability of predicted label number is lower than the given threshold, then the label number with second high probability
		*        will be cloosen as the predicted label number.
		* \param predict_array A List of label numbers to store the predicted results for each input image.
		* \param proba_array A float List to store the predicted probabilities.
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		Classification(List<int>^ predict_array, List<float>^ proba_array) = 0;


		/*!
		*
		* \brief Run inference with a given image folder and get confusion matrix back. All the images inside the folder will be analized recursively.
		* \param mapping_filename A image file list for perdiction process.
		* \param batch_size The maximum size for caching and batching process.
		* \param threshold_array A threshold List for inference.
		* \param result_file_array A image filename List of the processed images.
		* \param result_GT_array The ground truth List of the processed images.
		* \param result_predict_array A result List after processing image prediction.
		* \param result_confusion_matrix A confusion matrix after image prediction. The 2 dimenstion results are stored in a sequential array.
		* \param out_filename_of_confusion The output filename of the confusion matrix.
		* \param path_of_uncertainty_matrix A output path of uncertainty matrix.
		* \param confusion_prefix_name The prefix name of the confusion matrix.
		* \retval =0 Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS	Classification_WithMappingFile(System::String^ mapping_filename, int batch_size, List<float>^ threshold_array,
			List<System::String^>^ result_file_array, List<int>^ result_GT_array, List<int>^ result_predict_array, List<int>^ result_confusion_matrix,
			System::String^ out_filename_of_confusion, System::String^ path_of_uncertainty_matrix, System::String^ confusion_prefix_name) = 0;


		/*!
		*
		* \brief Destroy this instance.
		*
		* \return none.
		*
		*/
		virtual void 	Destroy() = 0;
	};

}
