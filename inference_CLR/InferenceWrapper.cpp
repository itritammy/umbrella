// This is the main DLL file.

#include <windows.h>
#include <direct.h>

#include "InferenceWrapper.h"
#include <fstream>  // NOLINT(readability/streams)
#include <iostream>  // NOLINT(readability/streams)

#include <msclr/marshal.h>        // .NET string to C-style string
#include <msclr/marshal_cppstd.h> // .NET string to STL string
#include <cliext/vector>  
#include <assert.h>

#include "string_utils.h"

using namespace msclr::interop;

using namespace CiKIT;
using namespace std;


#define R_CAST  reinterpret_cast
#define C_CAST  const_cast
#define S_CAST  static_cast

namespace CiKIT  {


std::string NetStr2STLStr(System::String^ str)
{
	marshal_context^ context = gcnew marshal_context();

	std::string stl_s = context->marshal_as<std::string>(str);

	return stl_s;
}


cv::Mat Bitmap2Mat(System::Drawing::Bitmap^ bitmap)
{
	IplImage* tmp;
	System::Drawing::Rectangle rect = System::Drawing::Rectangle(0, 0, bitmap->Size.Width, bitmap->Size.Height);

	System::Drawing::Imaging::BitmapData^ bmData = bitmap->LockBits(rect, System::Drawing::Imaging::ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format24bppRgb);

	tmp = cvCreateImage(cvSize(bitmap->Width, bitmap->Height), IPL_DEPTH_8U, 3);
	tmp->imageData = (char*)bmData->Scan0.ToPointer();

	bitmap->UnlockBits(bmData);

	return cv::Mat(tmp);
}


CLR_inference::CLR_inference()
  : m_predict_array(NULL), m_proba_buffer(NULL), 
	m_num_of_labels(0),
	m_max_of_cache_num(0),
	m_max_count_of_proba(0),
	m_threshold_buffer(NULL),
	m_threshold_count(0)
{
}


CLR_inference::~CLR_inference()
{
	if (m_threshold_buffer)
		free(m_threshold_buffer);

	m_label_codes.Clear();

	Destroy();
}

STATUS CLR_inference::CreatInstant()
{
	try {
		// Load the DLL.
		HINSTANCE hDLLClassifier = ::LoadLibrary(TEXT(".\\Classifier.dll"));
		/// #ifndef _DEBUG
		/// HINSTANCE hDLLClassifier = ::LoadLibrary(TEXT("./classifier_molicel.dll"));



		if (!hDLLClassifier)
		{
			std::cerr << "Load DLL module failed!\n";
			return CiKIT::STATUS::MSG_LOAD_DLL_FAILED; 
		}

		// Get the factory function to create an instance.
		IDeepLearningFactory FactoryFuncClassifier = R_CAST<IDeepLearningFactory>(::GetProcAddress(hDLLClassifier, "CreateDeepLearning"));
		if (!FactoryFuncClassifier)
		{
			::FreeLibrary(hDLLClassifier);
			std::cerr << "Load the factory function failed!\n";
			return CiKIT::STATUS::MSG_LOAD_FACTORY_FUNCTION_FAILED;
		}

		// Create a classifier.
		m_pObj = FactoryFuncClassifier();
		if (m_pObj == nullptr)
			return CiKIT::STATUS::MSG_CREATE_FACTORY_INSTANCE_FAILED;
	}
	catch (const std::exception & e)
	{
		// Something else 'bad' happened
		return CiKIT::STATUS::MSG_LOAD_DLL_FAILED;
	}

	return CiKIT::STATUS::MSG_NO_ERROR;
}


STATUS CLR_inference::CreatInstant(System::String^ dll_name)
{
	std::string stl_dll_name = NetStr2STLStr(dll_name);
	std::wstring wstl_dll_name;
	to_wstring(wstl_dll_name, stl_dll_name);
	try {
		HINSTANCE hDLLClassifier = ::LoadLibrary(wstl_dll_name.c_str());

		if (!hDLLClassifier)
		{
			std::cerr << "Load DLL module failed!\n";
			return CiKIT::STATUS::MSG_LOAD_DLL_FAILED;
		}

		// Get the factory function to create an instance.
		IDeepLearningFactory FactoryFuncClassifier = R_CAST<IDeepLearningFactory>(::GetProcAddress(hDLLClassifier, "CreateDeepLearning"));
		if (!FactoryFuncClassifier)
		{
			::FreeLibrary(hDLLClassifier);
			std::cerr << "Load the factory function failed!\n";
			return CiKIT::STATUS::MSG_LOAD_FACTORY_FUNCTION_FAILED;
		}

		// Create a classifier.
		m_pObj = FactoryFuncClassifier();
		if (m_pObj == nullptr)
			return CiKIT::STATUS::MSG_CREATE_FACTORY_INSTANCE_FAILED;
	}
	catch (const std::exception & e)
	{
		// Something else 'bad' happened
		return CiKIT::STATUS::MSG_NO_ERROR;
	}

	return CiKIT::STATUS::MSG_NO_ERROR;
}


int CLR_inference::FindLabels()
{
	char buffer[512];
	char token = ',';
	int m_num_of_labels = m_pObj->GetLabels(buffer, sizeof(buffer), token);

	if (m_num_of_labels <= 0)
		return 0;

	char *ptr_h = buffer;
	char *ptr_e = buffer;
	for (int i = 0; i < m_num_of_labels; i++)
	{
		ptr_h = ptr_e;
		ptr_e = strchr(ptr_e, token);
		if (ptr_e == NULL)
			break;

		*ptr_e = '\0';
		System::String^ code_value = gcnew System::String(ptr_h);

		m_label_codes.Add(code_value);
		ptr_e++;
	}

	return m_num_of_labels;
}

#ifdef SYNPOWER_ONLY
STATUS CLR_inference::Init(unsigned int GPU_id, System::String^ resource_path, bool is_net_encrypted, bool enable_cuDNN_engine, size_t net_size)
{
	std::string stl_s = NetStr2STLStr(resource_path);
	STATUS ret = CiKIT::STATUS::MSG_NO_ERROR;
	try{
		ret = (STATUS)m_pObj->Init((char*)stl_s.c_str(), GPU_id, is_net_encrypted, enable_cuDNN_engine, net_size);
		m_predict_array = NULL;
		m_proba_buffer = NULL;
		FindLabels();
	}
	catch (const std::exception & e)
	{
		// Something else 'bad' happened
		std::cout << "internal exception happened";
	}
	return ret;
}
#endif


STATUS CLR_inference::Init(unsigned int GPU_id, System::String^ resource_path, bool enable_cuDNN_engine, size_t net_size)
{
	std::string stl_s = NetStr2STLStr(resource_path);
	STATUS ret = CiKIT::STATUS::MSG_NO_ERROR;
	try{
		ret = (STATUS)m_pObj->Init((char*)stl_s.c_str(), GPU_id, enable_cuDNN_engine, net_size);
		m_predict_array = NULL;
		m_proba_buffer = NULL;
		FindLabels();
	}
	catch (const std::exception & e)
	{
		// Something else 'bad' happened
		std::cout << "internal exception happened";
	}
	return ret;
}


float CLR_inference::GetTime()
{
	return m_pObj->GetTime();
}


float CLR_inference::GetForwardTestingTime()
{
	return m_pObj->GetForwardTestingTime();
}


STATUS	CLR_inference::BatchAnalysis_SetCacheMaxSize(int size)
{
	STATUS ret = (STATUS)m_pObj->BatchAnalysis_SetCacheMaxSize(size);
	if (ret == STATUS::MSG_NO_ERROR)
	{
		if (m_predict_array)
			free(m_predict_array);

		if (m_proba_buffer)
			free(m_proba_buffer);

		// alloc buffer for predict
		m_num_of_labels = m_pObj->GetClassTotalNum();

		m_max_of_cache_num = size;
		m_max_count_of_proba = size * m_num_of_labels;
		m_predict_array = (int*)malloc(sizeof(int) * m_max_of_cache_num);
		m_proba_buffer = (float*)malloc(sizeof(float) * m_max_count_of_proba);
	}

	return ret;
}


STATUS CLR_inference::BatchAnalysis_AddToCache_withFilename(System::String^ cstrImage)
{
	std::string stl_s = NetStr2STLStr(cstrImage);

	return (STATUS)add_image_to_cache((char*)stl_s.c_str());
}


STATUS 	CLR_inference::BatchAnalysis_AddToCache(System::Drawing::Bitmap^ bitmap)
{
	cv::Mat src_img = Bitmap2Mat(bitmap);
	return (STATUS)m_pObj->BatchAnalysis_AddToCache(src_img.rows,
		src_img.cols,
		src_img.type(),
		src_img.data,
		src_img.step);
}


/* obsolete */
#if 0
int CLR_inference::BatchAnalysis_Run(List<int>^ results)
{
	std::vector<int> vect;
	int ret = m_pObj->BatchAnalysis_Run(vect);

	std::cout << "vect.size()= " << vect.size() << "\n";

	if (vect.size() == 0)
		return -1;

	results->Clear();
	for (unsigned int i = 0; i < vect.size(); i++)
	{
		//std::cout << "got result: " << vect[i] << "\n";
		results->Add(vect[i]);
	}
	return ret;
}
#endif


STATUS	CLR_inference::Classification_WithThreshold(List<int>^ pred_array, List<float>^ proba_array, List<float>^ threshold_array)
{
	int num_of_cached_images = m_pObj->BatchAnalysis_GetSizeOfCache();

	if (num_of_cached_images < 0)
		return (STATUS)num_of_cached_images; /// It's error code

	if (num_of_cached_images == 0)
		return (STATUS)MSG_IMAGE_CACHE_EMPTY;

	if (m_threshold_count != threshold_array->Count)
	{
		// reallocate the threshold buffer
		if (m_threshold_buffer) free(m_threshold_buffer);
		m_threshold_buffer = (float*) malloc ( sizeof(float) * threshold_array->Count );
		if (m_threshold_buffer == NULL)
			return STATUS::MSG_OUT_OF_FREE_MEMORY;

		m_threshold_count = threshold_array->Count;
	}

	for (int i = 0; i < threshold_array->Count; i++)
	{
		m_threshold_buffer[i] = threshold_array[i];
	}

	int ret = m_pObj->BatchAnalysis_Run_WithThreshold(m_predict_array, m_max_of_cache_num, m_proba_buffer, m_max_count_of_proba, m_threshold_buffer, threshold_array->Count);

	if (ret < 0)
		return (STATUS)ret;

	pred_array->Clear();
	for (int i = 0; i < num_of_cached_images; i++)
	{
		//int code = m_label_codes[m_predict_array[i]];
		int num = m_predict_array[i]; // Only return the lable number
		pred_array->Add(num);
	}

	proba_array->Clear();
	for (int i = 0; i < num_of_cached_images * m_num_of_labels; i++)
		proba_array->Add(m_proba_buffer[i]);

	return  STATUS::MSG_NO_ERROR;
}


STATUS	CLR_inference::Classification(List<int>^ predict_array, List<float>^ proba_array)
{
	List<float> ^threshold_array = gcnew List<float>;
	threshold_array->Clear();

	for (int i = 0; i < m_num_of_labels; i++)
		threshold_array->Add(0);

	return Classification_WithThreshold(predict_array, proba_array, threshold_array);
}


int	CLR_inference::BatchAnalysis_GetSizeOfCache()
{
	if (m_pObj)
		return m_pObj->BatchAnalysis_GetSizeOfCache();

	return MSG_CLASSIFIER_NOT_INITED;
}


void CLR_inference::BatchAnalysis_ClearCache()
{
	if (m_pObj)
		m_pObj->BatchAnalysis_ClearCache();
}


STATUS	CLR_inference::CheckUSB_Key()
{
	if (m_pObj)
		return (STATUS)m_pObj->CheckUSB_Key();

	return STATUS::MSG_CLASSIFIER_NOT_INITED;
}


STATUS CLR_inference::GetLabels(List<System::String^>^ labels)
{
	if (m_pObj == NULL)
		return STATUS::MSG_CLASSIFIER_NOT_INITED;

	labels->Clear();
	for (int i = 0; i < m_label_codes.Count; i++)
	{
		labels->Add(m_label_codes[i]);
	}

	return STATUS::MSG_NO_ERROR;
}


void CLR_inference::Destroy()
{
	if (m_predict_array)
		free(m_predict_array);

	if (m_proba_buffer)
		free(m_proba_buffer);

	if (m_pObj) {
		m_pObj->Destroy();
		m_pObj = NULL;
	}
}



/// ===========================================================
int Get_AllFiles_in_UnixLike(FILE *fp_out, std::string target_path)
{
	//char *tmpPath = (char *)args;
	int			file_counts = 0;
	std::string sPath = target_path;

	WIN32_FIND_DATAA FindFileData;
	std::string sTmpPath = sPath;
	sTmpPath += "/*.*";

	std::string currFile = "";

	HANDLE hFind = FindFirstFileA(sTmpPath.c_str(), &FindFileData);
	if (hFind == INVALID_HANDLE_VALUE) {
		std::cout << "Not a valid path\n";
		return 0;
	}
	else
	{
		do
		{
			// skip "." ".." "$RECYCLE.BIN" "$Recycle.Bin" ...
			if (FindFileData.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)
				continue;

			//check if its a directory...
			if ((FindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				std::string filePath = FindFileData.cFileName;
				//ignore '.' and '..'
				if (filePath.compare(".") == 0 || filePath.compare("..") == 0
					|| filePath.compare("$RECYCLE.BIN") == 0 || filePath.compare("$Recycle.Bin") == 0)
					continue;
				else
				{
					sTmpPath = sPath + "/" + filePath;
					file_counts += Get_AllFiles_in_UnixLike(fp_out, sTmpPath);
				}
			}
			else   //its a file...
			{
				std::string filePath = FindFileData.cFileName;

				sTmpPath = sPath + "/";
				//currFile = sTmpPath + std::wstring(FindFileData.cFileName) + L"\t\t " + std::to_wstring(label_id) + L"\n";
				currFile = sTmpPath + std::string(FindFileData.cFileName) + "\n";
				if (fp_out) {
					/// Got a filename
					//fwrite(currFile.c_str(), sizeof(WCHAR), currFile.length(), fp_out);
					fwrite(currFile.c_str(), sizeof(char), currFile.length(), fp_out);
					file_counts++;
					//std::fputws(currFile.c_str(), fp_out);
					//std::wcout << currFile << L"\n";
				}
			}
		} while (FindNextFileA(hFind, &FindFileData) != 0);

		FindClose(hFind);
	}

	return file_counts;
}


STATUS CLR_inference::add_image_to_cache(char* image_filename)
{
	cv::Mat src_img = cv::imread(image_filename, -1);
	if (src_img.empty())
		return STATUS::MSG_OPEN_INPUT_FILE_FAILED;

	return (STATUS)m_pObj->BatchAnalysis_AddToCache(src_img.rows,
		src_img.cols,
		src_img.type(),
		src_img.data,
		src_img.step);
}


typedef struct {
	float *result_ptr;
	size_t width;
	size_t height;
	size_t count;
	MSG_t status;
} stSEG_RESULT;

void segmentation_callback(void* user_data, float* result_ptr, size_t result_width, size_t result_height, size_t count, MSG_t status)
{
	printf("result_width(%ld), size_t result_height(%ld), size_t count(%ld), MSG_t status(%d)\n", result_width, result_height, count, status);
	stSEG_RESULT *seg_result = (stSEG_RESULT*)user_data;
	seg_result->result_ptr = result_ptr;
	seg_result->width = result_width;
	seg_result->height = result_height;
	seg_result->count = count;
	seg_result->status = status;
}


STATUS CLR_inference::Classification_WithMappingFile(System::String^ mapping_filename, int batch_size, List<float>^ threshold_array,
	List<System::String^>^ result_file_array, List<int>^ result_GT_array, List<int>^ result_predict_array, List<int>^ result_confusion_matrix,
	System::String^ confusion_output_filename, System::String^ path_of_uncertainty_matrix, System::String^ confusion_prefix_name)
{
	STATUS		  ret = STATUS::MSG_NO_ERROR;
	std::string   src_filename = NetStr2STLStr(mapping_filename);
	std::string   out_filename = NetStr2STLStr(confusion_output_filename);
	std::string   uncertainty_matrix_dir = NetStr2STLStr(path_of_uncertainty_matrix);
	std::string   conf_prefix = NetStr2STLStr(confusion_prefix_name);
	std::string   line;
	std::string	  fileName = "";
	int			  label;
	std::tuple<std::string, int, std::string>				img_tuple;
	std::vector<std::tuple<std::string, int, std::string> > mapping_list;


	int										*conf_mat_img_cnt = NULL;
	std::vector<std::vector<std::string>*>	conf_mat_img_list;
	conf_mat_img_cnt = (int*)malloc(sizeof(int) *m_num_of_labels*m_num_of_labels);
	if (conf_mat_img_cnt == NULL)
		return STATUS::MSG_OUT_OF_FREE_MEMORY; /// out of free memory
	memset(conf_mat_img_cnt, 0, sizeof(int) *m_num_of_labels*m_num_of_labels);
	for (int i = 0; i < m_num_of_labels * m_num_of_labels; i++)
		conf_mat_img_list.push_back(new std::vector<std::string>());

	/// Init the threshold if it is empty
	if (threshold_array->Count == 0)
	{
		for (int t = 0; t < m_num_of_labels; t++)
			threshold_array->Add(0);
	}


	std::ifstream infile(src_filename.c_str());

	if (infile.is_open() == false)
		return STATUS::MSG_OPEN_INPUT_FILE_FAILED;

	/// 1. Prepare the threshold
	if (m_threshold_count != threshold_array->Count)
	{
		// reallocate the threshold buffer
		if (m_threshold_buffer) free(m_threshold_buffer);
		m_threshold_buffer = (float*)malloc(sizeof(float) * threshold_array->Count);
		m_threshold_count = threshold_array->Count;
	}

	for (int i = 0; i < threshold_array->Count; i++)
		m_threshold_buffer[i] = threshold_array[i];


	// 2. Traversal the image source file
	result_file_array->Clear();
	std::vector<std::string> file_list;
	std::vector<int> gts;	// The ground truth of given picture files
	//std::string filename = "";
	while (infile.eof() == false)
	{
		file_list.clear();
		gts.clear();
		for (int i = 0; i < batch_size && infile.eof() == false; i++)
		{
			if (std::getline(infile, line))
			{
				if (seperate_fileanme_and_label(line, fileName, &label) < 0)
					continue;

				// 3. Add to cache
				ret = add_image_to_cache((char*)fileName.c_str());
				if (ret != STATUS::MSG_NO_ERROR)
				{
					//fclose(fp);
					infile.close();
					return ret;
				}
				else
				{
					file_list.push_back(fileName); // to keep the filename
					gts.push_back(label);  // to keep the ground truth
				}
			}
		}

		// 4. Run analysis
		int num_of_cached_images = m_pObj->BatchAnalysis_GetSizeOfCache();
		if (num_of_cached_images == 0)
			continue;

		std::vector<int> pred_array;

		// int *predict_array, int max_count_of_predict, float* proba_buffer, int max_count_of_proba
		ret = (STATUS)m_pObj->BatchAnalysis_Run_WithThreshold(m_predict_array, m_max_of_cache_num, m_proba_buffer, m_max_count_of_proba, m_threshold_buffer, threshold_array->Count);
		if (ret != STATUS::MSG_NO_ERROR) {
			if (infile.is_open() == true)
				infile.close();
			return ret;
		}

		// 4.1 Generate the results
		try {
			for (int i = 0; i < num_of_cached_images; i++)
			{
				// prediction result
				pred_array.push_back(m_predict_array[i]);
				result_predict_array->Add(m_predict_array[i]);

				// generate confusion matrix
				int idx = m_num_of_labels * gts.at(i) + m_predict_array[i];
				conf_mat_img_cnt[idx]++;

				// Add miss-classificated image into confusion matrix
				if (gts.at(i) != m_predict_array[i])
					conf_mat_img_list.at(idx)->push_back(file_list.at(i));
			}
		}
		catch (std::exception ex) {
			size_t ll = file_list.size();
			size_t cc = conf_mat_img_list.size();
			size_t tt = gts.size();
			int i = 15;
			int mm = m_predict_array[i];
			int gg = gts.at(i);
			int idx = m_num_of_labels * gts.at(i) + m_predict_array[i];
			std::string ff = file_list.at(i);
			int pp = m_predict_array[i];
			if (infile.is_open() == true)
				infile.close();
			return STATUS::MSG_ACCESS_OUT_OF_BOUNDS;
		}

		// 4.2. Generate filename and ground truch output
		for (int i = 0; i < file_list.size(); i++)
		{
			System::String^ str2 = gcnew System::String(file_list[i].c_str());
			result_file_array->Add(str2);
			result_GT_array->Add(gts[i]);
		}
	}

	if (infile.is_open() == true)
		infile.close();

	if (result_predict_array->Count == 0)
		return STATUS::MSG_OPEN_INPUT_FILE_FAILED;



	// 6. Save confusion matrix to file
	std::ofstream outfile;

	outfile.open(out_filename.c_str(), std::ios::out);

	if (outfile.is_open() == true)
	{
		for (int i = 0; i < m_num_of_labels; i++)
		{
			for (int k = 0; k < m_num_of_labels; k++)
			{
				int idx = i * m_num_of_labels + k;
				outfile << conf_mat_img_cnt[idx] << "\t";
			}
			outfile << "\n";
		}
		outfile.close();
		// ToDo: Set into config file
		// string confMatrix_filename = pObj.CFG_Get("validation", "confusion_matrix", "");
	}


	// 7. Save miss-classificated image into confusion matrix
	//    filename format: cm_0_1.txt, cm_0_2.txt
	for (int i = 0; i < m_num_of_labels; i++)
	{
		for (int k = 0; k < m_num_of_labels; k++)
		{
			if (i == k)
				continue;
			std::string cm_filename = uncertainty_matrix_dir + "/" + conf_prefix + "_cm_" + std::to_string(i) + "_" + std::to_string(k) + ".txt";
			std::ofstream outfile;
			outfile.open(cm_filename.c_str(), std::ios::out);
			int idx = i * m_num_of_labels + k;
			for (int m = 0; m < conf_mat_img_list[idx]->size(); m++) {
				std::string filename = conf_mat_img_list[idx]->at(m);
				outfile << filename << "\n";
			}
			outfile.close();
		}
	}

	if (outfile.is_open() == true)
		outfile.close();

	// free confusion matrix image list vector
	for (int i = 0; i < m_num_of_labels * m_num_of_labels; i++)
		delete conf_mat_img_list.at(i);

	if (conf_mat_img_cnt)
		free(conf_mat_img_cnt);
	return STATUS::MSG_NO_ERROR;
}


#ifndef SYNPOWER_ONLY
#if 0
STATUS CLR_inference::Segmentation(List<float>^ confidential_score_array)
{
	STATUS ret = STATUS::MSG_NO_ERROR;

	stSEG_RESULT seg_result;
		
	ret = (STATUS)m_pObj->BatchAnalysis_RunSegmentation(segmentation_callback, &seg_result);

	if (ret != STATUS::MSG_NO_ERROR)
		return ret;

	ret = (STATUS)seg_result.status;
	if (ret != STATUS::MSG_NO_ERROR)
		return ret;

	float *ptr = seg_result.result_ptr;
	size_t max_cnt = seg_result.width * seg_result.height * seg_result.count;
	for (size_t i = 0; i < max_cnt; i++)
	{
		confidential_score_array->Add(ptr[i]);
	}

	return ret;
}
#endif
#endif

void move_file(char *old_full_name, char* path)
{
	const char *name = NULL;
	char new_full_name[256];
	int ret = 0;

	memset(new_full_name, 0, sizeof(new_full_name));

	name = strrchr(old_full_name, '/');
	ret = mkdir(path);
	_snprintf(new_full_name, sizeof(new_full_name)-1, "%s%s", path, name);

	// correct the slash
	for (int i = 0; i < strlen(old_full_name); i++)
		if (old_full_name[i] == '\\')
			old_full_name[i] = '/';

	for (int i = 0; i < strlen(new_full_name); i++)
		if (new_full_name[i] == '\\')
			new_full_name[i] = '/';

	// 
	ret = rename(old_full_name, new_full_name);
	if (ret != 0) // rename again, if the destimation filename is duplicated.
	{
		char buf[256]; // for duplicated file name
		memset(buf, 0, sizeof(buf));

		buf[0] = old_full_name[0]; // keep the driver name
		_snprintf(buf+1, sizeof(buf)-1, "%s", old_full_name+2);
		for (int k = 0; k < strlen(buf); k++)
			if (buf[k] == '/')
				buf[k] = '_';

		_snprintf(new_full_name, sizeof(new_full_name), "%s/%s", path, buf);

		for (int i = 0; i < strlen(new_full_name); i++)
			if (new_full_name[i] == '\\')
				new_full_name[i] = '/';

		// rename again
		ret = rename(old_full_name, new_full_name);
	}
}


STATUS CLR_inference::DispatchImages(System::String^ s_dest_path, System::String^ s_src_path, System::String^ s_temp_list_filename, int batch_size, List<float>^ threshold_array)
{
	std::string dest_path = NetStr2STLStr(s_dest_path);
	std::string src_path = NetStr2STLStr(s_src_path);
	std::string temp_list_filename = NetStr2STLStr(s_temp_list_filename);
	
	STATUS ret = STATUS::MSG_NO_ERROR;
	errno_t err;
	FILE *fp_list = NULL;
	//wchar_t *images_list_w = L"images_list.tmp";
	const char *images_list = temp_list_filename.c_str(); //"images_list.tmp";

	//err = _wfopen_s(&fp_list, images_list, "w+, ccs=UTF-8");
	err = fopen_s(&fp_list, images_list, "w");
	if (fp_list == NULL)
		return STATUS::MSG_OPEN_OUTPUT_FILE_FAILED;

	Get_AllFiles_in_UnixLike(fp_list, src_path);
	fclose(fp_list);
	//


	// 2. Traversal the image source file

	char line[512];
	FILE *fp = NULL;
	//wfilelist = to_wstring(wfilelist, file_src);
	err = fopen_s(&fp, images_list, "r");
	if (fp == NULL)
		return STATUS::MSG_OPEN_INPUT_FILE_FAILED;

	// Prepare the threshold
	if (threshold_array->Count > 0)
	{
		if (m_threshold_count != threshold_array->Count)
		{
			// reallocate the threshold buffer
			if (m_threshold_buffer) free(m_threshold_buffer);
			m_threshold_buffer = (float*)malloc(sizeof(float) * threshold_array->Count);
			m_threshold_count = threshold_array->Count;
		}

		for (int i = 0; i < threshold_array->Count; i++)
		{
			m_threshold_buffer[i] = threshold_array[i];
		}
	}


	std::vector<std::string> file_list;
	//std::string filename = "";
	while (feof(fp) == false)
	{
		file_list.clear();
		for (int i = 0; i < batch_size && feof(fp) == false; i++)
		{
			if (fgets(line, sizeof(line), fp) != NULL)
			{
				//filename = to_string(filename, line);
				char *target_file = line; // (char*)filename.c_str();
				remove_line_feed_and_carriage_return(target_file);

				// 3. Add to cache
				ret = add_image_to_cache(target_file);
				if (ret != STATUS::MSG_NO_ERROR)
				{
					if (fp) {
						fclose(fp); fp = NULL;
					}
					return ret;
				}
				else
				{
					remove_line_feed_and_carriage_return(line);
					file_list.push_back(line);
				}
			}
		}

		// 4. Run analysis
		std::vector<int> pred_array;
		std::vector<float> proba_array;
		int num_of_cached_images = m_pObj->BatchAnalysis_GetSizeOfCache();

		// int *predict_array, int max_count_of_predict, float* proba_buffer, int max_count_of_proba
		ret = STATUS::MSG_NO_ERROR;
		if (threshold_array->Count > 0)
			ret = (STATUS)m_pObj->BatchAnalysis_Run_WithThreshold(m_predict_array, m_max_of_cache_num, m_proba_buffer, m_max_count_of_proba, m_threshold_buffer, threshold_array->Count);
		else
			ret = (STATUS)m_pObj->BatchAnalysis_Run(m_predict_array, m_max_of_cache_num);

		for (int i = 0; i < num_of_cached_images; i++)
			pred_array.push_back(m_predict_array[i]);

		//for (int i = 0; i < num_of_cached_images * m_num_of_labels; i++)
		//	proba_array.push_back(proba_array[i]);

		if (ret != STATUS::MSG_NO_ERROR)
		{
			if (fp) {
				fclose(fp); fp = NULL;
			}
			return ret;
		}

		// 5. Generate output
		for (int i = 0; i < file_list.size(); i++)
		{
			char target_path[256];
			_snprintf(target_path, sizeof(target_path), "%s/class%02d_%s", dest_path.c_str(), pred_array.at(i), m_label_codes[pred_array.at(i)]);
			move_file((char*)file_list[i].c_str(), target_path);
		}

	}

	fclose(fp);

	return STATUS::MSG_NO_ERROR;
}


STATUS CLR_inference::InferenceSpecifiedFolderWithThreshold(System::String^ s_src_path, int batch_size, List<float>^ threshold_array,
	List<System::String^>^ result_file_array, List<int>^ result_predict_array, System::String^ s_temp_list_filename)
{
	std::string src_path = NetStr2STLStr(s_src_path);
	std::string temp_list_filename = NetStr2STLStr(s_temp_list_filename);

	STATUS ret = STATUS::MSG_NO_ERROR;
	errno_t err;
	FILE* fp_list = NULL;
	//wchar_t *images_list_w = L"images_list.tmp";
	//char* images_list = "images_list.tmp";
	const char *images_list = temp_list_filename.c_str(); //"images_list.tmp";

	//err = _wfopen_s(&fp_list, images_list, "w+, ccs=UTF-8");
	err = fopen_s(&fp_list, images_list, "w");
	if (fp_list == NULL)
		return STATUS::MSG_OPEN_OUTPUT_FILE_FAILED;

	Get_AllFiles_in_UnixLike(fp_list, src_path);
	fclose(fp_list);
	//


	// 2. Traversal the image source file

	char line[512];
	FILE* fp = NULL;
	//wfilelist = to_wstring(wfilelist, file_src);
	err = fopen_s(&fp, images_list, "r");
	if (fp == NULL)
		return STATUS::MSG_OPEN_INPUT_FILE_FAILED;

	// Prepare the threshold
	if (m_threshold_count != threshold_array->Count)
	{
		// reallocate the threshold buffer
		if (m_threshold_buffer) free(m_threshold_buffer);
		m_threshold_buffer = (float*)malloc(sizeof(float) * threshold_array->Count);
		m_threshold_count = threshold_array->Count;
	}

	for (int i = 0; i < threshold_array->Count; i++)
	{
		m_threshold_buffer[i] = threshold_array[i];
	}

	result_file_array->Clear();
	std::vector<std::string> file_list;
	//std::string filename = "";
	while (feof(fp) == false)
	{
		file_list.clear();
		for (int i = 0; i < batch_size && feof(fp) == false; i++)
		{
			if (fgets(line, sizeof(line), fp) != NULL)
			{
				//filename = to_string(filename, line);
				char* target_file = line; // (char*)filename.c_str();
				remove_line_feed_and_carriage_return(target_file);

				// 3. Add to cache
				ret = add_image_to_cache(target_file);
				if (ret != STATUS::MSG_NO_ERROR)
				{
					if (fp) {
						fclose(fp); fp = NULL;
					}
					return ret;
				}
				else
				{
					remove_line_feed_and_carriage_return(line);
					file_list.push_back(line);
				}
			}
		}

		// 4. Run analysis
		std::vector<int> pred_array;
		std::vector<float> proba_array;
		int num_of_cached_images = m_pObj->BatchAnalysis_GetSizeOfCache();

		// int *predict_array, int max_count_of_predict, float* proba_buffer, int max_count_of_proba
		ret = (STATUS)m_pObj->BatchAnalysis_Run_WithThreshold(m_predict_array, m_max_of_cache_num, m_proba_buffer, m_max_count_of_proba, m_threshold_buffer, threshold_array->Count);

		for (int i = 0; i < num_of_cached_images; i++)
		{
			pred_array.push_back(m_predict_array[i]);
			result_predict_array->Add(m_predict_array[i]);
		}

		//for (int i = 0; i < num_of_cached_images * m_num_of_labels; i++)
		//	proba_array.push_back(proba_array[i]);

		if (ret != STATUS::MSG_NO_ERROR)
		{
			if (fclose) {
				fclose(fp); fp = NULL;
			}
			return ret;
		}

		// 5. Generate output
		for (int i = 0; i < file_list.size(); i++)
		{
			System::String^ str2 = gcnew System::String(file_list[i].c_str());
			result_file_array->Add(str2);

			//char target_path[256];
			//_snprintf(target_path, sizeof(target_path), "%s/class%02d_%02d", dest_path.c_str(), pred_array.at(i), m_label_codes[pred_array.at(i)]);
			//move_file(file_list[i].c_str(), target_path);
		}

	}

	fclose(fp);

	return STATUS::MSG_NO_ERROR;
}



STATUS CLR_inference::InferenceSpecifiedFolder(System::String^ s_src_path, int batch_size, List<System::String^>^ result_file_array, List<int>^ result_predict_array, System::String^ s_temp_list_filename)
{
	std::string src_path = NetStr2STLStr(s_src_path);
	std::string temp_list_filename = NetStr2STLStr(s_temp_list_filename);

	STATUS ret = STATUS::MSG_NO_ERROR;
	//errno_t err;
	FILE* fp_list = NULL;
	//wchar_t *images_list_w = L"images_list.tmp";
	//char* images_list = "images_list.tmp";
	const char *images_list = temp_list_filename.c_str(); //"images_list.tmp";

	//err = _wfopen_s(&fp_list, images_list, "w+, ccs=UTF-8");
	fp_list = fopen(images_list, "w");
	if (fp_list == NULL)
		return STATUS::MSG_OPEN_OUTPUT_FILE_FAILED;

	Get_AllFiles_in_UnixLike(fp_list, src_path);
	fclose(fp_list);
	//

	// 2. Traversal the image source file
	char line[512];
	FILE* fp = NULL;
	//wfilelist = to_wstring(wfilelist, file_src);
	fp = fopen(images_list, "r");
	if (fp == NULL)
		return STATUS::MSG_OPEN_INPUT_FILE_FAILED;

	int num_of_cached_images;
	result_file_array->Clear();
	result_predict_array->Clear();
	std::vector<std::string> file_list;
	//std::string filename = "";
	while (feof(fp) == false)
	{
		file_list.clear();
		for (int i = 0; i < batch_size && feof(fp) == false; i++)
		{
			if (fgets(line, sizeof(line), fp) != NULL)
			{
				//filename = to_string(filename, line);
				char* target_file = line; // (char*)filename.c_str();
				remove_line_feed_and_carriage_return(target_file);

				// 3. Add to cache
				ret = add_image_to_cache(target_file);
				if (ret != STATUS::MSG_NO_ERROR)
					goto error_exit;
				else
				{
					remove_line_feed_and_carriage_return(line);
					file_list.push_back(line);
				}
			}
		}

		// 4. Run analysis
		//std::vector<int> pred_array;
		num_of_cached_images = m_pObj->BatchAnalysis_GetSizeOfCache();
		if (num_of_cached_images == 0)
			continue;
		else if (num_of_cached_images < 0)	
			goto error_exit;

		ret = (STATUS)m_pObj->BatchAnalysis_Run(m_predict_array, m_max_of_cache_num);
		if (ret != STATUS::MSG_NO_ERROR)
			goto error_exit;

		for (int i = 0; i < num_of_cached_images; i++)
		{
			//pred_array.push_back(m_predict_array[i]);
			result_predict_array->Add(m_predict_array[i]);
		}

		if (ret != STATUS::MSG_NO_ERROR)
			goto error_exit;

		// 5. Generate output
		for (int i = 0; i < file_list.size(); i++)
		{
			System::String^ str2 = gcnew System::String(file_list[i].c_str());
			result_file_array->Add(str2);

			//char target_path[256];
			//_snprintf(target_path, sizeof(target_path), "%s/class%02d_%02d", dest_path.c_str(), pred_array.at(i), m_label_codes[pred_array.at(i)]);
			//move_file(file_list[i].c_str(), target_path);
		}

	}
	
error_exit:

	if (fp)
		fclose(fp);
	unlink(images_list);

	return ret;
}


}
