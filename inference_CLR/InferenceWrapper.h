
#pragma once

#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <string>
#include "DeepLearning.h"
#include "Status.h"
#include "iCLR_inference.h"

using namespace System;
using namespace System::Collections::Generic;
using namespace CiKIT;

namespace CiKIT  {

	//private delegate void segmentation_callback (void* user_data, float* result_ptr, size_t result_width, size_t result_height, size_t count, MSG_t status);

	/*!
	\brief This is a C++/CLI Wrapper Class for ITRI c/c++ library

	To use this class in your c#, please use  the CiKIT namespace and new a CLR_inference instance as below.
	\code

	using CiKIT;
	CiKIT.CLR_inference pObj;

	\endcode
	*/
	public ref class CLR_inference : public iCLR_inference
	{
	public:
		CLR_inference();
		~CLR_inference();


		/*!
		*
		* \brief Create the instance of CLR_inference and load Classifier.dll.
		*
		* \retval 0: Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS 	CreatInstant() override;


		/*!
		*
		* \brief Create the instance from the given .DLL file.
		*
		* \retval 0: Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS 	CreatInstant(System::String^ dll_name) override;


#ifdef SYNPOWER_ONLY
		/*!
		*
		* \brief Init the classification. The classification will be processed according to the given machine type.
		*
		* \param resource_path The path of the 'config.ini'. Please make sure all the other essential files included the config.ini are also put in the given resource directory.
		* \param GPU_id Specify the ID of GPUs to be used in the classification. The valid values are 0, 1, 2, and 3.
		* \param using_encrypted_net Specify the input DNN Net is encrypted or not. Default is true.
		* \param enable_cuDNN_engine Set to enable or disable the cuDNN engine on the computing. Default is true.
		* \param net_size Set the net topology flow. The value 1: Small-Net, 2: Medium-Net, 3: Large-Net. Default is 3.
		*
		* \retval 0: Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS  Init(unsigned int GPU_id, System::String^ resource_path, bool using_encrypted_net, bool enable_cuDNN_engine, size_t net_size) override;
#endif

		/*!
		*
		* \brief Init the classification. The classification will be processed according to the given machine type.
		*
		* \param resource_path The path of the 'config.ini'. Please make sure all the other essential files included the config.ini are also put in the given resource directory.
		* \param GPU_id Specify the ID of GPUs to be used in the classification. The valid values are 0, 1, 2, and 3.
		* \param enable_cuDNN_engine Set to enable or disable the cuDNN engine on the computing. Default is true.
		* \param net_size Set the net topology flow. The value 1: Small-Net, 2: Medium-Net, 3: Large-Net. Default is 3.
		*
		* \retval 0: Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS  Init(unsigned int GPU_id, System::String^ resource_path, bool enable_cuDNN_engine, size_t net_size) override;


		/*!
		*
		* \brief Get analysis time.
		*
		* \return The value of analysis time per image in ms (millisecond).
		*
		*/
		virtual float 	GetTime() override;


		/*!
		*
		* \brief Get total forward testing time.
		*
		* \return The value of total forward testing time in ms (millisecond).
		*
		*/
		virtual float 	GetForwardTestingTime() override;


		/*!
		*
		* \brief Get the labels and number of labels. This could only be called when the init() function is executed successfully.
		*
		* \param labels A List to store the labels.
		* \retval The number of labels
		*
		*/
		virtual STATUS		GetLabels(List<System::String^>^ labels) override;


		/*!
		*
		* \brief Set the maximum size of the cache buffer to buffering the incomming Image and processing the batching classification.
		*
		* \param size The maximum size for caching and batching process.
		* \retval =0 Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS		BatchAnalysis_SetCacheMaxSize(int size) override;


		/*!
		*
		* \brief Get the current number of elements in the cache buffer.

		* \retval The current number of elements in cache.
		*
		*/
		virtual int		BatchAnalysis_GetSizeOfCache() override;


		/*!
		*
		* \brief Clear the cache buffer.
		*
		* \return none.
		*/
		virtual void	BatchAnalysis_ClearCache() override;


		/*!
		*
		* \brief Check the USB Key status. This could be called after the Classifier instance is created before the init() function.
		*
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		CheckUSB_Key() override;


		/*!
		*
		* \brief Add a new incomming Image data into the cache buffer to wait for classification.
		*
		* \param cstrImage	The filename of image.
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		BatchAnalysis_AddToCache_withFilename(System::String^ cstrImage) override;


		/*!
		*
		* \brief Add a new incomming Bitmap image into the cache buffer to wait for classification.
		*
		* \param bitmap	The bitmap image.
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		BatchAnalysis_AddToCache(System::Drawing::Bitmap^ bitmap) override;


		/*!
		*
		* \brief Use this function to get both the classification label number and probabilities for the given images stored in the cached buffer.
		*        If the predicted probability of predicted label number is lower than the given threshold, then the label number with second high probability
		*        will be cloosen as the predicted label number.
		* \param predict_array A List of label numbers to store the predicted results for each input image.
		* \param proba_array A float List to store the predicted probabilities.
		* \param threshold_array A float List to store the threshold to filter the predicted probabilities. 
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		Classification_WithThreshold(List<int>^ predict_array, List<float>^ proba_array, List<float>^ threshold_array) override;


		/*!
		*
		* \brief Use this function to get both the classification label number and probabilities for the given images stored in the cached buffer.
		*        If the predicted probability of predicted label number is lower than the given threshold, then the label number with second high probability
		*        will be cloosen as the predicted label number.
		* \param predict_array A List of label numbers to store the predicted results for each input image.
		* \param proba_array A float List to store the predicted probabilities.
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		Classification(List<int>^ predict_array, List<float>^ proba_array) override;


		/*!
		*
		* \brief Destroy this instance.
		*
		* \return none.
		*
		*/
		virtual void 	Destroy() override;

#ifndef SYNPOWER_ONLY
#if 0
		/*!
		*
		* \brief Use this function to get the segmentation results.
		* \param confidential_score_array A List of float array to store the confidential score for input images added via BatchAnalysis_AddToCache().
		* \retval =0 Success.
		* \retval <0 Failure. Please check Status.h for detail.
		*
		*/
		virtual STATUS		Segmentation (List<float>^ confidential_score_array) override;
#endif

		/*!
		*
		* \brief Classify and dispatch the images. The input images are from the source path <b> src_path </b>. 
		*        After analizing the predicted probabilities, the images will be moved from the source path to the destination path.
		* \param dest_path The path to put the classified images.
		* \param src_path The path of comming images to be classified.
		* \param s_temp_list_filename A temporary file to store image mapping list for perdiction.
		* \param batch_size The maximum size for caching and batching process.
		* \param threshold_array A float List to store the threshold to filter the predicted probabilities.
		* \retval =0 Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS	DispatchImages(System::String^ dest_path, System::String^ src_path, System::String^ s_temp_list_filename, int batch_size, List<float>^ threshold_array) override;


		/*!
		*
		* \brief Run inference with a given image folder. All the images inside the folder will be analized recursively.
		* \param s_src_path The folder to run classification analysis.
		* \param batch_size The maximum size for caching and batching process.
		* \param result_file_array A List of processed image filenames.
		* \param result_predict_array A List of prediction results coming from the processed images in the result_file_array.
		* \param s_temp_list_filename A temporary file to store image mapping list int the perdiction process.
		* \retval =0 Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS	InferenceSpecifiedFolder(System::String^ s_src_path, int batch_size, List<System::String^>^ result_file_array, List<int>^ result_predict_array, System::String^ s_temp_list_filename);


		/*!
		*
		* \brief Run inference with a given image folder. All the images inside the folder will be analized recursively.
		* \param s_src_path The folder to run classification analysis.
		* \param batch_size The maximum size for caching and batching process.
		* \param threshold_array A threshold List for inference.
		* \param result_file_array A result List after processing image prediction.
		* \param result_predict_array A List of prediction results coming from the processed images in the result_file_array.
		* \param s_temp_list_filename A temporary file to store image mapping list int the perdiction process.
		* \retval =0 Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS  InferenceSpecifiedFolderWithThreshold(System::String^ s_src_path, int batch_size, List<float>^ threshold_array,
			List<System::String^>^ result_file_array, List<int>^ result_predict_array, System::String^ s_temp_list_filename);

#endif
		/*!
		*
		* \brief Run inference with a given image folder and get confusion matrix back. All the images inside the folder will be analized recursively.
		* \param mapping_filename A image file list for perdiction process.
		* \param batch_size The maximum size for caching and batching process.
		* \param threshold_array A threshold List for inference.
		* \param result_file_array A image filename List of the processed images.
		* \param result_GT_array The ground truth List of the processed images.
		* \param result_predict_array A result List after processing image prediction.
		* \param result_confusion_matrix A confusion matrix after image prediction. The 2 dimenstion results are stored in a sequential array.
		* \param out_filename_of_confusion The output filename of the confusion matrix.
		* \param path_of_uncertainty_matrix A output path of uncertainty matrix.
		* \param confusion_prefix_name The prefix name of the confusion matrix.
		* \retval =0 Success.
		* \retval <0 Please check STATUS.h for detail.
		*
		*/
		virtual STATUS	Classification_WithMappingFile(System::String^ mapping_filename, int batch_size, List<float>^ threshold_array,
			List<System::String^>^ result_file_array, List<int>^ result_GT_array, List<int>^ result_predict_array, List<int>^ result_confusion_matrix,
			System::String^ out_filename_of_confusion, System::String^ path_of_uncertainty_matrix, System::String^ confusion_prefix_name) override;


	private:
		STATUS		add_image_to_cache(char* image_filename);
		int			FindLabels();

		IDeepLearning	*m_pObj;
		int		*m_predict_array;
		float	*m_proba_buffer;
		int		m_num_of_labels;
		int		m_max_of_cache_num;
		int		m_max_count_of_proba;
		List<System::String^> m_label_codes;
		float	*m_threshold_buffer;
		int		m_threshold_count;
	};
}
