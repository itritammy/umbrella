﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CiKIT;
using System.Data.SqlClient;

using System.Threading;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.IO;
using System.Diagnostics;
using System.Deployment.Application;

namespace classifier_truck_GUI
{
    public partial class Form1 : Form
    {
        CiKIT.CLR_inference pObj = null;
        SqlConnection db_cn;

        bool is_classify = false;
        int img_batch_num = 0;
        string non_classify_type = "-1";
        string server_name = "";
        string database = "";
        string table_name = "";
        string user_id = "";
        string password = "";
        string model_path = "";
        string log_text = "";
        int gpu_id = 0;
        IniManager iniManager = null;
        private BackgroundWorker m_BackgroundWorker;
        StreamWriter log_file = null;
        DateTime time;

        public class dataInfo
        {
            public int id { get; set; }
            public string path { get; set; }
            public int predic { get; set; }
        }

        public Form1()
        {
            // default init file loading.
            InitializeComponent();
            iniManager = new IniManager("./default.ini");
            server_name = iniManager.ReadIniFile("db_connect", "server_name", "default");
            database = iniManager.ReadIniFile("db_connect", "database", "default");
            table_name = iniManager.ReadIniFile("db_connect", "table_name", "default");
            user_id = iniManager.ReadIniFile("db_connect", "user_id", "default");
            password = iniManager.ReadIniFile("db_connect", "password", "default");
            gpu_id = Convert.ToInt32(iniManager.ReadIniFile("Setting", "gpu_id", "default"));
            img_batch_num = Convert.ToInt32(iniManager.ReadIniFile("Setting", "batch_size", "default"));
            model_path = iniManager.ReadIniFile("Setting", "model_path", "default");

            // BackgroundWorker initialize.
            m_BackgroundWorker = new BackgroundWorker();
            m_BackgroundWorker.DoWork += new DoWorkEventHandler(DoWork);
            m_BackgroundWorker.WorkerSupportsCancellation = true;

            // Log file initialize.
            string path = ".\\classification_log.txt";
            if (File.Exists(path))
                File.Delete(path);
            log_file = File.CreateText(path);

            time = new DateTime();
        }

        void DoWork(object sender, DoWorkEventArgs e)  
        {
            // Check database connection
            if (connection() == false)
            {
                log_file.Flush();
                log_file.Close();
                return;
            }
            // Check init model success
            if (InitModel() == false)
            {
                log_file.Flush();
                log_file.Close();
                return;
            }
            // Run classification
            is_classify = true;
            int non_process_img_num = 0;  // record non process img number. After batch analyze, return to zero.
            time = DateTime.Now;
            log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " Start classification";
            log_file.WriteLine(log_text);
            try
            {
                while (is_classify)
                {
                    // Check is table with non process data.
                    string str_cmd = "SELECT AMEDITS, PATH, TYPE, CRDATE FROM " + "[" + table_name + "]" + " WHERE TYPE =" + non_classify_type;
                    SqlCommand cmdSelect = db_cn.CreateCommand();
                    cmdSelect.CommandText = str_cmd;
                    SqlDataAdapter adapter = new SqlDataAdapter(cmdSelect);

                    //Load select data to local dataSet
                    DataTable localTable = new DataTable();
                    adapter.Fill(localTable);

                    // Update table to list
                    List<dataInfo> datasInfo = new List<dataInfo>();
                    for (int i = 0; i < localTable.Rows.Count; i++)
                    {
                        dataInfo data = new dataInfo(); ;
                        data.id = Convert.ToInt32(localTable.Rows[i][0]);
                        data.path = Convert.ToString(localTable.Rows[i][1]);
                        CiKIT.STATUS ret = pObj.BatchAnalysis_AddToCache_withFilename(data.path);
                        if (ret != CiKIT.STATUS.MSG_NO_ERROR)
                        {
                            time = DateTime.Now;
                            log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " Messages: Error: " + ret;
                            log_file.WriteLine(log_text);
                            pObj.BatchAnalysis_ClearCache();
                            //[Todo] 標註有問題影像至資料庫
                            continue;
                        }

                        datasInfo.Add(data);
                        non_process_img_num++;

                        if (non_process_img_num >= img_batch_num)
                        {
                            List<int> predict_array = new List<int>();
                            List<float> proba_array = new List<float>();
                            ret = pObj.Classification(predict_array, proba_array);
                            if (ret == CiKIT.STATUS.MSG_NO_ERROR)
                            {
                                for (int j = 0; j < predict_array.Count(); j++)
                                    datasInfo[j].predic = predict_array[j];
                                writeToDatabase(datasInfo);
                            }
                            else
                            {
                                time = DateTime.Now;
                                log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " Messages: Error: " + ret;
                                log_file.WriteLine(log_text);
                                pObj.BatchAnalysis_ClearCache();
                                datasInfo.Clear();
                                return;
                            }
                            non_process_img_num = 0;
                            datasInfo.Clear();
                        }
                    }

                    non_process_img_num = 0;
                    datasInfo.Clear();
                    pObj.BatchAnalysis_ClearCache();
                    log_file.Flush();
                    Thread.Sleep(100);
                }
            }
            catch (SqlException error)
            {
                time = DateTime.Now;
                log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " " + error.ToString();
                log_file.WriteLine(log_text);
            }
            log_file.Flush();
            log_file.Close();
        }

        private bool  InitModel()
        {
            pObj = new CiKIT.CLR_inference();
            pObj.CreatInstant();

            bool enable_cuDNN_engine = true;
            ulong net_size = 3;

            CiKIT.STATUS ret = pObj.Init((uint)gpu_id, model_path, enable_cuDNN_engine, net_size);
            time = DateTime.Now;
            if (ret != CiKIT.STATUS.MSG_NO_ERROR)
            {
                log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " Messages: Init failed " + ret;
                log_file.WriteLine(log_text);
                log_file.Flush();
                return false;
            }

            // Set cache size
            ret = pObj.BatchAnalysis_SetCacheMaxSize(img_batch_num);
            time = DateTime.Now;
            if (ret != CiKIT.STATUS.MSG_NO_ERROR)
            {
                log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " Messages: Error: " + ret ;
                log_file.WriteLine(log_text);
                log_file.Flush();
                return false;
            }

            log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture)  + " Init model success";
            log_file.WriteLine(log_text);
            return true;
        }

        private bool  connection()
        {
            string connection_str = "Persist Security Info=False;User ID=" + user_id + ";Password=" + password+";Initial Catalog=" + database  + ";Server=" + server_name;
            db_cn = new SqlConnection(connection_str);
            time = DateTime.Now;

            try
            {
                db_cn.Open();
                log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " Messages: Connection Success." ;
                log_file.WriteLine(log_text);
                return true;
            }

            catch (SqlException error)
            {
                log_text = time.ToString(System.Globalization.CultureInfo.InvariantCulture) + " Messages: Connection Fail. Please check connect information.";
                log_file.WriteLine(log_text);
                log_file.WriteLine(error.ToString());
                log_file.WriteLine("connectuin_str:" + connection_str);
                return false;
            }
        }

        private bool writeToDatabase(List<dataInfo> datasInfo)
        {

            // write all classify datas to database
            string update_cmd = "UPDATE " + "[" + table_name + "]" + " SET TYPE = @predict WHERE PATH=@path ";
            for (int i=0; i< datasInfo.Count(); i++)
            {
                //string str_select = "SELECT AMEDITS, PATH, TYPE, CRDATE FROM " + "[" + comboBox_table.Text + "]" + " WHERE TYPE =" + non_classify_type;
                SqlCommand cmd = new SqlCommand(update_cmd, db_cn);
                cmd.Parameters.AddWithValue("@path", datasInfo[i].path);
                cmd.Parameters.AddWithValue("@predict", datasInfo[i].predic);
                //cmd.Parameters.AddWithValue("@id", datasInfo[i].id);
                cmd.ExecuteNonQuery();
                //dataTable.Rows[]
                //dsProducts.Tables[0].Rows[0]["UserName"] = System.DateTime.Now.ToString();
                //dsProducts.Tables[0].Rows[1]["UserName"] = System.DateTime.Now.ToString();
                //dsProducts.Tables[0].Rows[2]["UserName"] = System.DateTime.Now.ToString();

            }

            datasInfo.Clear();
            return true;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            is_classify = true;
            m_BackgroundWorker.CancelAsync();
            
        }

        private void Form1_Shown(object sender, EventArgs e)
        {
            m_BackgroundWorker.RunWorkerAsync(this);	 // Run backgroundworker to do analyze.
        }
    }

    public class IniManager
    {
        private string filePath;
        private StringBuilder lpReturnedString;
        private int bufferSize;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string lpString, string lpFileName);
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);
        public IniManager(string iniPath)
        {
            filePath = iniPath;
            bufferSize = 512;
            lpReturnedString = new StringBuilder(bufferSize);
        }

        // read ini date depend on section and key
        public string ReadIniFile(string section, string key, string defaultValue)
        {
            lpReturnedString.Clear();
            GetPrivateProfileString(section, key, defaultValue, lpReturnedString, bufferSize, filePath);
            return lpReturnedString.ToString();
        }
        // write ini data depend on section and key
        public void WriteIniFile(string section, string key, Object value)
        {
            WritePrivateProfileString(section, key, value.ToString(), filePath);
        }
    }
}
