# System requirements:
=======================================================
- Install the latest NVIDIA video driver from https://www.nvidia.com/Download/index.aspx?lang=en-us


# Guide to run the training tool:
=======================================================
Run execution file `training_tool.exe` to launch the training tool.


# Revision History:
=======================================================
Version 2.8.6:
- Update training_tool in order to compatible with classification_tool.

Version 2.8.5:
- Add msvcp120.dll and msvcr120.dll in release package.

Version 2.8.4:
- Auto switch to confusion matrix table after training is finished.

Version 2.8.3:
- Fix crash issue when the Image(H/W) value for Training/Validation Net is improper.

Version 2.8.2:
- Support image stretch when browsing the uncertain images
- Support opening log files.

Version 2.8.1:
- Fix down-sampling factor assigning incorrect bug.
- Abort the training process if the training loss is 87.3365. It means the training would not be able to converge.
  Users need to modify the learning rate, enlarge the batch size, and change related parameters to try again.

Version 2.7.8:
- Showing correct error code when click training "Start" button failed.

Version 2.7.6:
- Fix pseudo data balance parameters incorrect issue.
- Support default system encoding character-set (e.g.,Big5) on folder name for output folder 
  of misclassified images.

Version 2.7.5:
- Support move out the misclassified images in the confusion matrix table.

Version 2.7.4:
- Output optimal model also, even the user cancel the training.
- Fix saving top-N mode selectedindex incorrect bug.

Version 2.7.3:
- Fix inference not work issue.
- Fix maximum iteration UI not update issue.

Version 2.7.2:
- Fix calculating mean values for gray picture not correct issue.

Version 2.7.1:
- Fix calculating mean values crash bug.

Version 2.7:
- Support saving top N optimal models
- Add data balance factor for data augmentation.
- Support viewing the miss-classified images on confusion matrix.

Version 2.6:
- Support running multiple Apps at the same time.
- Update Net topologies to reduce the usage of GPU memory.

Version 2.5:
- Add more data augmentation parameters.

Version 2.4:
- Fix the bug of calculating the confusion matrix.
- Packaging the Large/Medium/Small net topologies into the optimal model for inference.

Version 2.3:
- Solve crash bug when the batch size is too big while using over 2 GPUs.
- Support logging file output to ./log folder.
- Support data augmentation for training images.
- Support generating confusion matrix for training, validation, and testing data.

Version 2.2:
- Solve bugs to avoid getting random or the same probability on the output model.

Version 2.1:
- Add confusion matrix after training is done.

Version 2.0:
- Show alarm message when running out of CUDA memory.
- Show alarm message when setting the batch size for training larger than the available training images.

Version 1.9:
- Support Windows 7 platform.
- Avoid system crash while clicking the "Output" model button if the training had not been processed.

Version 1.8:
 - Support customizing the number of classification classes.
 - Support USB license  key check.


# Package contents:
=======================================================
./
│  README.md             (This document) 
│  training_tool.exe     (This main program) 
├─ data                  (The Core data for training)
├─ USB_key_driver        (Driver for USB licence key dongle)
└─ *.dll                 (The essential runtime DLLs.)
