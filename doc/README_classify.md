# System requirements:
=======================================================
- Install the latest NVIDIA video driver from https://www.nvidia.com/Download/index.aspx?lang=en-us


# Guide to run the training tool:
=======================================================
Run execution file `classification_tool.exe` to launch the classification tool.


# Revision History:
=======================================================
Version 1.7:
- Update model to fit user requirements.

Version 1.6:
- Update model to fit user requirements(Add 鏟車 data).

Version 1.5:
- Add user id and password for sql server connection.

Version 1.4:
- Update GUI interface to fit user requirements. 
- Add classifcation_log.txt when classification is running.

Version 1.3:
- Update model to fit user requirements.

Version 1.2:
- Update GUI and default.ini to fit customer requirements.

Version 1.1:
- Add windows system library to prevent init fail.

Version 1.0:
 - First deliver version.


=======================================================
./
│  doc
│  		README_classify.md         (This document) 
│  classification_tool.exe         (This main program) 
│  USB_key_driver                  (Driver for USB licence key dongle)
│  *.dll                           (The essential runtime DLLs.)
│  Classicifation_tool_v1.0.pptx   (Document for classification tool.)       

