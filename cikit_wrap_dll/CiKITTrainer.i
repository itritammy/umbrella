%module CiKIT
 
%{

#include "CiKIT.h"
#include "CiKITTrainer.h"

using namespace CiKIT;

%}

%include <windows.i> 

%include <std_common.i>
%include <std_string.i>
%include <std_array.i>
%include <arrays_csharp.i>
%include <swigtype_inout.i>
%include <cpointer.i>
%include <carrays.i>

%include "typemaps.i"

%define %standard_byref_params(TYPE)
%apply TYPE& INOUT { TYPE& };
%apply TYPE& OUTPUT { TYPE& result };
%enddef

%standard_byref_params(bool)
%standard_byref_params(signed char)
%standard_byref_params(unsigned char)
%standard_byref_params(short)
%standard_byref_params(unsigned short)
%standard_byref_params(int)
%standard_byref_params(unsigned int)
%standard_byref_params(long)
%standard_byref_params(unsigned long)
%standard_byref_params(long long)
%standard_byref_params(unsigned long long)
%standard_byref_params(float)
%standard_byref_params(double)


%include <std_string.i>
%include <std_vector.i>
%include <std_pair.i>

%naturalvar tstring;
class tstring;
%apply std::string { tstring }
%apply const std::string & { const tstring & }


/* Failed: No typemaps are defined
%naturalvar wtstring;
class wtstring;
%apply std::wstring { wtstring }
%apply const std::wstring & { const wtstring & }
*/


%include "std_vector.i"
// Instantiate templates used by example
namespace std {
   %template(IntVector) vector<int>;
   %template(DoubleVector) vector<double>;
   %template(StringVector) vector<string>;
}


%include "../caffe.git/src/common_utils/message_defines.h"
%include "../caffe.git/src/common_utils/CiKIT.h"
%include "../caffe.git/src/common_utils/CiKITTrainer.h"

