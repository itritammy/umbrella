#ifndef __CUSTOM_TRAINING_FEATURES_H__
#define __CUSTOM_TRAINING_FEATURES_H__

#include "message_defines.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct
{
	char feature[48];
} stTRAINING_FEATURE_MAP;

MSG_t CheckTrainingLicense(stTRAINING_FEATURE_MAP* map);
MSG_t CheckTrainingFeatures(stTRAINING_FEATURE_MAP* map);


#ifdef __cplusplus
}
#endif

#endif