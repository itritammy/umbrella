#!bash
#
OUT_DIR=publish

echo ""
echo "Release to ./"${OUT_DIR}

mkdir -p ${OUT_DIR}/bin/x64/Debug
mkdir -p ${OUT_DIR}/bin/x64/Release
mkdir -p ${OUT_DIR}/include
mkdir -p ${OUT_DIR}/lib/x64/Debug
mkdir -p ${OUT_DIR}/lib/x64/Release

echo "Prepare the DLLs ..."
# Release version
CAFFE_PATH=../caffe.git
CAFFE_BUILD=${CAFFE_PATH}/Build
cp -a ${CAFFE_BUILD}/x64/Release/cublas64_92.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/cudart32_92.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/cudart64_92.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/cudnn64_7.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/curand64_92.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/gflags.dll   				${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/hdf5*.dll   				${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/libgcc_s_seh-1.dll   		${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/libgflags.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/libgfortran-3.dll   		${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/libglog.dll   				${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/libopenblas.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/libquadmath-0.dll   		${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/lmdb.dll   				${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/opencv_*.dll   			${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/szip.dll   				${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/zlib.dll   				${OUT_DIR}/bin/x64/Release
cp -a ${CAFFE_BUILD}/x64/Release/gflags.dll 				${OUT_DIR}/bin/x64/Release/libgflags.dll
# Debug version
cp -a ${CAFFE_BUILD}/x64/Debug/cublas64_92.dll   			${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/cudart32_92.dll   			${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/cudart64_92.dll   			${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/cudnn64_7.dll   				${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/curand64_92.dll   			${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/gflagsd.dll   				${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/hdf5*.dll   					${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/libgcc_s_seh-1.dll   		${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/libgflags-debug.dll   		${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/libgfortran-3.dll   			${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/libglog.dll   				${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/libopenblas.dll   			${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/libquadmath-0.dll   			${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/lmdbD.dll   					${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/opencv_*.dll   				${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/szip.dll   					${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/zlib.dll   					${OUT_DIR}/bin/x64/Debug
cp -a ${CAFFE_BUILD}/x64/Debug/gflagsd.dll 					${OUT_DIR}/bin/x64/Debug/libgflags-debug.dll

echo "Prepare the lib ... "
cp -a ${CAFFE_BUILD}/x64/Release/libcaffe.lib 				${OUT_DIR}/lib/x64/Release
cp -a ${CAFFE_BUILD}/x64/Debug/libcaffe.lib 				${OUT_DIR}/lib/x64/Debug

echo "Prepare the resource ... "
cp -a ./ITraining.h											./${OUT_DIR}/include/
cp -a ${CAFFE_PATH}/windows/common_utils/info_GPU.h			./${OUT_DIR}/include/
cp -a ${CAFFE_PATH}/windows/common_utils/message_defines.h	./${OUT_DIR}/include/
cp -a ${CAFFE_PATH}/include/caffe/training_history.hpp		./${OUT_DIR}/include/
cp -a ../x64/Release/Training_terapower.dll   				${OUT_DIR}/bin/x64/Release
cp -a ../x64/Debug/Training_terapower.dll   				${OUT_DIR}/bin/x64/Debug

echo "Prepare the AVI models ..."
mkdir -p ${OUT_DIR}/data/models/AVI
cp -a ./training_resource/models/AVI/AVI.caffemodel		${OUT_DIR}/data/models/AVI
cp -a ./training_resource/models/AVI/AVI.aes			${OUT_DIR}/data/models/AVI
cp -a ./training_resource/models/AVI/train_net_base.aes	${OUT_DIR}/data/models/AVI
cp -a ./training_resource/models/AVI/val_net_base.aes	${OUT_DIR}/data/models/AVI
cp -a ./training_resource/models/AVI/default.proj		${OUT_DIR}/data/models/AVI

echo "Prepare the TEL models ..."
mkdir -p ${OUT_DIR}/data/models/TEL
cp -a ./training_resource/models/TEL/TEL.caffemodel		${OUT_DIR}/data/models/TEL
cp -a ./training_resource/models/TEL/TEL.aes			${OUT_DIR}/data/models/TEL
cp -a ./training_resource/models/TEL/train_net_base.aes	${OUT_DIR}/data/models/TEL
cp -a ./training_resource/models/TEL/val_net_base.aes	${OUT_DIR}/data/models/TEL
cp -a ./training_resource/models/TEL/default.proj		${OUT_DIR}/data/models/TEL

echo "Prepare the UF3000 models ..."
mkdir -p ${OUT_DIR}/data/models/UF3000
cp -a ./training_resource/models/UF3000/UF3000.caffemodel	${OUT_DIR}/data/models/UF3000
cp -a ./training_resource/models/UF3000/UF3000.aes			${OUT_DIR}/data/models/UF3000
cp -a ./training_resource/models/UF3000/train_net_base.aes	${OUT_DIR}/data/models/UF3000
cp -a ./training_resource/models/UF3000/val_net_base.aes	${OUT_DIR}/data/models/UF3000
cp -a ./training_resource/models/UF3000/default.proj		${OUT_DIR}/data/models/UF3000

echo "Prepare the UF3000EX models ..."
mkdir -p ${OUT_DIR}/data/models/UF3000EX
cp -a ./training_resource/models/UF3000EX/UF3000EX.caffemodel	${OUT_DIR}/data/models/UF3000EX
cp -a ./training_resource/models/UF3000EX/UF3000EX.aes			${OUT_DIR}/data/models/UF3000EX
cp -a ./training_resource/models/UF3000EX/train_net_base.aes	${OUT_DIR}/data/models/UF3000EX
cp -a ./training_resource/models/UF3000EX/val_net_base.aes		${OUT_DIR}/data/models/UF3000EX
cp -a ./training_resource/models/UF3000EX/default.proj			${OUT_DIR}/data/models/UF3000EX

echo ""
echo "Done."