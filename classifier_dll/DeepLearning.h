//
// Copyright (c) ITRI. All rights reserved.
// Licensed under the ITRI license. 
// See LICENSE.md file in the project root for full license information.
//
// DeepLearning.h : Defines the exported functions.
//

#ifndef __DeepLearning_h__
#define __DeepLearning_h__

#ifdef ITRI_EXPORTS
#define ITRI_API __declspec(dllexport) 
#else
#define ITRI_API __declspec(dllimport) 
#endif

#include <string>
#include <vector>

// 2019_1107,Jim: To avoid error LNK2001: unresolved external symbol "public: virtual bool __cdecl cv::HOGDescriptor
//#include <opencv\cv.h>
//#include <opencv\highgui.h>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>

#include "message_defines.h"

/*!
\brief A class for dealing with classfication

This class is an interface for client's integration.
User should use below statement to get the factory function to obtain an instance.

\note Get the factory function to create an instance.
\code
IDeepLearningFactory FactoryFuncClassifier = R_CAST<IDeepLearningFactory>(::GetProcAddress(hDLLClassifier, "CreateDeepLearning"));
\endcode
\note Create a classifier.
\code
IDeepLearning * pObjClassifier = FactoryFuncClassifier();
\endcode

*/
class IDeepLearning
{
public:

	/*!
	*
	* \brief Init the classification. The classification will be processed according to the given machine type.
	*
	* \param resource_path The path of the 'config.ini'. Please make sure all the other essential files included the config.ini are also put in the given resource directory.
	* \param GPU_id Specify the ID of GPUs to be used in the classification. The valid values are 0, 1, 2, and 3.
	* \param enable_cuDNN_engine Set to enable or disable the cuDNN engine on the computing. Default is true.
	* \param net_size Set the net topology flow. The value 1: Small-Net, 2: Medium-Net, 3: Large-Net. Default is 3.
	*
	* \retval 0: Success.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t Init(const char* resource_path, size_t GPU_id, bool enable_cuDNN_engine = true, size_t net_size = 3) = 0;


	/*!
	*
	* \brief Get analysis time.
	*
	* \return The value of analysis time per image in ms (millisecond).
	*
	*/
	virtual float GetTime() = 0;


	/*!
	*
	* \brief Get total forward testing time.
	*
	* \return The value of total forward testing time in ms (millisecond).
	*
	*/
	virtual float GetForwardTestingTime() = 0;


	/*!
	*
	* \brief Set the maximum size of the cache buffer to buffering the incomming Image and processing the batching classification.
	*
	* \param size The maximum size for caching and batching process.
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t BatchAnalysis_SetCacheMaxSize(size_t size) = 0;


	/*!
	*
	* \brief Add a new incomming Image data into the cache buffer in CPU memory and wait for classification.
	*
	* \param rows	Number of rows in a 2D array.
	* \param cols	Number of columns in a 2D array.
	* \param type	Array type. Use CV_8UC1, ..., CV_64FC4 to create 1-4 channel matrices, or CV_8UC(n), ..., CV_64FC(n) to create multi-channel (up to CV_CN_MAX channels) matrices.
	*				For example,
	*				CV_8UC3       : It means a 3 channels image where each value is 8 bits, unsigned char. The enum value is 16.
	*				CV_8UC1       : It means a 1 channels image where each value is 8 bits, unsigned char. The enum value is 0.
	*				CV_8UC4       : It means a 4 channels image where each value is 8 bits, unsigned char. The enum value is 24.
	* \param data	Pointer to the CV image data in B G R sequence.
	* \param step	Number of bytes each matrix row occupies. The value should include the padding bytes at the end of each row, if any.
	*				For example, a CV_8UC3 image with width 92, then the step vaule is 276 (92 multiplied by 3).
	* \retval size	The size is the value of image numbers that already in the cache buffer after calling this function.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t BatchAnalysis_AddToCache(int rows, int cols, int type, void *data, size_t step) = 0;


	/*!
	*
	* \brief Get the current number of elements in the cache buffer.
	* \return The current number of elements in cache.
	*
	*/
	virtual int BatchAnalysis_GetSizeOfCache() = 0;


	/*!
	*
	* \brief Clear the cache buffer.
	*
	* \return none.
	*/
	virtual void BatchAnalysis_ClearCache() = 0;


	/*!
	*
	* \brief Use this function to get the classification probabilities for each label for the given images stored in the cached buffer.
	*
	* \param result A float array to store the classified probabilities.
	* \param max_result_size A maximum size of given array to store the classifition probabilities. The size is usually equal to label numbers x cached image numbers.
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t BatchAnalysis_Run(float *result, size_t max_result_size) = 0;


	/*!
	*
	* \brief Use this function to get the classification results in labeled number for each image which stored in the cached buffer.
	*
	* \param result A integer array to store the classified results in label number for each image. The label number should be an integer number.
	* \param max_result_size A maximum size of given array to store the classifition results.
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t BatchAnalysis_Run(int *result, size_t max_result_size) = 0;


	/*!
	*
	* \brief Use this function to get both the classification number and probabilities for the given images stored in the cached buffer.
	*
	* \param predict_array A integer array to store the predicted classification results in label-number form for each image. The label-number should be an integer number.
	* \param max_count_of_predict A maximum size of given array to store the predicted classification results.
	* \param proba_buffer A float array to store the predicted probabilities.
	* \param max_count_of_proba A maximum size of given array to store the predicted probabilities. The size is usually equal to the label numbers multiplied by cached image numbers.
	* \param count The count of images is predicted.
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t BatchAnalysis_Run(int *predict_array, size_t max_count_of_predict, float* proba_buffer, size_t max_count_of_proba, size_t* count) = 0;


	/*!
	*
	* \brief Use this function to get both the classification label number and probabilities for the given images stored in the cached buffer.
	*        If the predicted probability of predicted label number is lower than the given threshold, then the label number with second high probability
	*        will be chosen as the predicted label number.
	* \param predict_array The array of label numbers to store the predicted results for each input image.
	* \param max_count_of_predict A maximum size of given array to store the predicted results in label number.
	* \param proba_buffer A float array to store the predicted probabilities.
	* \param max_count_of_proba A maximum size of given array to store the predicted probabilities. The size is usually equal to the number of labels x cached image numbers.
	* \param threshold A float array to store the threshold to filter the predicted probabilities. The suggested threshold is <b> 0.99993 </b>
	* \param Nb_of_threshold A maximum count of given threshold.
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t BatchAnalysis_Run_WithThreshold(int *predict_array, size_t max_count_of_predict, float* proba_buffer, size_t max_count_of_proba, float *threshold, size_t Nb_of_threshold) = 0;

#ifdef USE_CAFFE
	/*!
	*
	* \brief The callback function designed for segmentation runtime image evaluationr.
	* \param user_data The custom user data to be passed to the callback function.
	* \param result_ptr The start pointer of segmentation data.
	* \param result_width The width of segmentation data per image.
	* \param result_height The height of segmentation data per image.
	* \param count The amount fo image data inside the segmentation data.
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*/
	typedef void(*Segmentation_ResultCallback)(void* user_data, float* result_ptr, size_t result_width, size_t result_height, size_t count, MSG_t status);


	/*!
	*
	* \brief Run segmentation runtime image evaluationr.
	* \param callback The CallBack function to be called after the prediction of segmentation is done.
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*/
	virtual MSG_t BatchAnalysis_RunSegmentation(Segmentation_ResultCallback callback, void *user_data) = 0;
#endif

	/*!
	*
	* \brief Check the USB Key status. This could be called after the Classifier instance is created before the init() function.
	*
	* \retval =0 Success.
	* \retval <0 Please check message_defines.h for detail.
	*
	*/
	virtual MSG_t CheckUSB_Key() = 0;


	/*!
	*
	* \brief Get the labels and number of labels. This could only be called when the init() function is executed successfully.
	*
	* \param buffer A buffer to store the labels with the given seperating token, such as common ','. The output example might be look like "000,2EC,01B".
	* \param max_buffer_size The maximum size of given buffer.
	* \param label_delimiter A character delimiter to seperate the return labels.
	* \return The number of labels
	*
	*/
	virtual int GetLabels(char* buffer, size_t max_buffer_size, char label_delimiter) = 0;


	/*!
	*
	* \brief Returns a total number of classes for the model that is allocated in the classifier.
	*
	* \return The number of labels in integer.
	*
	*/
	virtual size_t GetClassTotalNum() = 0;


	/*!
	*
	* \brief Destroy this instance.
	*
	* \return none.
	*
	*/
	virtual void Destroy() = 0;

#ifdef USE_CAFFE
	/*!
	*
	* \brief Reduce Cuda Memory after fordward inference if you need to increase the amount of GPU memory available.
	*
	* \retval true Success.
	* \retval false Fail.
	*
	*/
	virtual bool ReduceCudaMemory() = 0;
#endif

	/*!
	*
	* \brief Returns the class name at the specified index position specified during training.
	*
	* \param index The index number of class label.
	* \return The name of given label number.
	*
	*/
	virtual const char* GetClassLabelName(size_t index) = 0;


	/*!
	*
	* \brief Returns the batch size for the GPU used in the prediction.
	* \param status The status after running this function. Please check message_defines.h for detail.
	* \return The number of batch size in integer.
	*
	*/
	virtual size_t GetBatchSize(MSG_t &status) = 0;


	/*!
	*
	* \brief Calculates and returns the maximum batch size for the current classification model.
	*
	* \param status Search status.
	* \param resource_path The path of the classification configuration file 'config.ini'. Please make sure all the other essential files included the config.ini are also put in the given resource directory.
	* \param GPU_id Specify the ID of GPUs to be used in the classification. The valid values are 0, 1, 2, and 3.
	* \param enable_cuDNN_engine Set to enable or disable the cuDNN engine on the DNN computing. Default is true.
	* \param net_size The net topology flow size. The value 1: Small-Net, 2: Medium-Net, 3: Large-Net. Default is 3.
	* \return The number of batch size in integer.
	*
	*/
	virtual size_t GetMaxBatchSize(MSG_t &status, const char* resource_path, size_t GPU_id, bool enable_cuDNN_engine = true, size_t net_size = 3) = 0;


	/*!
	*
	* \brief Trun on the logger to record messages based on the LogSeverity. This should be called after the Init() function is successed.
	*
	* \param filename The filename for the logger to store messages.
	* \param severity Logging severity.
	* \retval true Success.
	* \retval false Fail.
	*
	*/
	virtual bool Logger_TurnOn(const char* filename, LOG_SEVERITY_t severity) = 0;


	/*!
	*
	* \brief Write message into the logger and give the LogSeverity.
	*
	* \param message The message to be stored in the logger.
	* \param severity Logging severity of the given message.
	* \retval true Success.
	* \retval false Fail.
	*/
	virtual bool Logger_Write(const char* message, LOG_SEVERITY_t severity) = 0;


	/*!
	*
	* \brief Turn off the logger.
	*
	*/
	virtual void Logger_TurnOff() = 0;


	/*!
	*
	* \brief Set the logger severity. The valid enum values are LOG_SEVERITY_INFO = 0, LOG_SEVERITY_WARNING = 1, LOG_SEVERITY_ERROR = 2.
	*
	* \param severity Logging severity.
	* \retval true Success.
	* \retval false Fail.
	*
	*/
	virtual bool Logger_SetSeverity(LOG_SEVERITY_t severity) = 0;


	/*!
	*
	* \brief Reset the GPU to inital status.
	*
	* \param GPU_id The ID of GPU to be reset.
	* 
	*/
	virtual void ResetGPU(size_t GPU_id) = 0;
};

// A factory of IFasterRCNN-implementing objects.
extern "C" ITRI_API IDeepLearning * __cdecl CreateDeepLearning(void);

// A factory of IClassifier-implementing objects.
typedef IDeepLearning * (__cdecl *IDeepLearningFactory)();

#endif
