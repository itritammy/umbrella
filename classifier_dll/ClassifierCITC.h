
#ifndef __ClassifierCITC_h__
#define __ClassifierCITC_h__

#include <stdio.h>  
#include <stdlib.h> 
#include <io.h>   
#include <algorithm>
#include <iosfwd>
#include <memory>
#include <string>
#include <utility>
#include <vector>
#include <iostream>
#include <string>

#ifdef USE_CAFFE
#include <caffe/caffe.hpp>
#endif
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
/// 2018_0731,Jim: to support log file
#ifdef USE_CAFFE
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#endif
///

#include "DeepLearning.h"
#include "message_defines.h"
#include "INIReader.h"	// 2018_0713:Jim, to support .ini file read and paring.

#ifdef USE_CAFFE
	#include "Classifier.h"
#elif USE_PYTORCH
	#include "ClassifierV2.h"
#endif
//#include "reg_headers.h" // 2019_0808,Jim: User Library dependncy input instead. In this way, we can get better performance in run time.

class ClassifierCITC : public IDeepLearning 
{
public:
	ClassifierCITC();
	~ClassifierCITC(); 
	virtual MSG_t CheckUSB_Key();
	virtual void  SetGPUId(int GPU_id);
	virtual	MSG_t Init(const char* resource_path, size_t GPU_id, bool enable_cuDNN_engine = true, size_t net_size = 3);
	virtual	MSG_t Init(const char* resource_path, size_t GPU_id, bool enable_encrypted_net = true, bool enable_cuDNN_engine = true, size_t net_size = 3);
	virtual float GetTime(void);
	virtual int   GetCurrentCachedImageSize();
	virtual int   GetLabels(char* buffer, size_t max_buffer_size, char label_delimiter);
	virtual size_t GetClassTotalNum();
	virtual float GetForwardTestingTime(void);
	virtual MSG_t BatchAnalysis_SetCacheMaxSize(size_t size);
	virtual MSG_t BatchAnalysis_AddToCache(cv::Mat & cvImage);
	virtual MSG_t BatchAnalysis_AddToCache(int rows, int cols, int type, void *data, size_t step);
	virtual int   BatchAnalysis_GetSizeOfCache();
	virtual void  BatchAnalysis_ClearCache();
	virtual MSG_t BatchAnalysis_Run(float *result, size_t max_result_size);	// It seems to out of date. It only return the raw probabilites. 
	virtual MSG_t BatchAnalysis_Run(int *result, size_t max_result_size);
	virtual MSG_t BatchAnalysis_Run(int *result, size_t max_result_count, float* proba_buffer, size_t proba_max_count, size_t* count);
	virtual MSG_t BatchAnalysis_Run_WithThreshold(int *predict_array, size_t max_count_of_predict, float* proba_buffer, size_t max_count_of_proba, float *threshold, size_t Nb_of_threshold);
#ifdef USE_CAFFE
	virtual MSG_t BatchAnalysis_RunSegmentation(Segmentation_ResultCallback callback, void *user_data);
#endif
	virtual bool  ReduceCudaMemory();
	virtual void  Destroy();
	virtual const char* GetClassLabelName(size_t index);
	virtual size_t GetBatchSize(MSG_t &status);
	virtual size_t GetMaxBatchSize(MSG_t &status, const char* resource_path, size_t GPU_id, bool enable_cuDNN_engine, size_t net_size);
	virtual bool Logger_TurnOn(const char* filename, LOG_SEVERITY_t severity);
	virtual bool Logger_Write(const char* message, LOG_SEVERITY_t severity);
	virtual void Logger_TurnOff();
	virtual bool Logger_SetSeverity(LOG_SEVERITY_t severity);
	virtual void ResetGPU(size_t GPU_id);

private:
	MSG_t CheckSupportFeatures(char* params, int max_length);

#ifdef USE_CAFFE
	Classifier* m_classifier;
#elif USE_PYTORCH
	ClassifierV2* m_classifier;
#endif
};

#endif
