
#include "ClassifierCITC.h"
#include "itri_hasp_interface.h"

#include "custom_classifier_features.h"

/// 2020_0117,Jim: To check the validation list of MAC address 
#include "win_net_utils.h"
#include "cuda_api.h"

ClassifierCITC::ClassifierCITC()
{
#ifdef USE_CAFFE
	m_classifier = new Classifier();
#elif USE_PYTORCH
	m_classifier = new ClassifierV2();
#endif
	//std::cout << "Constructor " << __FUNCTION__ << "\n";
}


ClassifierCITC::~ClassifierCITC()
{
	if (m_classifier)
		delete m_classifier;
	//std::cout << "Destructor " << __FUNCTION__ << "\n";
}



/// --------------------------------------------------------------------------
/// Inherient functions
/// --------------------------------------------------------------------------

void ClassifierCITC::SetGPUId(int GPU_id)
{
	if (m_classifier)
		m_classifier->SetGPUId(GPU_id);
}


// 2020_0821,Jim: Support to use plain net.
MSG_t ClassifierCITC::Init(const char* resource_path, size_t GPU_id, bool enable_encrypted_net, bool enable_cuDNN_engine, size_t net_size)
{
	MSG_t status = CheckUSB_Key();
	if (status != MSG_NO_ERROR)
		return status;

	m_classifier->SetGPUId((int)GPU_id);
	return m_classifier->Init(std::string(resource_path), false, enable_encrypted_net, enable_cuDNN_engine, (int)net_size);
}


// always use the encryped net
MSG_t ClassifierCITC::Init(const char* resource_path, size_t GPU_id, bool enable_cuDNN_engine, size_t net_size)
{
	return Init(resource_path, GPU_id, true, enable_cuDNN_engine, net_size);
	//MSG_t status = CheckUSB_Key();
	//if (status != MSG_NO_ERROR)
	//	return status;

	//m_classifier->SetGPUId((int)GPU_id);
	//return m_classifier->Init(std::string(resource_path), false, enable_cuDNN_engine, (int)net_size); /// always use the encryped net
}


float ClassifierCITC::GetTime(void)
{
	return m_classifier->GetTime();
}


float ClassifierCITC::GetForwardTestingTime(void)
{
	return m_classifier->GetForwardTestingTime();
}


MSG_t ClassifierCITC::BatchAnalysis_SetCacheMaxSize(size_t size)
{
	return m_classifier->BatchAnalysis_SetCacheMaxSize((int)size);
}


MSG_t ClassifierCITC::BatchAnalysis_AddToCache(cv::Mat & cvImage)
{
	return m_classifier->BatchAnalysis_AddToCache(cvImage);
}


MSG_t ClassifierCITC::BatchAnalysis_AddToCache(int rows, int cols, int type, void *data, size_t step)
{
	// Encapsulate the image data into cv::Mat format.
	cv::Mat cvImage = cv::Mat(rows, cols, type, data, step);
	return m_classifier->BatchAnalysis_AddToCache(cvImage);
}


int ClassifierCITC::BatchAnalysis_GetSizeOfCache()
{
	return m_classifier->BatchAnalysis_GetSizeOfCache();
}


void ClassifierCITC::BatchAnalysis_ClearCache()
{
	m_classifier->BatchAnalysis_ClearCache();
}


/// for vs2010
MSG_t ClassifierCITC::BatchAnalysis_Run(float *result, size_t max_result_size)
{
	MSG_t ret;
	std::vector<float> probility_list;
	//int pic_id = 0;

	ret = m_classifier->BatchAnalysis_Run(probility_list);

	if (ret != MSG_NO_ERROR)
		return ret;

	for (int i = 0; i < probility_list.size() && i < max_result_size; i++)
	{
		result[i] = probility_list.at(i);
	}

	return MSG_NO_ERROR;
}


MSG_t ClassifierCITC::BatchAnalysis_Run(int *result, size_t max_result_size)
{
	return m_classifier->BatchAnalysis_Run_WithReturn_LabelNum(result, (int)max_result_size);
}


MSG_t ClassifierCITC::BatchAnalysis_Run(int *result, size_t max_result_count, float* proba_buffer, size_t proba_max_count, size_t* count)
{
	return m_classifier->BatchAnalysis_Run_WithReturn_PredNum_and_Proba(result, (int)max_result_count, proba_buffer, (int)proba_max_count, count);
}


int ClassifierCITC::GetCurrentCachedImageSize()
{
	return m_classifier->GetCurrentCachedImageSize();
}


MSG_t ClassifierCITC::CheckUSB_Key()
{
	/// 2020_0121,Jim: Add white list to pass the USB Key check
	if (CheckMAC_Address() == MSG_NO_ERROR)
		return MSG_NO_ERROR;
	/// end 

	stCLASSIFIER_FEATURE_MAP map;

	if (CheckClassifierLicense(&map) == MSG_NO_ERROR)
		return CheckClassifierFeatures(&map);
	else
		return MSG_USB_KEY_FEATURE_NOT_FOUND;
}


int ClassifierCITC::GetLabels(char* buffer, size_t max_buffer_size, char label_delimiter)
{
	if (m_classifier)
		return m_classifier->GetLabels(buffer, max_buffer_size, label_delimiter);
	else
		return 0;
}


size_t ClassifierCITC::GetClassTotalNum()
{
	if (m_classifier)
		return m_classifier->GetNumOfLabels();
	else
		return 0;
}


bool ClassifierCITC::ReduceCudaMemory()
{
#ifdef USE_CAFFE
	#ifdef ENABLE_RELEASE_CUDA_MEM
		return m_classifier->ReduceCudaMemory();
	#else
		return false;
	#endif
#elif USE_PYTORCH
	return false;
#else
	return false;
#endif
}

// Destroy this instance.
void ClassifierCITC::Destroy()
{
	//m_classifier->Destroy();
	if (m_classifier != NULL) {
		delete m_classifier;
		m_classifier = NULL;
	}
}


MSG_t ClassifierCITC::BatchAnalysis_Run_WithThreshold(int *predict_array, size_t max_count_of_predict, float* proba_buffer, size_t max_count_of_proba, float *threshold_array, size_t Nb_of_threshold)
{
	int		count_of_predicted = 0;
	MSG_t	ret = MSG_t::MSG_NO_ERROR;

	ret = m_classifier->BatchAnalysis_Run_WithThreshold_Return_PredNum_and_Proba(predict_array, max_count_of_predict,
		proba_buffer, max_count_of_proba, &count_of_predicted, threshold_array, Nb_of_threshold);

	return ret;
}

#ifdef USE_CAFFE
MSG_t ClassifierCITC::BatchAnalysis_RunSegmentation(Segmentation_ResultCallback callback, void *user_data)
{
	float  *reault_prob=NULL;
	size_t result_width=0;
	size_t result_height=0;
	size_t batch_size=0;

	MSG_t	ret = MSG_t::MSG_NO_ERROR;

	ret = m_classifier->BatchAnalysis_RunSegmentation(&reault_prob, &result_width, &result_height, &batch_size);

	callback(user_data, reault_prob, result_width, result_height, batch_size, ret);

	return ret;
}
#endif

const char* ClassifierCITC::GetClassLabelName(size_t index)
{
	if (m_classifier)
		return m_classifier->GetLabelCodeByLabelNumber((int)index);
	else
		return NULL;
}


size_t ClassifierCITC::GetBatchSize(MSG_t &status)
{
	status = MSG_NO_ERROR;

	if (m_classifier == NULL) {
		status =  MSG_CLASSIFIER_NOT_INITED;
		return 0;
	}

	return m_classifier->GetBatchSize();
}


size_t ClassifierCITC::GetMaxBatchSize(MSG_t &status, const char* resource_path, size_t GPU_id, bool enable_cuDNN_engine, size_t net_size)
{
	size_t batch_size_base = 5;
	size_t batch_size_delta = 5;
	int max_batch_size = batch_size_base;
	int prev_max_batch_size = max_batch_size;
	status = MSG_NO_ERROR;
#ifdef USE_CAFFE
	/// Create classifier instance
	Classifier *classifier = new Classifier();
	if (classifier == NULL)
	{
		status = MSG_CLASSIFIER_NOT_INITED;
		return 0;
	}

	/// Check USB Key
	status = CheckUSB_Key();
	if (status != MSG_NO_ERROR)
		return 0;

	/// Init classifier
	bool enable_CPU = false;
	classifier->SetGPUId(GPU_id);
	status = classifier->Init(std::string(resource_path), enable_CPU, enable_cuDNN_engine, (int)net_size);
	if (status != MSG_NO_ERROR) {
		delete classifier;
		return 0;
	}


	status = classifier->BatchAnalysis_SetCacheMaxSize(max_batch_size);

	// Create pseudo image for evaluate
	int width = 224;
	int height = 224;
	int type = CV_8UC3; // The enum value is 24.
	int channel = 3;
	int classNum = classifier->GetNumOfLabels();
	//unsigned char img_buf[244*244*3]; /// 2020_0615,Jim: To avoid exceeds/analyze:stacksize '16384'. Now 244x244x3 = 178876
	unsigned char *img_buf = (unsigned char *)malloc(width*height*channel);
	if (img_buf == NULL)
	{
		if (classifier != NULL)
			delete classifier;
		status = MSG_OUT_OF_FREE_MEMORY;
		return 0;
	}

	int unknown_num = 0;
	int NbOfPredicted = 0;
	int num_of_unknown_class = 0;

	int *analysis_result = NULL;
	float *analysis_proba = NULL;
	float *threshold_array = (float*)malloc(classNum * sizeof(float));


	//size_t used_m_bs10;
	size_t base_used_m;
	size_t free_m_prev;
	size_t mem_per_img;
	for (int k = 0; k < 6; k++)
	{
		//std::cout << "k=" << k << "\n";
		// Add images to cache
		for (int i = 0; i < max_batch_size; i++)
		{
			cv::Mat cvImage = cv::Mat(height, width, type, img_buf, width*channel);
			status = classifier->BatchAnalysis_AddToCache(cvImage);

			if (status != MSG_NO_ERROR) {
				if (classifier != NULL)
					delete classifier;
				if (threshold_array != NULL)
					free(threshold_array);
				if (img_buf != NULL)
					free(img_buf);
				return 0;
			}
		}


		analysis_result = (int*)malloc(max_batch_size * sizeof(int));
		analysis_proba = (float*)malloc(max_batch_size * classNum * sizeof(float));

		/// Run inference and get the result
		status = classifier->BatchAnalysis_Run_Type1(analysis_result, (int)max_batch_size,
			&NbOfPredicted, &unknown_num, threshold_array, classNum);

		if (analysis_result != NULL)
			free(analysis_result);
		if (analysis_proba != NULL)
			free(analysis_proba);


		if (status == MSG_CUDA_ALLOC_FAILED) {
			//std::cout << "MSG_CUDA_ALLOC_FAILED \n";
			break;
		}

		if (status != MSG_NO_ERROR) {
			prev_max_batch_size = 0;
			break;
		}

		size_t free_m;
		size_t total_m;

		cudaError_t cuda_err = CUDA_getGPU_MemInfo(GPU_id, &free_m, &total_m);

		if (k == 0) {
			prev_max_batch_size = max_batch_size;
			free_m_prev = free_m;
			base_used_m = total_m - free_m;
			max_batch_size += batch_size_delta;

			status = classifier->BatchAnalysis_SetCacheMaxSize(max_batch_size);
			batch_size_base = max_batch_size;
		}
		else if (k == 1)
		{
			prev_max_batch_size = max_batch_size;
			mem_per_img = ((total_m - free_m) - base_used_m) / batch_size_delta;
			max_batch_size = batch_size_base + (free_m / mem_per_img);

			status = classifier->BatchAnalysis_SetCacheMaxSize(max_batch_size);
			batch_size_base = max_batch_size;
			base_used_m = total_m - free_m;
		}
		else
		{
			//std::cout << "k=" << k << "\n";
			prev_max_batch_size = max_batch_size;
			max_batch_size++;

			status = classifier->BatchAnalysis_SetCacheMaxSize(max_batch_size);
		}
	}

	//final_exit:
	if (threshold_array != NULL)
		free(threshold_array);

	if (classifier != NULL)
		delete classifier;

	if (img_buf != NULL)
		free(img_buf);

	//cudaGetLastError();
	CUDA_deviceReset(GPU_id);

	//std::cout << "prev_max_batch_size=" << prev_max_batch_size << " \n";
	status = MSG_NO_ERROR;
	return prev_max_batch_size;
#elif USE_PYTORCH
	return prev_max_batch_size;
#endif
}


bool ClassifierCITC::Logger_TurnOn(const char* filename, LOG_SEVERITY_t severity)
{
	if (m_classifier == NULL)
		return false;

	return m_classifier->Logger_TurnOn(filename, severity);
}


bool ClassifierCITC::Logger_Write(const char* message, LOG_SEVERITY_t severity)
{
	if (m_classifier == NULL)
		return false;

	return m_classifier->Logger_Write(message, severity);
}


void ClassifierCITC::Logger_TurnOff()
{
	if (m_classifier != NULL)
		m_classifier->Logger_TurnOff();
}


bool ClassifierCITC::Logger_SetSeverity(LOG_SEVERITY_t severity)
{
	if (m_classifier == NULL)
		return false;

	return m_classifier->Logger_SetSeverity(severity);
}


void ClassifierCITC::ResetGPU(size_t GPU_id)
{
	CUDA_deviceReset((int)GPU_id);
}


// Create an instance of "Classifier".
extern "C" ITRI_API IDeepLearning * __cdecl CreateDeepLearning(void)
{
    //std::cout << "Create an instance.\n";
	return (IDeepLearning*) new ClassifierCITC();
}
//}
