#ifndef __CUSTOM_CLASSIFIER_FEATURES_H__
#define __CUSTOM_CLASSIFIER_FEATURES_H__

#include "message_defines.h"

#ifdef __cplusplus
extern "C" {
#endif


typedef struct
{
	char feature[48];
} stCLASSIFIER_FEATURE_MAP;

MSG_t CheckClassifierLicense(stCLASSIFIER_FEATURE_MAP* map);
MSG_t CheckClassifierFeatures(stCLASSIFIER_FEATURE_MAP* map);


#ifdef __cplusplus
}
#endif

#endif