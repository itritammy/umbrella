#include "caffe/common.hpp"
#if 1
#include "caffe/layers/input_layer.hpp"
#include "caffe/layers/inner_product_layer.hpp"
#include "caffe/layers/dropout_layer.hpp"
#include "caffe/layers/conv_layer.hpp"
#include "caffe/layers/relu_layer.hpp"

#include "caffe/layers/lrn_layer.hpp"
#include "caffe/layers/softmax_layer.hpp"
#endif

#include "caffe/layers/pooling_layer.hpp"

#include "caffe/layers/absval_layer.hpp"
#include "caffe/layers/accuracy_layer.hpp"
#include "caffe/layers/argmax_layer.hpp"
#include "caffe/layers/batch_norm_layer.hpp"
#include "caffe/layers/batch_reindex_layer.hpp"
#include "caffe/layers/bias_layer.hpp"
#include "caffe/layers/bnll_layer.hpp"
#include "caffe/layers/concat_layer.hpp"
#include "caffe/layers/contrastive_loss_layer.hpp"
#include "caffe/layers/crop_layer.hpp"
#include "caffe/layers/data_layer.hpp"
#include "caffe/layers/deconv_layer.hpp"
#include "caffe/layers/dropout_layer.hpp"
#include "caffe/layers/dummy_data_layer.hpp"
#include "caffe/layers/eltwise_layer.hpp"
#include "caffe/layers/elu_layer.hpp"
#include "caffe/layers/embed_layer.hpp"
#include "caffe/layers/euclidean_loss_layer.hpp"
#include "caffe/layers/exp_layer.hpp"
#include "caffe/layers/filter_layer.hpp"
#include "caffe/layers/flatten_layer.hpp"
#include "caffe/layers/hdf5_data_layer.hpp"
#include "caffe/layers/hdf5_output_layer.hpp"
#include "caffe/layers/hinge_loss_layer.hpp"
#include "caffe/layers/im2col_layer.hpp"
#include "caffe/layers/image_data_layer.hpp"
#include "caffe/layers/infogain_loss_layer.hpp"
#include "caffe/layers/inner_product_layer.hpp"
#include "caffe/layers/input_layer.hpp"
#include "caffe/layers/log_layer.hpp"
#include "caffe/layers/memory_data_layer.hpp"
#include "caffe/layers/multinomial_logistic_loss_layer.hpp"
#include "caffe/layers/mvn_layer.hpp"
#include "caffe/layers/parameter_layer.hpp"
#include "caffe/layers/power_layer.hpp"
#include "caffe/layers/prelu_layer.hpp"
#include "caffe/layers/reduction_layer.hpp"
#include "caffe/layers/reshape_layer.hpp"
#include "caffe/layers/scale_layer.hpp"
#include "caffe/layers/sigmoid_cross_entropy_loss_layer.hpp"
#include "caffe/layers/silence_layer.hpp"
#include "caffe/layers/slice_layer.hpp"
#include "caffe/layers/softmax_loss_layer.hpp"
#include "caffe/layers/split_layer.hpp"
#include "caffe/layers/spp_layer.hpp"
#include "caffe/layers/threshold_layer.hpp"
#include "caffe/layers/tile_layer.hpp"
#include "caffe/layers/window_data_layer.hpp"


namespace caffe
{
	extern INSTANTIATE_CLASS(InputLayer);
	extern INSTANTIATE_CLASS(InnerProductLayer);
	extern INSTANTIATE_CLASS(DropoutLayer);

	extern INSTANTIATE_CLASS(ConvolutionLayer);
	REGISTER_LAYER_CLASS(Convolution);

	extern INSTANTIATE_CLASS(BiasLayer);
	extern INSTANTIATE_CLASS(BatchNormLayer);

	extern INSTANTIATE_CLASS(ReLULayer);
	REGISTER_LAYER_CLASS(ReLU);

	extern INSTANTIATE_CLASS(PoolingLayer);
	REGISTER_LAYER_CLASS(Pooling);

	extern INSTANTIATE_CLASS(LRNLayer);
	REGISTER_LAYER_CLASS(LRN);

	extern INSTANTIATE_CLASS(ScaleLayer);

	extern INSTANTIATE_CLASS(SoftmaxLayer);
	REGISTER_LAYER_CLASS(Softmax);


	extern INSTANTIATE_CLASS(FlattenLayer);
	extern INSTANTIATE_CLASS(ConcatLayer);
	extern INSTANTIATE_CLASS(ReshapeLayer);


#if 0
	extern INSTANTIATE_CLASS(AbsValLayer);
	REGISTER_LAYER_CLASS(AbsVal);

	extern INSTANTIATE_CLASS(AccuracyLayer);
	REGISTER_LAYER_CLASS(Accuracy);

	extern INSTANTIATE_CLASS(ArgMaxLayer);
	REGISTER_LAYER_CLASS(ArgMax);

	extern INSTANTIATE_CLASS(BatchNormLayer);
	REGISTER_LAYER_CLASS(BatchNorm);

	extern INSTANTIATE_CLASS(BatchReindexLayer);
	REGISTER_LAYER_CLASS(BatchReindex);

	extern INSTANTIATE_CLASS(BiasLayer);
	REGISTER_LAYER_CLASS(Bias);

	extern INSTANTIATE_CLASS(BNLLLayer);
	REGISTER_LAYER_CLASS(BNLL);

	extern INSTANTIATE_CLASS(ConcatLayer);
	REGISTER_LAYER_CLASS(Concat);

	extern INSTANTIATE_CLASS(ContrastiveLossLayer);
	REGISTER_LAYER_CLASS(ContrastiveLoss);

	extern INSTANTIATE_CLASS(CropLayer);
	REGISTER_LAYER_CLASS(Crop);

	extern INSTANTIATE_CLASS(DataLayer);
	REGISTER_LAYER_CLASS(Data);

	extern INSTANTIATE_CLASS(DeconvolutionLayer);
	REGISTER_LAYER_CLASS(Deconvolution);

	extern INSTANTIATE_CLASS(DropoutLayer);
	REGISTER_LAYER_CLASS(Dropout);

	extern INSTANTIATE_CLASS(DummyDataLayer);
	REGISTER_LAYER_CLASS(DummyData);

	extern INSTANTIATE_CLASS(EltwiseLayer);
	REGISTER_LAYER_CLASS(Eltwise);

	extern INSTANTIATE_CLASS(ELULayer);
	REGISTER_LAYER_CLASS(ELU);

	extern INSTANTIATE_CLASS(EmbedLayer);
	REGISTER_LAYER_CLASS(Embed);

	extern INSTANTIATE_CLASS(EuclideanLossLayer);
	REGISTER_LAYER_CLASS(EuclideanLoss);

	extern INSTANTIATE_CLASS(ExpLayer);
	REGISTER_LAYER_CLASS(Exp);

	extern INSTANTIATE_CLASS(FilterLayer);
	REGISTER_LAYER_CLASS(Filter);

	extern INSTANTIATE_CLASS(FlattenLayer);
	REGISTER_LAYER_CLASS(Flatten);

	extern INSTANTIATE_CLASS(HDF5DataLayer);
	REGISTER_LAYER_CLASS(HDF5Data);

	extern INSTANTIATE_CLASS(HDF5OutputLayer);
	REGISTER_LAYER_CLASS(HDF5Output);

	extern INSTANTIATE_CLASS(HingeLossLayer);
	REGISTER_LAYER_CLASS(HingeLoss);

	extern INSTANTIATE_CLASS(Im2colLayer);
	REGISTER_LAYER_CLASS(Im2col);

	extern INSTANTIATE_CLASS(ImageDataLayer);
	REGISTER_LAYER_CLASS(ImageData);

	extern INSTANTIATE_CLASS(InfogainLossLayer);
	REGISTER_LAYER_CLASS(InfogainLoss);

	extern INSTANTIATE_CLASS(InnerProductLayer);
	REGISTER_LAYER_CLASS(InnerProduct);

	extern INSTANTIATE_CLASS(InputLayer);
	REGISTER_LAYER_CLASS(Input);

	extern INSTANTIATE_CLASS(LogLayer);
	REGISTER_LAYER_CLASS(Log);

	extern INSTANTIATE_CLASS(MemoryDataLayer);
	REGISTER_LAYER_CLASS(MemoryData);

	extern INSTANTIATE_CLASS(MultinomialLogisticLossLayer);
	REGISTER_LAYER_CLASS(MultinomialLogisticLoss);

	extern INSTANTIATE_CLASS(MVNLayer);
	REGISTER_LAYER_CLASS(MVN);

	extern INSTANTIATE_CLASS(ParameterLayer);
	REGISTER_LAYER_CLASS(Parameter);

	extern INSTANTIATE_CLASS(PowerLayer);
	REGISTER_LAYER_CLASS(Power);

	extern INSTANTIATE_CLASS(PReLULayer);
	REGISTER_LAYER_CLASS(PReLU);

	extern INSTANTIATE_CLASS(ReductionLayer);
	REGISTER_LAYER_CLASS(Reduction);

	extern INSTANTIATE_CLASS(ReshapeLayer);
	REGISTER_LAYER_CLASS(Reshape);

	extern INSTANTIATE_CLASS(ScaleLayer);
	REGISTER_LAYER_CLASS(Scale);

	extern INSTANTIATE_CLASS(SigmoidCrossEntropyLossLayer);
	REGISTER_LAYER_CLASS(SigmoidCrossEntropyLoss);

	extern INSTANTIATE_CLASS(SilenceLayer);
	REGISTER_LAYER_CLASS(Silence);

	extern INSTANTIATE_CLASS(SliceLayer);
	REGISTER_LAYER_CLASS(Slice);

	extern INSTANTIATE_CLASS(SoftmaxWithLossLayer);
	REGISTER_LAYER_CLASS(SoftmaxWithLoss);

	extern INSTANTIATE_CLASS(SplitLayer);
	REGISTER_LAYER_CLASS(Split);

	extern INSTANTIATE_CLASS(SPPLayer);
	REGISTER_LAYER_CLASS(SPP);

	extern INSTANTIATE_CLASS(ThresholdLayer);
	REGISTER_LAYER_CLASS(Threshold);

	extern INSTANTIATE_CLASS(TileLayer);
	REGISTER_LAYER_CLASS(Tile);

	extern INSTANTIATE_CLASS(WindowDataLayer);
	REGISTER_LAYER_CLASS(WindowData);

#endif


}
