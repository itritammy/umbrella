
#include "custom_classifier_features.h"
#include "itri_hasp_interface.h"

#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif


static int g_valid_feature_IDs[] = { 96 /* Inference*/, 97/*Training*/, -1 }; /// The list of supported feature IDs

MSG_t CheckClassifierLicense(stCLASSIFIER_FEATURE_MAP* map)
{
	memset(map, -1, sizeof(stCLASSIFIER_FEATURE_MAP));
	for (int i = 0; g_valid_feature_IDs[i] != -1; i++)
	{
		if (itri_USB_licence_check_with_read_memory(g_valid_feature_IDs[i], 0, sizeof(map->feature), map->feature) == MSG_NO_ERROR)
			return MSG_NO_ERROR;
	}

	return MSG_USB_KEY_FEATURE_NOT_FOUND;
}


MSG_t CheckClassifierFeatures(stCLASSIFIER_FEATURE_MAP* map)
{
	//if (map->feature[0] != 0x01)  /// Is feature 0 support ?
	//	return MSG_INVALID_FEATURE_PARAM;
	
	return MSG_NO_ERROR;
}


#ifdef __cplusplus
}
#endif

